
# Entrenando sus dedos

En este capítulo vamos a hablar sobre cómo aprender Vim de manera más efectiva, pero primero debemos hacer un poco de preparación.

Configuremos un mapeo más que ahorrará más desgaste en su mano izquierda que cualquier otro mapeo que haya creado. Ejecute el siguiente comando:

```vimscript
:inoremap jk <esc>
```

Ahora ingrese al modo de inserción y escriba `jk`. Vim actuará como si presionara la tecla `<Esc>` y lo regresara al modo normal.

Hay varias formas de salir del modo inserción en Vim de forma predeterminada:

```vimscript
. `<esc>`
. `<c-c>`
. `<c-[>`
```

Cada uno de ellos requiere que estire sus dedos incómodamente. Usar `jk` es genial porque las teclas están justo debajo de dos de sus dedos más fuertes y no tiene que realizar un acorde.

Algunas personas prefieren usar `jj` en lugar de `jk`, pero yo prefiero `jk` por dos razones:

* Se escribe con dos teclas separadas, por lo que puede "rodar" los dedos en lugar de usar el mismo dos veces.
* Presionar `jk` en modo normal por costumbre se moverá hacia abajo y luego hacia arriba, dejándole exactamente donde comenzó. Usar `jj` en modo normal lo moverá a un lugar diferente en su archivo.

Si escribe en un idioma en el que `jk` es una combinación de letras que se usa con frecuencia (como en holandés), es probable que desee elegir un mapeo diferente.

### Aprendiendo Map

Ahora que tiene un nuevo y excelente mapeo, ¿cómo puede aprender a usarlo? Lo más probable es que ya tenga la tecla de escape en su memoria muscular, así que cuando esté editando, la golpeará sin siquiera pensarlo.

El truco para reaprender un mapeo es forzarse a usarlo deshabilitando la(s) tecla(s) antigua(s). Ejecute el siguiente comando:

```vimscript
:inoremap <esc> <nop>
```

Esto efectivamente desactiva la tecla `<Esc>` en el modo inserción al decirle a Vim que realice `<nop>` (not operation) en su lugar. Ahora tiene que usar su mapeo `jk` para salir del modo de inserción.

Al principio se olvidará, escribira escape y comenzara a tratar de hacer algo en el modo normal y terminará con caracteres extraviados en su texto.
Será frustrante, pero si se limita a ello, se sorprenderá de lo rápido que su mente y sus dedos absorben el nuevo mapeo.
Dentro de una o dos horas ya no estará golpeando accidentalmente la tecla `<esc>`.

Esta idea se aplica a cualquier nuevo mapeo que cree para reemplazar uno antiguo, e incluso a la vida en general.
Cuando quiera cambiar un hábito, ¡hagalo más difícil o imposible!

Si desea comenzar a cocinar comidas en lugar de cenas de TV con microondas, no compre cenas de TV cuando vaya de compras.
Va a cocinar algo de comida real cuando tengas suficiente hambre.

Si quiere dejar de fumar, siempre deje sus cigarrillos en la cajuela de su auto.
Cuando sienta la necesidad de fumar un cigarrillo informal, pensará en el dolor de cabeza que será caminar hasta el auto y es menos probable que se moleste en hacerlo.

### Ejercicios

Si aún se encuentra usando las teclas de flecha para navegar por Vim en el modo normal, asigne a `<nop>` para detenerse.

Si aún utiliza las teclas de flecha en el modo de inserción, asignelas a `<nop>` allí también.
La forma correcta de usar Vim es salir del modo de inserción tan pronto como sea posible y usar el modo normal para moverse.
