
# Resaltado de sintaxis incluso mas avanzado

El resaltado de sintaxis en Vim es un tema que podría llenar fácilmente un libro propio.

Vamos a cubrir una última parte importante aquí y luego pasaremos a otras cosas.
Si desea obtener más información, puede leer la documentación de Vim con `:help syntax` y ver los archivos de sintaxis que otras personas han creado.

### Resaltando strings

Potion, como la mayoría de los lenguajes de programación, admite literales de cadena como `"¡Hola, mundo!"`.
Debemos resaltar estos literales como `strings`.
Para hacer esto, usaremos el comando `syntax region`. Agregue lo siguiente a su archivo de sintaxis de Potion:

```vimscript
syntax region potionString start=/\v"/ skip=/\v\\./ end=/\v"/
highlight link potionString String
```

¡Cierre y vuelva a abrir su archivo `factorial.pn` y verá que el string al final del archivo está resaltado!

La última línea aquí debería ser familiar.
Vuelva a leer los dos capítulos anteriores si no entiende lo que hace.

La primera línea se agrega a un grupo de sintaxis utilizando `"region"`.
Las regiones tienen un patrón de "inicio" y un patrón de "fin" que especifican dónde comienzan y terminan.
En este caso, una string de Potion comienza cuando vemos una comilla doble y termina cuando vemos la siguiente comilla doble.

El argumento `"skip"` para la sintaxis de región nos permite manejar escapes de strings como `"Ella dijo: \" Vimscript es complicado, pero útil\"!"`.

Si no utilizáramos el argumento `skip`, Vim terminaría la cadena en el `"` antes de Vimscript, ¡que no es lo que queremos!

En pocas palabras, el argumento  `skip` de  `syntax region` le dice a Vim:
"una vez que comience a hacer coincidir esta región, quiero que ignore todo lo que coincida con `skip`,
incluso si normalmente se consideraría el final de la región".


Tómese unos minutos y piense en esto. ¿Qué sucede con algo como `"foo \\" bar"`?
¿Es ese el comportamiento correcto?
¿Será ese siempre el comportamiento correcto? ¡Cierre este libro, tómese unos minutos y piense realmente en esto!

### Ejercicios

Agregue resaltado de sintaxis para cadenas entre comillas simples.

Lea `:help syn-region`.

Leer eso debería llevar más tiempo que leer este capítulo. Viértase un trago, ¡se lo ha ganado!
