
Funciones de Strings
====================


Vim tiene muchas funciones integradas para manipular `strings`. En este capítulo veremos algunas de las más importantes.

### Length

La primera función que veremos es `strlen`. Ejecute el siguiente comando:


```vimscript
:echom strlen("foo")
```

Vim muestra 3, que es la longitud de el string "foo". Ahora pruebe el siguiente comando:


```vimscript
:echom len("foo")
```

Vim muestra una vez más 3. Cuando se usa con strings, `len` y `strlen` tienen efectos idénticos. Volveremos a hablar más adelante sobre ellos n el libro.

### Splitting


Ejecute el siguiente comando (tenga en cuenta que es un `echo` y no un `echom`):


```vimscript
:echo split("one two three")
```

Vim muestra `['one', 'two', 'three']`. La función de `split` divide un string en una lista. Hablaremos de listas en breve, pero por ahora no se preocupe demasiado por ellas.

También puede decirle a Vim que use un separador que no sea un "espacio en blanco" para la división. Ejecute el siguiente comando:


```vimscript
:echo split("one,two,three", ",")
```

Vim mostrará una vez más `['one', 'two', 'three']`, porque el segundo argumento para `split` le dice a `split` que use el carácter `,` para dividir el string.

### Joining


No solo puedes dividir strings, también puedes unirlas. Ejecute el siguiente comando:


```vimscript
:echo join(["foo", "bar"], "...")
```

Vim mostrará `foo...bar`. No te preocupes por la sintaxis de la lista por ahora.

`split` y `join` se pueden combinar con gran efecto. Ejecute el siguiente comando:


```vimscript
:echo join(split("foo bar"), ";")
```

Vim muestra `foo;bar`. Primero partimos la cadena `"foo bar"` en una lista, luego la unimos a esa lista usando un punto y coma como separador.

### Mayúsculas y Minúsculas

Vim tiene dos funciones para cambiar la capitalización de los strings. Ejecute los siguientes comandos:


```vimscript
:echom tolower("Foo")
:echom toupper("Foo")
```

Vim muestra `foo` y `FOO`. Esto debería ser bastante fácil de entender.


En muchos lenguajes (como Python), una practica común es forzar strings a minúsculas antes de compararlas para realizar una comparación que no distinga mayúsculas y minúsculas.
En Vimscript esto no es necesario porque tenemos los operadores de comparación que no distinguen entre mayúsculas y minúsculas.
Vuelva a leer el capítulo sobre comparaciones si no las recuerda.

Depende de usted decidir si usar `tolower` y `==#`, o simplemente `==?` para realizar comparaciones entre mayúsculas y minúsculas.
No parece haber una fuerte preferencia en la comunidad de Vimscript.
Elija uno y acostúmbrese a él para todos sus scripts.

## Ejercicios

Ejecute `:echo split('1 2')` y `:echo split('1,,,2', ',')`. ¿Se comportan igual?.
Lea `:help split()`.

Lea `:help join()`.

Lea `:help functions` y hojee la lista de funciones incorporadas para aquellas que mencionan la palabra "String". Use el comando `/` para hacerlo más fácil (recuerde, los archivos de ayuda de Vim se pueden navegar como cualquier otro tipo de archivo). Hay muchas funciones aquí, así que no sienta que necesitas leer la documentación de cada una. Solo intente obtener una idea de lo que está disponible si lo necesita en el futuro.
