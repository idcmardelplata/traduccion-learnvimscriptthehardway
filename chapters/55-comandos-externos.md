
# Comandos externos

Vim sigue la filosofía de UNIX de "hacer una cosa bien".
En lugar de tratar de agrupar toda la funcionalidad que pueda desear dentro del editor, la forma correcta de usar Vim es delegar a comandos externos cuando sea apropiado.

Agreguemos un poco de interacción con el compilador de Potion a nuestro plugin para mojarnos los pies con comandos externos en Vim.

### Compilando

Primero agregaremos un comando para compilar y ejecutar el archivo de Potion actual.
Hay varias formas de hacer esto, pero simplemente usaremos un comando externo por ahora.

Cree un archivo `potion/ftplugin/potion/running.vim` en el repositorio de su plugin.
Aquí es donde crearemos los mapeos para compilar y ejecutar los archivos de Potion.

```vimscript
if !exists("g:potion_command")
    let g:potion_command = "potion"
endif

function! PotionCompileAndRunFile()
    silent !clear
    execute "!" . g:potion_command . " " . bufname("%")
endfunction

nnoremap <buffer> <localleader>r :call PotionCompileAndRunFile()<cr>
```

El primer fragmento almacena el comando utilizado para ejecutar Potion en una variable global, si es que esa variable aún no está configurada.
Hemos visto este tipo de verificación antes.

Esto permitirá a los usuarios sobreescribirlo si potion no está en su `$PATH` poniendo una línea como `let g:potion_command="/Users/sjl/src/potion/potion"` en su archivo `~/.vimrc`.

La última línea agrega un mapeo local al búfer que llama a una función que hemos definido anteriormente.
Recuerde que debido a que este archivo está en el directorio `ftdetect/potion`, se ejecutará cada vez que el `filetype` de un archivo se configure en potion.

La funcionalidad real está en la función `PotionCompileAndRunFile()`. Continúe y guarde este archivo, abra `factorial.pn` y presione `<localleader>r` para ejecutar el mapeo y ver qué sucede.

Si `potion` está en su `$PATH`, el archivo debe ejecutarse y debería ver la salida en su terminal (o en la parte inferior de la ventana si está utilizando una GUI Vim). Si recibe un error acerca de que no se encuentra el comando  `potion`, deberá configurar `g:potion_command` en su archivo `~/.vimrc` como se mencionó anteriormente.

Echemos un vistazo a cómo funciona la función `PotionCompileAndRunFile()`.

### Comando Bang!

El comando `:!` (pronunciado "bang") en Vim ejecuta comandos externos y muestra su salida en la pantalla.
Pruébelo ejecutando el siguiente comando:

```vimscript
:!ls
```
Vim debería mostrarle la salida del comando `ls`, así como el mensaje "Presione ENTRAR o escriba el comando para continuar".

Vim no pasa ninguna entrada al comando cuando se ejecuta de esta manera.
Confirme esto ejecutando:

```vimscript
:!cat
```

Escriba algunas líneas y verá que el comando `cat` las retorna de nuevo, como lo haría normalmente si ejecutara `cat` fuera de Vim.
Use `Ctrl-D` para terminar.

Para ejecutar un comando externo sin `presionar ENTER o escriba un comando para continuar`, use `:silent!`. Ejecute el siguiente comando:

```vimscript
:silent !echo Hello, world.
```
Si ejecuta esto en una GUI de  Vim como MacVim o gVim, no verá la salida `hello, world` del comando anterior.

Si lo ejecuta en un terminal Vim, sus resultados pueden variar según su configuración.
Es posible que deba ejecutar `:redraw!` para arreglar su pantalla después de ejecutar un pelado `:silent!`

Tenga en cuenta que este comando es `:silent !` y no `:silent!` (¿Ve el espacio?) Esos son dos comandos diferentes,
¡y queremos el primero! ¿No es genial Vimscript?

Volvamos a la función `PotionCompileAndRun()`:

```vimscript
function! PotionCompileAndRunFile()
    silent !clear
    execute "!" . g:potion_command . " " . bufname("%")
endfunction
```

Primero ejecutamos un comando `silent! clear`, que debe borrar la pantalla sin `"presionar ENTER ..."` Esto asegurará que solo veamos el resultado de esta ejecución, lo que es útil cuando ejecuta los mismos comandos una y otra vez.

La siguiente línea usa nuestro viejo amigo `execute` para construir un comando dinámicamente.
El comando que crea se verá más o menos así:

```vimscript
!potion factorial.pn
```
Tenga en cuenta que no hay `silent` aquí, por lo que el usuario verá la salida del comando y tendrá que presionar enter para volver a Vim.
Esto es lo que queremos para este mapeo en particular, así que estamos listos.

### Mostrando Bytecode

El compilador de Potion tiene una opción que le permitirá ver el bytecode que genera a medida que se compila.
Esto puede ser útil si está intentando depurar su programa a muy bajo nivel.
Pruébelo ejecutando el siguiente comando en una terminal:

```vimscript
potion -c -V factorial.pn
```

Debería ver una gran cantidad de salida parecida a esta:

```vimscript
-- parsed --
code ...
-- compiled --
; function definition: 0x109d6e9c8 ; 108 bytes
; () 3 registers
.local factorial ; 0
.local print_line ; 1
.local print_factorial ; 2
...
[ 2] move     1 0
[ 3] loadk    0 0   ; string
[ 4] bind     0 1
[ 5] loadpn   2 0   ; nil
[ 6] call     0 2
...
```

Agreguemos un mapeo que permitirá al usuario ver el bytecode generado para el archivo de Potion actual en una división de Vim para que pueda navegarlo y examinarlo fácilmente.

Primero, agregue la siguiente línea al final de `ftplugin/potion/running.vim`:

```vimscript
nnoremap <buffer> <localleader>b :call PotionShowBytecode()<cr>
```

Nada especial ahi, es solo un mapeo simple. Ahora bosquejemos la función que hará el trabajo:

```vimscript
function! PotionShowBytecode()
    " Get the bytecode.

    " Open a new split and set it up.

    " Insert the bytecode.

endfunction
```

Ahora que tenemos un pequeño esqueleto configurado, hablemos sobre cómo hacerlo realidad.

### system()

Existen varias formas en que podríamos implementar esto, por lo que elegiré una que le será útil más adelante.

Ejecute el siguiente comando:
```vimscript
:echom system("ls")
```

Debería ver la salida del comando `ls` en la parte inferior de la pantalla.
Si ejecuta `:messages`, también lo verá allí.
La función `system()` de Vim toma una cadena de comando como parámetro y devuelve la salida de ese comando como un string.

Puede pasar un segundo string como argumento a `system()`. Ejecute el siguiente comando:

```vimscript
:echom system("wc -c", "abcdefg")
```

Vim mostrará `7` (con algo de padding).
Si pasa un segundo argumento como este, Vim lo escribirá en un archivo temporal y le hara un pipe al comando en el standar input.
Para nuestros propósitos no necesitaremos esto, pero es bueno saberlo.

De vuelta a nuestra función. Edite `PotionShowBytecode()` para completar la primera parte del esqueleto de esta manera:

```vimscript
function! PotionShowBytecode()
    " Get the bytecode.
    let bytecode = system(g:potion_command . " -c -V " . bufname("%"))
    echom bytecode

    " Open a new split and set it up.

    " Insert the bytecode.

endfunction
```

Continúe y pruébelo recargando el archivo y ejecutando `:set ft=potion` en `factorial.pn` para recargarlo, y utilizando el mapeo `<localleader>b`.
Vim debería mostrar el código de bytes en la parte inferior de la pantalla.
Una vez que pueda ver que está funcionando, puede eliminar la línea `echom`.

### Divisiones Scratch

A continuación, vamos a abrir una nueva ventana dividida para que el usuario vea los resultados.
Esto permitirá al usuario ver y navegar por el bytecode con todo el poder de Vim, en lugar de solo leerlo una vez desde la pantalla.

Para hacer esto, crearemos una división "scratch", que no es mas que una división que contiene un búfer que nunca se guardará y se sobrescribirá cada vez que ejecutemos el mapeo. Cambie la función `PotionShowBytecode()` para que se vea así:

```vimscript
function! PotionShowBytecode()
    " Get the bytecode.
    let bytecode = system(g:potion_command . " -c -V " . bufname("%"))

    " Open a new split and set it up.
    vsplit __Potion_Bytecode__
    normal! ggdG
    setlocal filetype=potionbytecode
    setlocal buftype=nofile

    " Insert the bytecode.

endfunction
```
Este nuevo comando debería ser bastante fácil de entender.

`vsplit` crea una nueva división vertical para un búfer llamado `__Potion_Bytecode__`. Rodeamos el nombre con guiones bajos para que sea más claro para el usuario que este no es un archivo normal (es un búfer solo para contener la salida). Los guiones bajos no son especiales, son solo una convención.

A continuación, eliminamos todo en este búfer con `normal! ggdG`. La primera vez que se ejecuta el mapeo, esto no hará nada, pero las veces siguientes reutilizaremos el búfer __Potion_Bytecode__, por lo que esto lo borra.

A continuación, preparamos el búfer estableciendo dos configuraciones locales.
Primero configuramos su tipo de archivo en `potionbytecode`, solo para dejar en claro qué contiene.
También cambiamos la configuración de `buftype` a `nofile`, que le dice a Vim que este búfer no está relacionado con un archivo en el disco, por lo que nunca debería intentar escribirlo.

Todo lo que queda es volcar el bytecode que guardamos en la variable bytecode en este búfer.
Termine la función haciendo que se vea así:

```vimscript
function! PotionShowBytecode()
    " Get the bytecode.
    let bytecode = system(g:potion_command . " -c -V " . bufname("%") . " 2>&1")

    " Open a new split and set it up.
    vsplit __Potion_Bytecode__
    normal! ggdG
    setlocal filetype=potionbytecode
    setlocal buftype=nofile

    " Insert the bytecode.
    call append(0, split(bytecode, '\v\n'))
endfunction
```

La función  `append()` de  Vim toma dos argumentos: un número de línea para agregar después y una lista de cadenas para agregar como líneas.
Por ejemplo, intente ejecutar el siguiente comando:

```vimscript
:call append(3, ["foo", "bar"])
```

Esto agregará dos líneas, `foo` y `bar`, debajo de la línea `3` en su búfer actual.
En este caso, agregamos debajo de la línea `0`, que significa "en la parte superior del archivo".

Necesitamos una lista de strings para agregar, pero solo tenemos un solo string con caracteres de nueva línea incrustados desde cuando usamos `system()`.
Usamos la función `split()` de Vim para dividir ese trozo gigante de texto en una lista de strings.
`split()` toma un string para dividir y una expresión regular para encontrar los puntos donde dividir.
Es muy simple

Ahora que la función está completa, continúe y pruebe el mapeo.
Cuando ejecuta `<localleader>b` en el búfer `factorial.pn`, Vim abrirá un nuevo búfer que contiene el bytecode de Potion.
Juegue con él cambiando la fuente, guardando el archivo y ejecutando el mapeo nuevamente para ver el cambio del bytecode.

### Ejercicios

* Lea `:help bufname`.

* Lea `:help buftype`.

* Lea `:help append()`.

* Lea `:help split()`.

* Lea `:help :!`.

* Lea `:help :read` y `:help :read!` no cubrimos estos comandos, pero son extremadamente útiles).

* Lea `:help system()`.

* Lea `:help design-not`.

Actualmente, nuestros mapeos requieren que el usuario guarde el archivo antes de ejecutar el mapeo para que los cambios surtan efecto.
`undo `es barato en estos días, así que edite las funciones que escribimos para guardar el archivo actual para ellos.

¿Qué sucede cuando ejecuta el mapeo de bytecode en un archivo de Potion con un error de sintaxis? ¿Por qué pasa eso?

Cambie la función `PotionShowBytecode()` para detectar cuándo el compilador de Potion devuelve un error y muestra un mensaje de error al usuario.

### Crédito Extra

Cada vez que ejecute el mapeo de bytecode, se creará una nueva división vertical, incluso si el usuario no ha cerrado la anterior.
Si el usuario no se molesta en cerrarlos, podría terminar con muchas ventanas adicionales apiladas.

Cambie `PotionShowBytecode()` para detectar cuando una ventana ya está abierta para el búfer __Potion_Bytecode__, y cuando ese sea el caso, cambie a él en lugar de crear una nueva división.

Probablemente quiera leer `:help bufwinnr()` para este caso.


### Mas crédito extra

¿Recuerda cómo configuramos el tipo de archivo del búfer temporal a potionbytecode?
Cree un archivo `syntax/potionbytecode.vim` y defina el resaltado de sintaxis para los buffers de bytecode de Potion para que sean más fáciles de leer.
