
# Mapeo Estricto

Prepárese, porque las cosas van a ponerse un poco salvajes.

Hasta ahora, hemos utilizado `map`, `nmap`, `vmap` e `imap` para crear mapeos de teclas que ahorrarán tiempo. Estos funcionan, pero tienen un inconveniente.

Ejecute los siguientes comandos:

```vimscript
:nmap - dd
:nmap \ -
```

Ahora intente presionar `\` (en modo normal). ¿que sucede?

Cuando presiona `\` Vim ve la asignación y dice "Debería ejecutar `-`, en su lugar". Pero resulta que ya hemos mapeado `-` para hacer otra cosa! Vim ve eso y dice "oh, ahora necesito ejecutar `dd`", y así se borra la línea actual.

Cuando mape teclas con estos comandos, Vim también tendrá en cuenta los otros mapeos. Esto puede sonar como algo bueno al principio, pero en realidad es puro mal. Hablemos sobre por qué, pero primero elimine esas asignaciones ejecutando los siguientes comandos:

```vimscript
:nunmap -
:nunmap \
```

### Recursion

Ejecute este comando:

```vimscript
:nmap dd O<esc>jddk
```
A primera vista podría parecer que esto mapearía `dd` a los siguientes pasos:

1. Abre una nueva línea encima de esta.
2. Sale del modo inserción.
3. Retrocede hacia la linea de abajo.
4. Elimina la línea actual.
5. Sube a la línea en blanco que acaba de crear.

Efectivamente esto debería "borrar la línea actual". Inténtalo.

Vim parecerá congelarse al presionar `dd`. Si presiona `<c-c>` obtendrá a Vim de vuelta, ¡pero habrá un montón de líneas vacías en tu archivo! ¿Que pasó?

Este mapeo es en realidad recursivo! Cuando presiona `dd`, Vim dice:

1. `dd` esta mapeado, así que ejecuto el mapeo.

2.     Abre una línea.
3.     Salir del modo de inserción.
4.     Mueve hacia abajo una línea.
5.     dd esta mapeado, así que ejecuto el mapeo.
6.         Abre una línea.
7.         Salir del modo  inserción.
8.         Mueve hacia abajo una línea.
9.         dd esta mapeado, así que ejecuto el mapeo, y así sucesivamente...

Este mapeo nunca puede terminar de ejecutarse! Continúe y elimine esta cosa terrible con el siguiente comando:

```vimscript
:nunmap dd
```

### Efectos secundarios

Una desventaja de los comandos de `*map` es el peligro de la concurrencia. Otra es que su comportamiento puede cambiar si instala un plugin que mapea las teclas de las que depende.

Cuando instale un nuevo plugin de Vim, hay una buena posibilidad de que no use ni memorice cada mapeo que el plugin crea. Incluso si lo hace, tendría que volver atrás y revisar su archivo `~/.vimrc` para asegurarse de que ninguno de sus mapeos personalizados use una combinación que el complemento haya asignado.

Esto haría que la instalación de complementos sea tediosa y propensa a errores. Tiene que haber una mejor manera.

### Mapeos no recursivos

Vim ofrece otro conjunto de comandos de mapeo que no tomarán en cuenta sus mapeos previos cuando sean ejecutados.

Ejecute estos comandos:

```vimscript
:nmap x dd
:nnoremap \ x
```

Ahora presione `\` y vea que pasa.

Cuando presiona `\` Vim, ignora la asignación `x` y hace lo que haría `x` de forma predeterminada. En lugar de eliminar la línea actual, elimina el carácter actual.

Cada uno de los comandos `*map` tiene una contra parte `*noremap` que ignora sus otras asignaciones: `noremap`, `nnoremap`, `vnoremap` e `inoremap`.

¿Cuándo debería usar estas variantes no recursivas en lugar de sus equivalentes normales?

**Siempre**.

**No, en serio, siempre**.

El uso de un simple `*map` es solo para problemas cuando instala un plugin o agrega un nuevo mapeo personalizado. Ahorrase el problema y escriba los caracteres adicionales para asegurarse de que nunca suceda nada malo con sus mapeos.

### Ejercicios

Convierta todos los mapeos que agregó a su archivo `~/.vimrc` en los capítulos anteriores a sus homólogos no recursivos.

Lea: `:help unmap`.
