
# Programacion Responsable

Hasta ahora hemos cubierto un montón de comandos Vim que le permiten personalizar el editor rápidamente.
Todos excepto los grupos de comandos automáticos, fueron comandos de una sola línea que puede agregar a su archivo `~/.vimrc` en segundos.

En la siguiente parte del libro, vamos a sumergirnos en Vimscript como un lenguaje de programación real, pero antes de hacerlo quiero hablar un poco sobre cómo permanecer cuerdo mientras escribe grandes cantidades de Vimscript.

### Comentando

Vimscript es extremadamente poderoso, pero ha crecido orgánicamente con el paso de los años hasta convertirse en un laberinto sinuoso listo para atrapar a los programadores desprevenidos que ingresan en él.

Las opciones y los comandos a menudo son concisos y difíciles de leer, y solucionar los problemas de compatibilidad puede aumentar la complejidad de su código. Escribir un plugin y permitir la personalización del usuario introduce otra capa completa por encima de eso.

Esté a la defensiva cuando escriba algo que requiera más que unas pocas líneas de Vimscript. Agregue un comentario que explique lo que hace, y si hay un tema de ayuda relevante, ¡mencionelo en el comentario!

Esto no solo lo beneficia cuando intenta mantenerlo meses o años más tarde, sino que también ayuda a otras personas a entenderlo si comparte su archivo `~/.vimrc` en Bitbucket o GitHub (que recomiendo encarecidamente).

### Agrupando

Nuestros mapeos para edición y recarga del fichero `~/.vimrc` han hecho que sea rápido y fácil agregarle nuevas cosas sobre la marcha. Desafortunadamente, esto también hace que sea más fácil salirse de control y volverse mas difícil de navegar.

Una forma de combatir esto es usar las capacidades de plegado de código de Vim y agrupar las líneas en secciones. Si nunca ha usado el plegado de Vim, debería buscarlo lo antes posible. Algunas personas (incluyéndome a mí) lo consideran indispensable en nuestro uso diario del editor.

Primero necesitamos configurar el plegado para archivos Vimscript. Agregue las siguientes líneas a su archivo `~/.vimrc`:

```vimscript
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END
```

Esto le dirá a Vim que use el método `marker` de plegado para cualquier archivo Vimscript.

Continúe y ejecute: `setlocal foldmethod = marker` en la ventana con su archivo `~/.vimrc`. La recarga del archivo no funcionará, ya que Vim ha configurado el tipo de archivo para este archivo y el comando automático solo se dispara cuando eso sucede. En el futuro no necesitará hacerlo manualmente.

Ahora agregue líneas antes y después de ese grupo de autocommand para que se vea así:

```vimscript
" Vimscript file settings ---------------------- {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}
```

Vuelva al modo normal, coloque el cursor en cualquiera de esas líneas y escriba `za`. Vim plegara las líneas comenzando en la que contiene `{{{{` y terminando en la que contiene `}}}.` Escribir `za` nuevamente desplegará las líneas.

Puede pensar que agregar comentarios explícitos al código fuente que describen el plegamiento es feo al principio. Pensé lo mismo cuando lo vi por primera vez. Para la mayoría de los archivos todavía creo que está mal. No todos usan el mismo editor, por lo que ensuciar su código con comentarios plegados es simplemente ruidoso para cualquier otra persona que vea el código en otra cosa que no sea Vim.

Los archivos Vimscript son un caso especial, sin embargo. Es muy poco probable que alguien que no use Vim esté leyendo tu código, y es especialmente importante agrupar las cosas explícita y cuidadosamente al escribir Vimscript para que no se vuelva loco.

Pruebe estos pliegues explícitos por un tiempo. Podría comenzar a amarlos.

### Nombres Cortos

Vim le permite usar nombres abreviados para la mayoría de los comandos y opciones. Por ejemplo, ambos comandos hacen exactamente lo mismo:

```vimscript
:setlocal wrap
:setl wrap
```

Me gustaría advertirle enérgicamente contra el uso de estas abreviaturas en su archivo `~/.vimrc` y en los complementos que escriba. Vimscript es lo suficientemente conciso y críptico para empezar; Acortar aún más las cosas solo hará que sea aún más difícil de leer. Incluso si sabe lo que significa un cierto comando abreviado, es posible que otra persona que lea su código no lo sepa.

Dicho esto, las formas abreviadas son excelentes para ejecutar comandos manualmente en medio de la codificación. Nadie los volverá a ver después de presionar enter, así que no hay razón para presionar más teclas de las que debe.

### Ejercicios

Revise todo su archivo `~/.vimrc` y organice las líneas en grupos relacionados. Algunos lugares para comenzar podrían ser :"Configuración básica", "Configuración específica del tipo de archivo", "Mapeos" y "Barra de estado". Añadir marcadores plegables con encabezados a cada sección.

Descubra cómo hacer que Vim pliegue todo automáticamente la primera vez que abra el archivo. Mirar `:help foldlevelstart` para comenzar es un buen lugar.

Revise su archivo `~/.vimrc` y cambie los comandos y opciones abreviados a sus nombres completos.

Revise su archivo `~/.vimrc` y asegúrese de que no tenga información confidencial en su interior. Cree un git o un repositorio de Mercurial, mueva el archivo a él y haga un enlace simbólico de ese archivo a `~/.vimrc`.

Confíe en el repositorio que acaba de crear y póngalo en Bitbucket o GitHub para que otras personas puedan verlo y obtener ideas para ellos. Asegúrese de comitear y enviar el repositorio con bastante frecuencia para que se registren sus cambios.

Si usa Vim en más de una máquina, clone el repositorio y vincule el archivo allí también. Esto hará que sea simple y fácil de usar exactamente la misma configuración de Vim en todas las máquinas con las que trabaje.
