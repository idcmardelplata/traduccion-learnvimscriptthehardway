
# Mapeo Modal

En el último capítulo hablamos sobre cómo mapear teclas en Vim. Hemos usado el comando `map` que hace que las teclas funcionen en modo normal. Si jugó un poco antes de pasar a este capítulo, es posible que haya notado que los mapeos también tuvieron efecto en el modo visual.

Puede ser más específico acerca de cuándo desea que se apliquen los mapeos utilizando `nmap`, `vmap` e `imap`.

Estas variantes le dicen a Vim que solo use el mapeo en modo normal, visual o de inserción, respectivamente.

Ejecute este comando:

```vimscript
:nmap \ dd
```

Ahora ponga el cursor en su archivo de texto, asegúrese de que está en modo normal y presione `\`. Vim borrará la línea actual.

Ahora ingrese al modo visual e intente presionar `\`. No pasará nada porque le dijimos a Vim que solo usara esa asignación en modo normal (y `\` no hace nada por defecto).

Ejecute este comando:

```vimscript
:vmap \ U
```

Ingrese al modo visual y seleccione un texto, luego presione `\` . Vim convertirá el texto a mayúsculas!

Pruebe la tecla `\` varias veces en los modos normal y visual y observe que ahora hace algo completamente diferente según el modo en el que se encuentre.

### Memoria muscular

Al principio, la idea de mapear la misma tecla para hacer cosas diferentes según el modo en el que te encuentres puede parecer una idea terrible. ¿Por qué querría detenerse y pensar en qué modo está antes de presionar la tecla? No invalidaría eso cualquier momento que ahorramos con el mapeo?

En la práctica resulta que esto no es realmente un problema. Una vez que comienza a usar Vim a menudo, ya no estará pensando en las teclas individuales que estás escribiendo. Sino que pensará: "borrar una línea" y no "presionar dd". Sus dedos y su cerebro aprenderán sus mapeos y las teclas se convertirán en algo  subconsciente.

### Modo inserción

Ahora que hemos cubierto cómo mapear las teclas en modo normal y visual, pasemos al modo de inserción.

Ejecute este comando:

```vimscript
:imap <c-d> dd
```

Podría pensar que esto le permitiría presionar `Ctrl + d` cuando esté en el modo de inserción para eliminar la línea actual. Esto sería útil porque no tendría que volver al modo normal para recortar líneas.

Sigue adelante e inténtelo. No funcionará, en su lugar, solo pondrá dos dd en su archivo. Eso es bastante inútil.

El problema es que Vim está haciendo exactamente lo que le dijimos. Dijimos: "cuando presiono `<c-d>` quiero que hagas lo que normalmente haces cuando se presionan las teclas d y d". Bueno, normalmente cuando está en el modo de inserción y presiona la tecla d dos veces, obtiene dos dd seguidas.

Para que este mapeo haga lo que pretendíamos, necesitamos ser muy explícitos.

Ejecute este comando para cambiar el mapeo:

```vimscript
:imap <c-d> <esc>dd
```


`<esc>` es nuestra forma de decirle a Vim que presione la tecla Escape, que nos sacará del modo de inserción.

Ahora pruebe el mapeo. Funciona, pero note que ahora está nuevamente en modo normal. Esto tiene sentido porque le dijimos a Vim que <c-d> debería salir del modo de inserción y eliminar una línea, pero nunca le dijimos que regresara al modo de inserción.

Ejecute un comando más para arreglar el mapeo de una vez por todas:

```vimscript
:imap <c-d> <esc>ddi
```

La `i` al final ingresa al modo de inserción, y nuestro mapeo finalmente está completo.

### Ejercicios

Configure una mapeo para que pueda presionar <c-u> y convertir la palabra actual a mayúsculas cuando esté en el modo de inserción. Recuerde que `U`  en modo visual pone en mayúsculas la selección. Personalmente encuentro esta asignación extremadamente útil cuando escribo el nombre de una constante larga como `MAX_CONNECTIONS_ALLOWED`. Escribo la constante en minúsculas y luego la transformo en mayúsculas con el mapeo en lugar de tener que cambiar todo el tiempo.

Agregue esa mapeo a su archivo ~ / .vimrc.

Configure un mapeo para que pueda poner en mayúsculas la palabra actual con <c-u> cuando se encuentre en el modo normal. Esto será ligeramente diferente al mapeo anterior porque no necesita ingresar al modo normal. Debería volver al modo normal al final, en lugar de al modo de inserción.

Agregue ese mapeo a su archivo ~ / .vimrc.
