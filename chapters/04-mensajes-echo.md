
# Mensajes echo

Las primeras partes de Vimscript que veremos son los comandos `echo` y `echom`.

Puede leer toda la documentación sobre estos dos comandos ejecutando `:help echo` y `:help echom` en Vim.
A medida que revise este libro, debe intentar leer la ayuda (`:help`) de cada comando nuevo que encuentre para aprender más sobre ellos.

Pruebe `echo` ejecutando el siguiente comando:

```vimscript
:echo "Hello, world!"
```
Debería ver aparecer el mensaje `Hello, world!` en la parte inferior de la ventana.

## echo persistente

Ahora pruebe `echom` ejecutando el siguiente comando.

```vimscript
:echom "Hello again, world!"
```

Debería ver aparecer el mensaje `Hello again, world!` en la parte inferior de la ventana.

Para ver la diferencia entre estos dos comandos, ejecute lo siguiente:

```vimscript
:messages
```
Debería ver una lista de mensajes, donde `¡hello, world!` no estará en esta lista, pero `¡hello again, world!` si estará en ella.

Cuando mas adelante en este libro escriba Vimscript cada vez más complicado, es posible que desee "imprimir algunos resultados" para ayudarlo a solucionar problemas. El viejo `:echo` imprimirá la salida, pero a menudo desaparecerá cuando el ciclo de vida de su script finalize. Usando `:echom` se guardará la salida y le permitirá ejecutar el comando `:messages` para verlo más tarde.

## Comentarios

Antes de continuar, veamos cómo agregar comentarios. Cuando escriba código de Vimscript (en su archivo `~/.vimrc` o en cualquier otro archivo) puede agregar comentarios con el carácter `"`. Vea este ejemplo:

```vimscript
" Make space more useful
nnoremap <space> za
```

Esto no siempre funciona (*ese es uno de esos rincones feos de Vimscript*), pero en la mayoría de los casos lo hace. Más adelante hablaremos de cuándo no funcionara (y por qué sucede eso).

### Ejercicios

Lea `:help echo`

Lea `:help echom`

Lea `:help :messages`

Agregue una línea a su archivo `~/.vimrc` que muestre un amigable gato ASCII-art (`> ^. ^ <`) Cada vez que abra Vim.
