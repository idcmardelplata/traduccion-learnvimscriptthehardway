
# Autocomandos

Ahora vamos a ver un tema casi tan importante como los mapeos y estos son los **autocomandos**.

Los autocomandos son una forma de decirle a Vim que ejecute ciertos comandos cada vez que ocurren ciertos eventos. Vamos a sumergirnos en un ejemplo.

Abra un archivo nuevo con `:edit foo` y ciérrelo inmediatamente con `:quit`. Mire en su disco duro y notará que el archivo no está allí. Esto se debe a que Vim no crea realmente el archivo hasta que lo guarde por primera vez.

Vamos a cambiarlo para que Vim cree archivos tan pronto como los edite.
Ejecute el siguiente comando:

```vimscript
:autocmd BufNewFile * :write
```

Esto es mucho para asimilar, pero pruébelo y verá que funciona.
Ejecute `:edit foo` nuevamente, ciérrelo con `:quit` y mire su disco duro. Esta vez el archivo estará allí (y vacío, por supuesto).

Tendrá que cerrar Vim para eliminar el autocommando. Hablaremos sobre cómo evitar esto en un capítulo posterior.

### Estructura de un autocomando

Echemos un vistazo más de cerca al autocomando que acabamos de crear:

```vimscript
:autocmd BufNewFile * :write
         ^          ^ ^
         |          | |
         |          | El comando a ejecutar
         |          |
         |          El "patron" para filtrar el evento.
         |
         El "evento" a vigilar.
```

La primera parte del comando es el tipo de evento que queremos ver. Vim ofrece muchos eventos para ver. Algunos de ellos incluyen:

* Empezando a editar un archivo que aún no existe.
* Leyendo un archivo, si existe o no.
* Cambiar la configuración del tipo de archivo de un búfer.
* No presionar una tecla en su teclado durante un cierto tiempo.
* Entrando en el modo de inserción.
* Saliendo del modo de inserción.

Esta es solo una pequeña muestra de los eventos disponibles. Hay muchos más que puede usar para hacer muchas cosas interesantes.

La siguiente parte del comando es un "patrón" que le permite ser más específico sobre cuándo desea que se active el comando. Inicie una nueva instancia de Vim y ejecute el siguiente comando:

```vimscript
:autocmd BufNewFile *.txt :write
```

Esto es casi lo mismo que el último comando, pero esta vez solo se aplicará a los archivos cuyos nombres terminen en `.txt`.

Pruébelo ejecutando `:edit bar`, luego `:quit` luego `:edit bar.txt`, luego `:quit`. Verá que Vim escribe `bar.txt` automáticamente, pero no escribe `bar` porque no coincide con el patrón.

La parte final del comando es el comando que queremos ejecutar cuando se activa el evento. Esto se explica por sí mismo, excepto por un problema: no puede usar caracteres especiales como `<cr>` en el comando.
Hablaremos sobre cómo sortear esta limitación más adelante en el libro, pero por ahora solo tendrá que vivir con eso.

### Otro ejemplo

Vamos a definir otro comando automático, esta vez usando un evento diferente. Ejecute el siguiente comando:

```vimscript
:autocmd BufWritePre *.html :normal gg=G
```

Nos estamos adelantando un poco a nosotros mismos porque hablaremos del comando `normal` más adelante en el libro, pero por ahora tendrá que soportarme porque es difícil encontrar ejemplos útiles en este momento.

Cree un nuevo archivo llamado `foo.html`. Edítelo con Vim e ingrese el siguiente texto exactamente, incluido el espacio en blanco:

```html
<html>
<body>
 <p>Hello!</p>
                 </body>
                  </html>
```

Ahora guarde este archivo con `:w`. ¿Que pasó? ¡Vim parece haber reindentado el archivo por nosotros antes de guardarlo!

Por ahora quiero que confíe en mí en que al ejecutar `:normal gg=G` le dirá a Vim que vuelva a reindentar el archivo actual.
No se preocupe por cómo funciona eso todavía.

Lo que sí queremos prestar atención es al `autocommand`.
El tipo de evento es `BufWritePre`, lo que significa que el evento se comprobará justo antes de guardar cualquier archivo.

Utilizamos un patrón de `*.html` para asegurarnos de que este comando solo se activará cuando estemos trabajando en archivos que terminan en `.html`.
Esto nos permite dirigir nuestros autocomandos a archivos específicos, lo cual es una idea muy poderosa que continuaremos explorando más adelante.

### Multiples Eventos

Puede crear un solo autocomando enlazado a múltiples eventos separando los eventos con una coma `,`.
Ejecute este comando:

```vimscript
:autocmd BufWritePre,BufRead *.html :normal gg=Gm
```

Esto es casi como nuestro último comando, excepto que también reindentará el código cada vez que leamos un archivo HTML y cuando lo guardemos. Esto podría ser útil si tiene compañeros de trabajo que no indentan muy bien su HTML.

Algo muy común en VimScript es emparejar los eventos `BufRead` y `BufNewFile` para ejecutar un comando cada vez que abra un determinado tipo de archivo, independientemente de si ya existe o no. Ejecute el siguiente comando:

```vimscript
:autocmd BufNewFile,BufRead *.html setlocal nowrap
```

Esto desactivará el ajuste de línea cuando esté trabajando en un archivo HTML.

### Eventos de Tipo de Archivos

Uno de los eventos más útiles es el evento `FileType`. Este evento se activa cada vez que Vim establece un tipo de archivo de búfer.

Vamos a configurar algunos mapeos útiles para una variedad de tipos de archivos. Ejecute los siguientes comandos:

```vimscript
:autocmd FileType javascript nnoremap <buffer> <localleader>c I//<esc>
:autocmd FileType python     nnoremap <buffer> <localleader>c I#<esc>
```

Abra un archivo Javascript (un archivo que termina en .js), elija una línea y escriba `<localleader> c`. Esto va a comentar la línea.

Ahora abra un archivo de Python (un archivo que termina en .py), elija una línea y escriba `<localleader> c`. ¡Esto comentará la línea, pero usará el carácter de comentario de Python!

Al usar los autocomandos junto con los mapeos locales de búfer que aprendimos en el capítulo anterior, podremos crear mapeos que son específicos para el tipo de archivo que estamos editando.

Esto reduce la carga en nuestras mentes cuando estamos programando. En lugar de tener que pensar en pasar al principio de la línea y agregar un carácter de comentario, simplemente podemos pensar "comentar esta línea".

### Ejercicios

Examine rapidamente :`help autocmd-events` para ver una lista de todos los eventos a los que puede enlazar sus autocomandos.
No necesita memorizar cada uno en este momento. Solo trate de familiarizarse con el tipo de cosas que puede hacer.

Cree algunos comandos automáticos de `FileType` que usen `setlocal` para configurar las opciones para sus tipos de archivo favoritos de la manera que más le guste.
Algunas opciones que le gustaría cambiar por tipo de archivo son `wrap`, `list`, `spell` y `number`.

Cree unos cuantos autocomandos de "comentar esta línea" para los tipos de archivos con los que trabaja con frecuencia.

Agregue todos estos comandos automáticos a su archivo `~/.vimrc`. ¡Utilice los mapeos de accesos directos para editarlo y recargarlo rápidamente, por supuesto!
