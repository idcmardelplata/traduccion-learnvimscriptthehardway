
# Programación Funcional

Vamos a tomar un breve descanso ahora para hablar sobre un estilo de programación del que quizás haya oído hablar: La [ programación funcional ](https://secure.wikimedia.org/wikipedia/en/wiki/Functional_programming).

Si ha programado en lenguajes como Python, Ruby o Javascript, o especialmente Lisp, Scheme, Clojure o Haskell, probablemente esté familiarizado con la idea de usar funciones como variables y usar estructuras de datos con estado inmutable. Si nunca antes ha hecho esto, puede saltarse este capítulo de manera segura, ¡pero le animo a que lo pruebe de todos modos y amplíe sus horizontes!

Vimscript tiene todas las piezas necesarias para programar en un estilo funcional, pero es un poco torpe. Sin embargo, podemos crear algunos ayudantes que lo harán menos doloroso.

Continúe y cree un archivo `funcional.vim` en algún lugar para que no tenga que seguir escribiendo todo una y otra vez. Este archivo será nuestro bloc de notas para este capítulo.

### Estructuras de datos inmutables

Desafortunadamente, Vim no tiene colecciones inmutables integradas como los vectores y mapas de Clojure, pero al crear algunas funciones auxiliares podemos fingirlo hasta cierto punto.

Agregue la siguiente función a su archivo:

```vimscript
function! Sorted(l)
    let new_list = deepcopy(a:l)
    call sort(new_list)
    return new_list
endfunction
```
Guarde y recargue el archivo, luego ejecute `:echo Sorted ([3, 2, 4, 1])` para probarlo.
Vim hace `echo [1, 2, 3, 4]`.

¿Cómo es esto diferente de  llamar simplemente a la función incorporada `sort()`?
La clave es la primera línea: `let new_list = deepcopy (a: l)`.
Vim's `sort()` ordena la lista en su lugar, por lo que primero creamos una copia completa de la lista y la ordenamos para que la lista original no se cambie.


Esto evita los efectos secundarios y nos ayuda a escribir código que sea más fácil de razonar y probar.
Agreguemos algunas funciones auxiliares más siguiendo este mismo estilo:

```vimscript
function! Reversed(l)
    let new_list = deepcopy(a:l)
    call reverse(new_list)
    return new_list
endfunction

function! Append(l, val)
    let new_list = deepcopy(a:l)
    call add(new_list, a:val)
    return new_list
endfunction

function! Assoc(l, i, val)
    let new_list = deepcopy(a:l)
    let new_list[a:i] = a:val
    return new_list
endfunction

function! Pop(l, i)
    let new_list = deepcopy(a:l)
    call remove(new_list, a:i)
    return new_list
endfunction
```

Cada una de estas funciones es exactamente la misma,
excepto por la línea media y los argumentos que toman.
Guarde y recargue el archivo y pruébelos con algunas listas.


* `Reversed()` toma una lista y devuelve una nueva lista con los elementos invertidos.
* `Append()` devuelve una nueva lista con el nuevo valor agregado al final de la lista anterior.
* `Assoc()` (abreviatura de "asociar") devuelve una nueva lista, con el elemento en el índice dado reemplazado por el nuevo valor.
* `Pop()` devuelve una nueva lista con el elemento en el índice dado eliminado.

### Funciones como variables

Vimscript admite el uso de variables para almacenar funciones,
pero la sintaxis es un poco obtusa.
Ejecute los siguientes comandos:

```vimscript
:let Myfunc = function("append")
:echo Myfunc([1, 2], 3)
```

Vim mostrará `[1, 2, 3]` como se esperaba.
Observe que la variable que usamos comenzó con una letra mayúscula.
Si una variable Vimscript se refiere a una función, esta debe comenzar con una letra mayúscula.

Las funciones se pueden almacenar en listas como cualquier otro tipo de variable.
Ejecute los siguientes comandos:

```vimscript
:let funcs = [function("Append"), function("Pop")]
:echo funcs[1](['a', 'b', 'c'], 1)
```

Vim muestra `['a', 'c']`. La variable `funcs` no necesita comenzar con una letra mayúscula porque está almacenando una lista, no una función.
El contenido de la lista no importa en absoluto.

### Funciones de orden superior

Creemos algunas de las funciones de orden superior más utilizadas.
Si no está familiarizado con ese término, las funciones de orden superior son funciones que toman otras funciones y hacen algo con ellas.

Comenzaremos con la confiable función `map`.
Agregue esto a su archivo:

```vimscript
function! Mapped(fn, l)
    let new_list = deepcopy(a:l)
    call map(new_list, string(a:fn) . '(v:val)')
    return new_list
endfunction
```

Guarde y recargue el archivo, y pruébelo ejecutando los siguientes comandos:

```vimscript
:let mylist = [[1, 2], [3, 4]]
:echo Mapped(function("Reversed"), mylist)
```

Vim muestra `[[2, 1], [4, 3]]`, que es el resultado de aplicar `Reversed()` a cada elemento de la lista.

¿Cómo funciona `Mapped()`? Una vez más, creamos una lista nueva con `deepcopy()`,
le hacemos algo y devolvemos la copia modificada, nada nuevo allí.
La parte difícil es la del medio.

`Mapped()` toma dos argumentos: un `funcref` (término de Vim para una "variable que almacena una función") y una lista.
Utilizamos la función `map()` incorporada para realizar el trabajo real.
Lea `:help map()` para ver cómo funciona.

Ahora crearemos algunas otras funciones de orden superior bastante comunes.
Agregue lo siguiente a su archivo:

```vimscript
function! Filtered(fn, l)
    let new_list = deepcopy(a:l)
    call filter(new_list, string(a:fn) . '(v:val)')
    return new_list
endfunction
```

Pruebe `Filtered()` con los siguientes comandos:

```vimscript
:let mylist = [[1, 2], [], ['foo'], []]
:echo Filtered(function('len'), mylist)
```
Vim muestra `[[1, 2], ['foo']]`.

`Filtered()` toma una función de predicado (que no es mas que una función que retorna un `0` o un `1`) y una lista.
Devuelve una copia de la lista que contiene solo los elementos de la lista original donde el resultado de llamar a la función es "verdadero".
En este caso, utilizamos la función incorporada `len()`, por lo que esta filtra todos los elementos cuya longitud es cero.

Finalmente crearemos la contraparte de `Filtered()`:

```vimscript
function! Removed(fn, l)
    let new_list = deepcopy(a:l)
    call filter(new_list, '!' . string(a:fn) . '(v:val)')
    return new_list
endfunction
```

Pruébelo como lo hicimos con `Filtered()`:

```vimscript
:let mylist = [[1, 2], [], ['foo'], []]
:echo Removed(function('len'), mylist)
```

Vim muestra `[[], []]`.
`Removed()` es como `Filtered()` excepto que solo mantiene elementos donde la función de predicado no devuelve algo verdadero.

La única diferencia en el código es el sencillo `'!'` que agregamos al comando `call`, este operador niega el resultado del predicado.

### Performance

Tal vez piense que copiar listas en todo el lugar es un desperdicio, ya que Vim tiene que crear constantemente nuevas copias y recolectar las viejas.

Si es así: ¡tienes razón! Las listas de Vim no admiten el mismo tipo de intercambio estructural que los vectores de Clojure,
por lo que todas esas operaciones de copia pueden ser costosas.

A veces esto importará. Si está trabajando con listas enormes, las cosas pueden disminuir.
Sin embargo, en la vida real, es posible que se sorprenda de lo poco que realmente notará la diferencia.

Considere esto: mientras escribo este capítulo, mi programa Vim está utilizando aproximadamente 80 megabytes de memoria (y tengo muchos complementos instalados).
Mi computadora portátil tiene 8 gigabytes de memoria.
¿La sobrecarga de tener algunas copias de una lista realmente va a hacer una diferencia notable? Por supuesto, eso depende del tamaño de la lista, pero en la mayoría de los casos la respuesta será "no".

Por el contrario, mi instancia de Firefox con cinco pestañas abiertas actualmente usa 1.22 gigabytes de RAM.

Deberá usar su propio criterio sobre cuándo este estilo de programación crea un rendimiento inaceptablemente malo.

### Ejercicios

* Lea `:help sort()`
* Lea `:help reverse()`
* Lea `:help copy()`
* Lea `:help deepcopy()`
* Lea `:help map()` (si todavía no lo ha echo)
* Lea `:help function()`

Modifique `Assoc()`, `Pop()`, `Mapped()`, `Filtered()` y `Removed()` para admitir diccionarios.
Probablemente necesitará leer `:help type()` para lograrlo.

Implemente `Reduced()`

Tómese un vaso de su bebida favorita. ¡Este capítulo fue intenso!
