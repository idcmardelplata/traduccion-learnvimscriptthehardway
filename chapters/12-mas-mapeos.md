
# Más Mapeos

Sé que hemos hablado mucho sobre mapeos hasta ahora, pero vamos a practicarlos de nuevo una vez más. Los mapeos son una de las maneras más fáciles y rápidas de hacer que su edición con Vim sea más productiva, por lo que es bueno centrarse en ellas un poco.

Un concepto que se ha mostrado en varios ejemplos, pero del que no hemos hablado explícitamente, es mapear una secuencia de múltiples teclas.

Ejecute el siguiente comando:

```vimscript
:nnoremap jk dd
```

Ahora asegúrese de que está en modo normal y presione `j` seguido rápidamente por `k`. Vim borrará la línea actual.

Ahora intente presionar solo `j` y esperar un poco. Si no presiona `k` rápidamente después de la `j`, Vim decide que no desea activar el mapeo y en su lugar ejecuta la funcionalidad `j` normal (bajando una línea).

Este mapeo hará que sea doloroso moverse, así que eliminémoslo.
Ejecute el siguiente comando:

```vimscript
:nunmap jk
```

Ahora, al escribir `jk` en el modo normal, se moverá hacia abajo y subirá una línea como de costumbre.

### Un mapeo más complicado

Ha visto un montón de mapeos simples hasta ahora, así que es hora de mirar algo con un poco más de carne.
Ejecute el siguiente comando:

```vimscript
:nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel
```

Ahora si que es un mapeo interesante! Primero, adelante y pruébelo. Ingrese al modo normal, coloque el cursor sobre una palabra en su texto y escriba `<leader>`". ¡Vim rodeará la palabra entre comillas dobles!

¿Como funciona esto? Vamos a dividirlo en pedazos y pensar en lo que hace cada parte:

```vimscript
viw<esc>a"<esc>bi"<esc>lel
```

* `viw`: selecciona visualmente la palabra actual
* `<esc>`: sale del modo visual, que deja el cursor en el último carácter de la palabra
* `a`: entrar en el modo inserción después del carácter actual
* `"`: inserta una `"` en el texto, porque estamos en modo de inserción
* `<esc>`: volver al modo normal
* `b`: retroceder al principio de la palabra
* `i`: entrar en el modo de inserción antes del carácter actual
* `"`: inserta una `"` en el texto de nuevo
* `<esc>`: volver al modo normal
* `l`: mover hacia la derecha, lo que pone nuestro cursor en el primer carácter de la palabra
* `e`: mover al final de la palabra
* `l`: mover hacia la derecha, lo que pone nuestro cursor sobre la cita final

Recuerde: como usamos `nnoremap` en lugar de `nmap`, no importa si ha mapeado alguna de las teclas de esta secuencia a otra cosa. Vim usará la funcionalidad por defecto para todos ellos.

Esperemos que pueda ver cuánto potencial tienen los mapeos de Vim, así como cuán ilegibles pueden llegar a ser.

### Ejercicios

Cree un  mapeo similar al a que acabamos de ver, pero para comillas simples en lugar de comillas dobles.

Intente usar `vnoremap` para agregar una asignación que incluya el texto que ha seleccionado visualmente entre comillas. Probablemente necesitará los comandos `<` y `>` para esto, así que lea sobre ellos con `:help <.`

Mape `H` en modo normal para ir al principio de la línea actual. Como `h` se mueve a la izquierda, puedes pensar en `H` como una `h` "más fuerte".

Mapa `L` en modo normal para ir al final de la línea actual. Ya que `l` se mueve a la derecha, puedes pensar en `L` como una `l` "más fuerte" .

Averigüe qué comandos acaba de sobrescribir al leer `:help H` y `:help L`. Decida si le interesan.

Agregue todos estos mapeos a su archivo `~/.vimrc`, asegurándose de usar sus mapeos "editar mi `~/.vimrc"` y "source mi `~/.vimrc`" para hacerlo.
