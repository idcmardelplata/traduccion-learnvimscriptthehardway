# Teclas leaders

Hemos aprendido cómo  mapear teclas de una manera que no nos hará querer arrancarnos los pelos más tarde, pero es posible que haya notado un problema más.

Cada vez que hacemos algo como `:nnoremap <space> dd` sobrescribimos lo que normalmente hace `<space>`. ¿Y si necesitamos esa tecla más tarde?

Hay un montón de teclas que normalmente no necesitamos en nuestro uso diario de Vim. `-`, `H`, `L`, `<space>,` `<cr>` y `<bs>` hacen cosas que casi nunca necesitas (en el modo normal, por supuesto). Dependiendo de cómo trabaje, puede encontrar otros que nunca usa.

Esos son seguros de mapear, pero eso solo nos da seis claves para trabajar. ¿Qué pasó con la personalización legendaria de Vim?

### Mapeando secuencias de teclas

A diferencia de Emacs, Vim hace que sea más fácil mapear más que una simple tecla. Ejecute estos comandos:

```vimscript
:nnoremap -d dd
:nnoremap -c ddO
```

Pruébelos escribiendo `-d` y `-c` (rápidamente) en el modo normal. El primero crea un mapeo personalizado para eliminar una línea, mientras que el segundo "borra" una línea y lo pone en modo de inserción.

Esto significa que puede elegir una tecla que no le importe (como `-`) como una clave de "prefijo" y crear mapeos sobre ella.

Esto significa que tendrá que escribir una tecla adicional para activar los mapeos, pero una pulsación adicional se puede absorber fácilmente en la memoria muscular.

Si crees que esto puede ser una buena idea, tienes razón, ¡y resulta que Vim ya tiene mecanismos para esta clave de "prefijo"!

### Tecla Leader

Vim llama a esta tecla de "prefijo" `"leader"`. Puede establecer su tecla leader para lo que quiera. Ejecute este comando:

```vimscript
:let mapleader = "-"
```
Puede reemplazarlo con cualquier tecla que le guste. Personalmente me gusta `,` aunque oculte una función útil, porque es muy fácil de escribir.

Cuando está creando nuevos mapeos, puede usar `<leader>` para decir: "lo que sea que tenga configurado mi tecla leader para...". Ejecute este comando:

```vimscript
:nnoremap <leader>d dd
```

Ahora inténtalo presionando la tecla de `leader` y luego `d`. Vim borrará la línea actual.

¿Por qué molestarse en establecer `<leader>`? ¿Por qué no incluir su clave de "prefijo" directamente en sus comandos de mapeo? Hay tres buenas razones.

En primer lugar, puede decidir que necesita la función normal de su `leader` más tarde. Definirlo en un solo lugar hace que sea fácil cambiarlo más tarde.

En segundo lugar, cuando alguien más esté mirando su archivo `~/.vimrc`, sabrán de inmediato lo que quiere decir cuando dice `<leader>`. Simplemente pueden copiar su asignación en su propio `~/.vimrc` si les gusta, incluso si usan un `leader` diferente.

Finalmente, muchos plugins de Vim crean mapeos que comienzan con `<leader>.` Si ya lo tiene configurado, funcionarán correctamente y se sentirán familiares desde el primer momento.

### LocalLeader

Vim tiene una segunda clave "leader" llamada "localleader". Esto pretende ser un prefijo para los mapeos que solo surten efecto para ciertos tipos de archivos, como los archivos Python o los archivos HTML.

Hablaremos sobre cómo hacer mapeos para tipos específicos de archivos más adelante en el libro, pero puede seguir adelante y configurar su "localleader" ahora:

```vimscript
:let maplocalleader = "\\"
```

Observe que tenemos que usar `\\` y no solo `\` porque `\` es el carácter de escape en las cadenas de Vimscript. Aprenderás más sobre esto más adelante.

Ahora puede usar `<localleader>` en los mapeos y funcionará igual que `<leader>` (excepto para resolver una tecla diferente, por supuesto).

Siéntase libre de cambiar esta tecla a otra cosa si no le gusta la barra invertida.

### Ejercicios

Lea `:help mapleader`.
Lea `:help maplocalleader`.

Establezca `mapleader` y `maplocalleader` en su archivo `~/.vimrc`.

Transforme todas los mapeos que agregó a su archivo `~/.vimrc` en los capítulos anteriores para que usen el prefijo `<leader>`, de ese modo se evitara opacar los comandos existentes.
