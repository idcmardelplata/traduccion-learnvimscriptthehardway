
# Opciones de configuración

Vim tiene muchas opciones que puede configurar para cambiar cómo se comporta.

Hay dos tipos principales de opciones:
* opciones booleanas (ya sea `"on"` o `"off"`)
* opciones que toman un valor.

## Opciones Booleanas

Ejecute el siguiente comando:

```vimscript
:set number
```

Los números de línea deberían aparecer en el lado izquierdo de la ventana si aún no estaban allí.
Ahora ejecute esto:

```vimscript
:set nonumber
```

Los números de línea deben desaparecer.
`number` es una opción booleana que puede estar desactivado o activado.
Lo activa "on" ejecutando el comando `:set number` y lo desactiva "off" con el comando `:set nonumber`.

Todas las opciones booleanas funcionan de esta manera. `:set <nombre>` activa la opción y `:set no<nombre>` la desactiva.

## Alternar opciones booleanas

También puede "alternar" las opciones booleanas para establecerlas en el valor opuesto al que contengan ahora.

Ejecute esto:

```vimscript
:set number!
```

Los números de línea deberían reaparecer, ahora ejecútelo de nuevo:

```vimscript
:set number!
```

Deberían desaparecer una vez más.

Añadiendo un `!` (signo de exclamación o "bang") a una opción booleana puede alternar entre sus dos valores.

### Opciones de verificación

Puede preguntarle a Vim que valor esta almacenado actualmente en una opción utilizando un `?`.
Ejecute estos comandos y observe lo que sucede después de cada uno:

```vimscript
:set number
:set number?
:set nonumber
:set number?
```

Observe cómo la primera ves comando `:set number?` mostró `number` mientras que la segunda mostró `nonumber`.

### Opciones con valores

Algunas opciones tienen un valor en lugar de estar apagadas o encendidas. Ejecute los siguientes comandos y observe lo que sucede después de cada uno:

```vimscript
:set number
:set numberwidth=10
:set numberwidth=4
:set numberwidth?
```

La opción `numberwidth` cambia el ancho de la columna que contiene los números de línea. Puede cambiar las opciones no booleanas con `:set <name> = <value>`, y verificarlas de la manera habitual (`:set <name>?`).

Intente verificar qué otras opciones comunes están configuradas para:

```vimscript
:set wrap?
:set shiftround?
:set matchtime?
```

### Configuración de múltiples opciones a la vez

Finalmente, puede especificar más de una opción con el comando `:set` y así lograr aligerar un poco la escritura.

Intente ejecutar esto:

```vimscript
:set numberwidth=2
:set nonumber
:set number numberwidth=6
```

Observe cómo ambas opciones fueron establecidas y surtieron efecto en el último comando.

### Ejercicios

Lea `:help 'number'` (note las comillas simples).

Lea `:help relativenumber`.

Lea `:help numberwidth`.

Lea `:help wrap`.

Lea `:help shiftround`.

Lea `:help matchtime`.

Agregue algunas líneas a su archivo `~/.vimrc` para configurar estas opciones como desee.
