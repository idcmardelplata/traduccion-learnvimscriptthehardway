
# Caso de Estudio: Operador de Grep, Parte Dos

Ahora que tenemos un bosquejo preliminar de nuestra solución, es hora de desarrollarlo en algo poderoso.

Recuerde: nuestro objetivo original era crear un "operador grep".
Hay muchas cosas nuevas que debemos cubrir para hacer esto, pero vamos a seguir el mismo proceso que hicimos en el capítulo anterior:
comience con algo simple y transfórmelo hasta que haga lo que necesita.

Antes de comenzar, comente el mapeo que hemos creado en el capítulo anterior en su archivo `~/.vimrc`: usaremos la misma pulsación de teclas para nuestro nuevo operador.

## Crear un archivo

Crear un operador tomará una serie de comandos y escribirlos a mano se volverá tedioso muy rápidamente.
Podría agregarlo a su archivo `~/.vimrc`, pero crearemos un archivo separado solo para este operador.
Es lo suficientemente carnoso para garantizar un archivo propio.

Primero, encuentre su directorio de Vim `plugins`. En Linux u OS X, esto estará en `~/.vim/plugin`. Si está en Windows, estará dentro del directorio `vimfiles` en su directorio de inicio. (Use el comando:: `echo $HOME` en Vim si no está seguro de dónde se encuentra). Si este directorio no existe, créalo.

Dentro del directorio `plugin/` crearemos un archivo llamado `grep-operator.vim`. Aquí es donde colocará el código para este nuevo operador.
Cuando edite el archivo puede ejecutar: `source %` para recargar el código en cualquier momento.
Este archivo también se cargará cada vez que inicie Vim al igual que lo hace `~/.vimrc`.

¡Recuerde que debe escribir el archivo antes de recargarlo para ver los cambios!

## Esqueleto

Para crear un nuevo operador Vim, deberá comenzar con dos componentes: una función y un mapeo.
Comience agregando el siguiente código a `grep-operator.vim`:


```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@

function! GrepOperator(type)
    echom "Test"
endfunction
```

Escriba el archivo y recarguelo con `:source %`. Pruébelo presionando `<leader>giw` para decir "grep inner word".
Vim realizará un `echo "Test"` después de aceptar el movimiento `iw`, lo que significa que hemos dispuesto el armazón.


La función es simple y no tiene nada que no hayamos visto antes, pero ese mapeo es un poco más complicado.
Primero establecemos la opción `operatorfunc` a nuestra función, y luego ejecutamos `g@` que llama a esta función como operador.
Esto puede parecer un poco complicado, pero es cómo funciona Vim.

Por ahora está bien considerar que este mapeo es magia negra.
Puede profundizar en la documentación detallada más adelante.

## Modo visual

Hemos agregado el operador al modo normal, pero también queremos usarlo desde el modo visual.
Agregue otro mapeo debajo del primero:


```vimscript
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>
```

Guárdelo y recargue el archivo.
Ahora seleccione visualmente algo y presione `<leader>g`.
No pasa nada, pero Vim hace un `echo "Test"`, por lo que nuestra función está siendo llamada.

Hemos visto el `<c-u>` en este mapeo antes, pero nunca explicamos lo que hace.
Intente seleccionar visualmente un texto y presione:. Vim abrirá una línea de comando como lo hace normalmente cuando se presiona `:`
¡pero se llena automáticamente con `'<,'>` al principio de la línea!

Vim está tratando de ser útil e inserta este texto para que el comando que está a punto de ejecutar funcione en el rango seleccionado visualmente.
En este caso, sin embargo, no queremos la ayuda.
Usamos `<c-u>` para decir "eliminar del cursor al principio de la línea", eliminando el texto. Esto nos deja con un simple `:`, listo para el comando `call`.


El `call GrepOperator()` es simplemente una llamada de función como hemos visto antes, pero el `visualmode()` que estamos pasando como argumento es nuevo.
Esta función es una función nativa de Vim que retorna una string de un carácter que representa el último tipo de modo visual usado: `"v"` para carácter, `"V"` para línea y un carácter `Ctrl-v` para bloque.

## Tipos de movimiento

La función que definimos toma un argumento `type`. Sabemos que cuando usamos el operador desde el modo visual, será el resultado de `visualmode()`,
pero ¿qué ocurre cuando lo ejecutamos como operador desde el modo normal?


Edite el cuerpo de la función para que el archivo se vea así:


```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>

function! GrepOperator(type)
    echom a:type
endfunction
```

Recargue el archivo, continúe y pruébelo de varias maneras. Algunos ejemplos de la salida que obtiene son:


* Al presionar `viw<leader>g` se hace echo de `v` porque estábamos en modo visual de caracteres.
* Al presionar `Vjj<leader>g` se hace echo de `V` porque estábamos en modo visual lineal.
* Al presionar `<leader>giw` se hace echo de `char` porque usamos un movimiento de carácter con el operador.
* Al presionar `<leader>gG` se hace echo de `line` porque usamos un movimiento lineal con el operador.

Ahora sabemos cómo podemos distinguir la diferencia entre los tipos de movimiento,
lo que será importante cuando seleccionemos el texto para buscar.

## Copiando el Texto

Nuestra función necesitará obtener de alguna manera acceso al texto que el usuario desea buscar,
y la forma más fácil de hacerlo es simplemente copiarlo.
Edite la función para que se vea así:


```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>

function! GrepOperator(type)
    if a:type ==# 'v'
        execute "normal! `<v`>y"
    elseif a:type ==# 'char'
        execute "normal! `[v`]y"
    else
        return
    endif

    echom @@
endfunction
```


Wow. Eso es un montón de cosas nuevas.
Pruébelo presionando cosas como `<leader>giw`, `<leader>g2e` y `vi(<leader> g)`.
Cada vez Vim hará `echo` del texto que cubre el movimiento, ¡así que claramente estamos progresando!

Vamos a desglosar este nuevo código paso a paso.
Primero tenemos una sentencia `if` que verifica el argumento `a:type.` Si el tipo es `'v'`, se llamó desde el modo visual,
por lo que hacemos algo para copiar el texto seleccionado visualmente.


Observe que usamos la comparación que distingue entre mayúsculas y minúsculas `==#`.
Si utilizamos plana `==` y el usuario tiene establecido un `ignorecase`, también coincidiría con `"V"`,
que no es lo que queremos. Código defensivo!

El segundo caso de `if` se activa si se llamó al operador desde el modo normal mediante un carácter de movimiento.

El caso final simplemente retorna. Ignoramos explícitamente los casos de `modo visual en línea / bloque` y `movimientos en línea / bloque`.
Grep no busca entre líneas por defecto, por lo que tener una nueva línea en el patrón de búsqueda no tiene ningún sentido.

Cada uno de nuestros dos casos `if` ejecutan el comando `normal!` que hace dos cosas:


* Selecciona visualmente el rango de texto que deseamos via:
    * Mover la marca al comienzo del rango.
    * Entrar al modo visual
    * Moverse a la marca al final del rango

* Copiando el texto seleccionado.

No se preocupe acerca de marcas especificas por ahora. Aprenderá por qué deben ser diferentes cuando complete los ejercicios al final de este capítulo.

La linea final de la función hace un `echom` de la variable `@@`. Recuerde que las variables que comienzan con un `@` son registros.

`@@` es el registro "sin nombre": es aquel registro en el que Vim coloca el texto cuando copia o elimina sin especificar un registro particular.

En pocas palabras: seleccionamos el texto a buscar, lo copiamos y luego hacemos `echo` del texto copiado.

## Escapando el termino de búsqueda
hora que tenemos el texto que necesitamos en un string de Vim, podemos escaparlo como lo hicimos en el capítulo anterior.
Modifique el comando `echom` para que se vea así:

```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>

function! GrepOperator(type)
    if a:type ==# 'v'
        normal! `<v`>y
    elseif a:type ==# 'char'
        normal! `[v`]y
    else
        return
    endif

    echom shellescape(@@)
endfunction
```

Guarde y recargue el archivo, y pruebelo seleccionando visualmente un texto con algún carácter especial en el y presione `<leader>g`. Vim hará un
`echom` con una versión adecuada del texto seleccionado para pasar a un comando de shell.

## Corriendo Grep

¡Finalmente estamos listos para agregar el comando `grep`! Que realizara la búsqueda real.
Reemplace la linea `echom` para que nuestro código se vea así:

```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>

function! GrepOperator(type)
    if a:type ==# 'v'
        normal! `<v`>y
    elseif a:type ==# 'char'
        normal! `[v`]y
    else
        return
    endif

    silent execute "grep! -R " . shellescape(@@) . " ."
    copen
endfunction
```

Esto debería parecerle familiar. Simplemente ejecutamos el comando `silent grep!` que se nos ocurrió en el ultimo capitulo.
¡Es aun mas legible aquí porque no estamos tratando de meter todo en un comando `nnoremap`!.

¡Guarde y recargue el archivo, luego pruébelo y disfrute de los frutos de su trabajo!

Debido a que hemos definido un nuevo operador de Vim, podemos usarlo de muchas maneras diferentes, como por ejemplo:

* viw<leader>g : Visualmente selecciona una palabra, y luego la busca con el comando grep.
* <leader>g4w  : Busca las proximas 4 palabras
* <leader>gt;  : Busca hasta el `;`
* <leader>gi[  : Busca dentro de corchetes.

Esto destaca una de las mejores cosas de Vim: sus comandos de edición son como un lenguaje.
Cuando agrega un nuevo verbo, funciona automáticamente con (la mayoría de) los sustantivos y adjetivos existentes.

## Ejercicios

* Lea `:help visualmode()`
* Lea `:help c_ctrl_u`
* Lea `:help operatorfunc`
* Lea `:help map-operator`
