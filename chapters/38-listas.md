
# Listas

Hasta ahora hemos trabajado mucho con las variables, ¡pero aún no hemos hablado sobre los tipos agregados!
Vim tiene dos tipos de datos agregados principales, y veremos el primero ahora: **las listas**.

Las listas de Vimscript son colecciones ordenadas y heterogéneas de elementos. Ejecute el siguiente comando:

```vimscript
:echo ['foo', 3, 'bar']
```

Vim muestra la lista. Las listas pueden por supuesto, estar anidadas. Ejecute el siguiente comando:

```vimscript
:echo ['foo', [3, 'bar']]
```

Vim nuevamente muestra la lista.

## Indexando


Las listas de Vimscript están indexadas a cero y puede acceder a los elementos de la forma habitual.
Ejecute este comando:

```vimscript
:echo [0, [1, 2]][1]
```
Vim muestra `[1, 2]`. También puede indexar desde el final de la lista, al igual que Python.
Pruebe este comando:

```vimscript
:echo [0, [1, 2]][-2]
```

Vim muestra 0. El índice -1 se refiere al último elemento de la lista, -2 es el penúltimo, y así sucesivamente.

## Slicing

Las listas de Vim también se pueden cortar.
Esto les resultará familiar a los programadores de Python, ¡pero no siempre actúa de la misma manera! Ejecute este comando:


```vimscript
:echo ['a', 'b', 'c', 'd', 'e'][0:2]
```

Vim muestra los elementos `['a', 'b', 'c']` (elementos que se corresponden con el  0, 1 y 2).
También puede superar con seguridad el límite superior. Pruebe este comando:

```vimscript
:echo ['a', 'b', 'c', 'd', 'e'][0:100000]
```

Vim simplemente muestra la lista entera.

Los índices de corte pueden ser negativos. Pruebe este comando:


```vimscript
:echo ['a', 'b', 'c', 'd', 'e'][-2:-1]
```

Vim muestra `['d', 'e']` (elementos -2 y -1).

Al dividir listas, puede omitir el primer índice que significa "el principio" y / o el último índice que significa "el final".
Ejecute los siguientes comandos:

```vimscript
:echo ['a', 'b', 'c', 'd', 'e'][:1]
:echo ['a', 'b', 'c', 'd', 'e'][3:]
```

Vim muestra `['a', 'b']` y `['d', 'e']`.

Al igual que Python, Vimscript le permite indexar y cortar cadenas también.
Ejecute el siguiente comando:

```vimscript
:echo "abcd"[0:2]
```

Vim muestra `abc`.
Sin embargo, no puede usar índices desnudos negativos con cadenas.
¡Pero puede usar índices negativos al cortar cadenas usando slicing! Ejecute el siguiente comando:

```vimscript
:echo "abcd"[-1] . "abcd"[-2:]
```

Vim muestra `cd` (el uso de un índice negativo en un slice resultó en una cadena vacía).

## Concatenación


Puede combinar listas de Vim con `+`.
Pruebe este comando:

```vimscript
:echo ['a', 'b'] + ['c']
```

Vim, como era de esperar, muestra `['a', 'b', 'c']`.
No hay mucho más que decir aquí: las listas de Vimscript son sorprendentemente sensatas en comparación con el resto del lenguaje.

## Funciones de listas


Vim tiene una serie de funciones integradas para trabajar con listas.
Ejecute estos comandos:

```vimscript
:let foo = ['a']
:call add(foo, 'b')
:echo foo
```

Vim muta la lista `foo` para agregar `'b'` y muestra `['a', 'b']`.
Ahora ejecute este comando:

```vimscript
:echo len(foo)
```

Vim muestra `2` que es la longitud de la lista.
Pruebe estos comandos:

```vimscript
:echo get(foo, 0, 'default')
:echo get(foo, 100, 'default')
```

Vim muestra un `a` y un `default`.
La función `get` obtendrá el elemento en el índice dado de la lista dada,
o devolverá el valor predeterminado dado si el índice está fuera del rango en la lista.

Ejecute este comando:


```vimscript
:echo index(foo, 'b')
:echo index(foo, 'nope')
```

Vim muestra `1` y `-1`.
La función `index` retorna el primer índice del elemento dado en la lista dada, o `-1` si el elemento no está en la lista.

Ahora ejecute este comando:

```vimscript
:echo join(foo)
:echo join(foo, '---')
:echo join([1, 2, 3], '')
```

Vim muestra `ab`, `a---b` y `123`. `join` unirá los elementos de la lista dada en un string, separado por el string de separación dado
(o un espacio si no se da ninguno), coaccionando cada elemento a un string si es necesario y posible.


Ejecute los siguientes comandos:


```vimscript
:call reverse(foo)
:echo foo
:call reverse(foo)
:echo foo
```

Vim muestra `['b', 'a']` y luego `['a', 'b']`. `reverse` invierte la lista dada.

## Ejercicios

* Lea `:help List` por completo (Note la L capital)
* Lea `:help add()`
* Lea `:help len()`
* Lea `:help get()`
* Lea `:help index()`
* Lea `:help join()`
* Lea `:help reverse()`

Lea también `:help functions` para encontrar algunas otras funciones relacionadas con las listas que no hemos mencionado todavia.


Ejecute `:match Keyword /\clist/` para resaltar en mayúsculas y minúsculas la palabra `list`,
esto ayuda a que sea más fácil encontrar lo que estás buscando.
