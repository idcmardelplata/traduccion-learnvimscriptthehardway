
# Ámbito de Variabes

Hasta ahora, las variables de Vimscript le pueden parecer familiares si proviene de un lenguaje dinámico como Python o Ruby. En su mayor parte, las variables actúan como usted esperaría, pero Vim agrega un cierto giro a las variables: *El Ámbito*.

Abra dos archivos diferentes en divisiones separadas, luego vaya a uno de ellos y ejecute los siguientes comandos:

```vimscript
:let b:hello = "world"
:echo b:hello
```

Como era de esperar, Vim muestra `world`. Ahora cambie al otro búfer y ejecute nuevamente el comando `echo`:

```vimscript
:echo b:hello
```

Esta vez Vim lanza un error, diciendo que no puede encontrar la variable.

Cuando usamos `b:` en el nombre de la variable le decimos a Vim que la variable `hello` debería ser local al búfer actual.

Vim tiene muchos ámbitos diferentes para las variables, pero necesitamos aprender un poco más sobre Vimscript antes de poder aprovechar el resto. Por ahora, solo recuerda que cuando vea una variable que comienza con un carácter y dos puntos, está describiendo una variable con ambito.

### Ejercicios

Navegue por la lista de ámbitos con `:help internal-variables`. No se preocupe si no sabe lo que significan algunas de ellas, simplemente eche un vistazo y manténgalas en el fondo de su mente.
