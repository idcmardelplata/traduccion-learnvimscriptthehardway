
# Crear un plugin completo

Hemos cubierto mucho terreno en los últimos cuarenta capítulos.
En la parte final del libro, veremos cómo crear un plugin de  Vim para un lenguaje de programación desde cero.

Esto no es para los débiles de corazón. Va a tomar mucho esfuerzo.

Si quiere parar ahora, ¡está completamente bien!
Ya ha aprendido lo suficiente como para hacer mejoras serias en su propio archivo `~/.vimrc`
y para corregir los errores que encuentre en los plugins de otras personas.

No hay vergüenza en decir "eso es suficiente para mí, no quiero pasar horas de mi vida creando plugins que no usaré muy a menudo".
Sea práctico. Si no puede pensar en un plugin completo que desea crear, deténgase ahora y vuelva cuando lo haga.

Si desea continuar, asegúrese de estar listo para dedicar un poco de tiempo.
El resto del libro será intenso, y voy a suponer que realmente quiere aprender, no solo leer los capítulos en su sofá.

### Potion

El plugin que crearemos agregará soporte para el lenguaje de programación Potion.

Potion es un lenguaje de juguete.
Es un lenguaje extremadamente pequeño que lo hace ideal para nuestros propósitos.

Potion se parece mucho a [Io](http://iolanguage.com/), con algunas ideas de Ruby, Lua y otros lenguajes mezclados.
Si nunca has probado [Io](http://iolanguage.com/), puede parecer extraño.
Recomiendo jugar con Potion durante al menos una o dos horas.
No lo usará en la vida real, pero podría cambiar su forma de pensar y exponerlo a nuevas ideas.

La implementación actual de Potion tiene muchos bordes ásperos.

Por ejemplo: si estropea la sintaxis, generalmente obtiene un fallo de segmentacion.
Intente no obsesionarse demasiado con esto.
Le proporcionaré un montón de código de muestra que funciona para que pueda centrarse principalmente en el Vimscript y no en Potion.

El objetivo no es aprender Potion (aunque eso también puede ser divertido).
El objetivo es usar Potion como un pequeño ejemplo para que podamos abordar muchos aspectos diferentes de la escritura de plugins completos de Vim.

### Ejercicios

Descargue e instale [ Potion ](http://perl11.github.com/potion/index.html). Está solo en esto, aunque debería ser bastante simple.

Asegúrese de que puede hacer los primeros dos ejemplos trabajando en el CLI de Potion y colocándolos en un archivo `.pn`.

Si parece que el intérprete no funciona, revise [este issue](https://github.com/perl11/potion/issues/12) por una posible causa.
