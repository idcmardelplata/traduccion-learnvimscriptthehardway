
# Variables

Hasta este punto hemos cubierto los comandos individuales. Para el próximo tercio del libro, veremos Vimscript como un **lenguaje de programación**. esto no será tan gratificante al instante como el resto de lo que aprendió, pero sentará las bases de la última parte del libro, donde crearemos un plugin vim completo desde cero.

Empecemos. Lo primero de lo que tenemos que hablar son de las variables. Ejecute los siguientes comandos:

```vimscript
:let foo = "bar"
:echo foo
```
vim mostrará `bar`. `foo` ahora es una variable, y le hemos asignado un string: "bar". Ahora ejecute estos comandos:

```vimscript
:let foo = 42
:echo foo
```

vim mostrará 42, porque hemos reasignado `foo` al entero `42`.

A partir de estos breves ejemplos, puede parecer que vimscript se escribe dinámicamente. Ese no es el caso, pero hablaremos sobre eso más adelante.

### opciones como variables

Puede leer y establecer opciones como variables usando una sintaxis especial. Ejecute los siguientes comandos:

```vimscript
:set textwidth=80
:echo &textwidth
```

vim mostrará `80`, el uso del símbolo ampersand `&` en el inicio de un nombre le dice a vim que se estás refiriendo a una opción, no a una variable que tenga el mismo nombre.

Veamos cómo funciona vim con opciones booleanas. Ejecute los siguientes comandos:

```vimscript
:set nowrap
:echo &wrap
```

```vimscript
:set wrap
:echo &wrap
```

Esta vez, vim muestra `1`, esta es una sugerencia muy clara de que vim trata al entero `0` como `falso` y al entero `1` como `verdadero.` sería razonable suponer que vim trata a cualquier entero que no sea cero como `verdadero,` y este es realmente el caso.

También podemos establecer opciones como variables usando el comando `let`. Ejecute los siguientes comandos:

```vimscript
:let &textwidth = 100
:set textwidth?
```

vim mostrará `textwidth = 100`.

¿por qué querríamos hacer esto cuando podríamos usar `set`? Ejecute los siguientes comandos:

```vimscript
:let &textwidth = &textwidth + 10
:set textwidth?
```

Esta vez vim muestra `textwidth = 110`, cuando configura una opción utilizando `set`, solo puede establecerla en un único valor literal. Por otro lado cuando usa `let` y lo configura como una variable, puede usar todo el poder de vimscript para determinar el valor.

### opciones locales

Si desea establecer el valor *local* de una opción como una variable, en lugar del valor *global*, debe prefijar el nombre de la variable.

Abra dos archivos en divisiones separadas y ejecute el siguiente comando:

```vimscript
:let &l:number = 1
```

Ahora cambie al otro archivo y ejecute este comando:

```vimscript
:let &l:number = 0
```

Observe que la primera ventana tiene números de línea y la segunda no.

### registros como variables

También puede leer y establecer registros como variables. Ejecute el siguiente comando:

```vimscript
:let @a = "hello!"
```

Ahora ponga el cursor en alguna parte de su texto y escriba `"ap`. Este comando le dice a vim que "pegue el contenido del registro `a` aquí". Simplemente configuramos el contenido de ese registro, por lo que vim pega `hello!` en su texto.

Los registros también se pueden leer. Ejecute el siguiente comando:

```vimscript
:echo @a
```

vim hará un echo `hola!`.

Seleccione una palabra en su archivo y cópiela con `y`, luego ejecute este comando:

```vimscript
:echo @"
```

vim hará un `echo` de la palabra que acaba de copiar, el registro `"` es el "registro sin nombre", que es donde irá el texto que copio sin especificar un destino.

Realice una búsqueda en su archivo con `/someword,` luego ejecute el siguiente comando:

```vimscript
:echo @/
```
vim hará `echo` del patrón de búsqueda que acaba de usar. Esto le permite leer y modificar mediante programación el patrón de búsqueda actual, que puede ser muy útil a veces.

### Ejercicios

Ir a través de su archivo `~/.vimrc` y cambiar algunos de los comandos `set` y `setlocal` a la forma `let`. Recuerde que las opciones booleanas todavía deben ser fijadas con `set algo`.

Intente establecer una opción booleana como `wrap` en algo distinto de cero o uno. ¿Qué sucede cuando lo configura a un número diferente? ¿Qué pasa si lo pone en un `string`?


Vuelva a través de su archivo `~/.vimrc` y deshaga los cambios.
Nunca debe usar `let` si `set` es suficiente, es más difícil de leer.
