
# Editando su vimrc

Antes de continuar con el aprendizaje de más Vimscript, encontremos una manera de facilitar la adición de nuevos mapeos a nuestro archivo `~/.vimrc.`

Hay veces que estará programando furiosamente un problema y se dará cuenta de que un nuevo mapeo facilitaría su edición.
Debe agregarlo a su archivo `~/.vimrc` en ese momento para asegurarse de que no lo olvide, pero no quiere perder su concentración.

La idea de este capítulo es que desea facilitar facilitar editar texto.

Eso no es un error tipográfico. Léalo de nuevo.

La idea de este capítulo es que desea (facilitar (facilitar (editar texto))).

### Editando mapeos

Agreguemos un mapeo que abra su archivo `~/.vimrc` en una división para que pueda editarlo rápidamente y volver a lo que estaba haciendo. Ejecute este comando:

```vimscript
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
```

Me gusta pensar en este comando como "editar mi archivo vimrc".

*$MYVIMRC* es una variable Vim especial que apunta a su archivo `~/.vimrc`. No se preocupe por eso ahora, solo confíe en mí que funciona.

`:vsplit` abre una nueva división vertical. Si prefiere una división horizontal, puede reemplazarla con `:split`.

Tómese un minuto y piense en ese comando. El objetivo es: "abrir mi archivo `~/.vimrc` en una nueva división". ¿Por qué funciona? ¿Por qué es necesaria cada pieza de ese mapeo?

Con ese mapeo puede abrir su archivo `~/.vimrc` con tres pulsaciones de tecla. Una vez que lo use varias veces, se quemará en su memoria muscular y tardará menos de medio segundo en escribirlo.

Cuando este en medio de la codificación y cree un nuevo mapeo que le ahorraría tiempo, es trivial agregarlo a tu archivo `~/.vimrc.`


### Recargando los mapeos

Una vez que haya agregado un mapeo a su archivo `~/.vimrc`, no tendrá efecto de inmediato. Su archivo `~/.vimrc` solo se lee cuando se inicia Vim. Esto significa que también debe ejecutar el comando manualmente para que funcione en la sesión actual, lo cual es una molestia.

Agreguemos un mapeo para hacer esto más fácil:

```vimscript
:nnoremap <leader>sv :source $MYVIMRC<cr>
```

Me gusta pensar en este comando como "source mi archivo vimrc".

El comando `source` le dice a Vim que tome el contenido del archivo dado y lo ejecute como Vimscript.

Ahora puede agregar fácilmente nuevos mapeos durante el proceso de codificación:

    Use `<leader>ev` para abrir el archivo.
    Agregue el mapeo.
    Use `:wq<cr>` (o `ZZ`) para escribir el archivo y cerrar la división, devolviéndolo a donde estaba.
    Use `<leader>sv` para obtener el archivo y hacer que nuestros cambios surtan efecto.

Son ocho pulsaciones más lo que sea necesario para definir el mapeo. Es muy poca sobrecarga, lo que reduce la posibilidad de romper el enfoque.

### Ejercicios

Agregue los mapeos para "editar mi `~/.vimrc`" y "source mi `~/.vimrc`" a su archivo `~/.vimrc`.

Pruebelos un par de veces, agregando mapeos ficticios cada vez.

Lea `:help myvimrc`.
