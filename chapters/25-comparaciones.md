
# Comparaciones

Hemos revisado los condicionales, pero las sentencias `if` no son muy útiles si no podemos comparar las cosas. Por supuesto, Vim nos permite comparar valores, pero no es tan sencillo como puede parecer.

Ejecute los siguientes comandos:

```vimscript
:if 10 > 1
:    echom "foo"
:endif
```

Vim, por supuesto, mostrará `foo`. Ahora ejecute estos comandos:

```vimscript
:if 10 > 2001
:    echom "bar"
:endif
```

Vim no muestra nada, porque 10 no es mayor que 2001. Hasta ahora todo funciona como se esperaba. Ejecute estos comandos:

```vimscript
:if 10 == 11
:    echom "first"
:elseif 10 == 10
:    echom "second"
:endif
```

Vim muestra `second`. Nada sorprendente aquí. Vamos a tratar de comparar `strings`. Ejecute estos comandos

```vimscript
:if "foo" == "bar"
:    echom "one"
:elseif "foo" == "foo"
:    echom "two"
:endif
```

Vim hace un `echo` de `two`. Todavía no hay nada sorprendente, entonces, ¿de qué estaba hablando al principio del capítulo?


### Sensibilidad Case

Ejecute los siguientes comandos:


```vimscript
:set noignorecase
:if "foo" == "FOO"
:    echom "vim is case insensitive"
:elseif "foo" == "foo"
:    echom "vim is case sensitive"
:endif
```

Vim evalúa el `elseif`, por lo que aparentemente Vimscript distingue entre mayúsculas y minúsculas. Es bueno saberlo, pero nada estremecedor. Ahora ejecute estos comandos:

```vimscript
:set noignorecase
:if "foo" == "FOO"
:    echom "no, it couldn't be"
:elseif "foo" == "foo"
:    echom "this must be the one"
:endif
```

Whoa Alto ahí. Sí, lo vio bien.
El comportamiento de `==` depende de la configuración de un usuario.
Le prometo que no voy a jugar con usted. Inténtalo de nuevo y verá. No estoy bromeando, no puedo hacer eso.

### Programación Defensiva

¿Qué significa esto? Esto significa que nunca puede confiar en la comparación `==` al escribir un plugin para que otras personas lo utilicen. Un simple `==` nunca debe aparecer en el código de sus plugins.

Esta idea es la misma que la de "`nmap` versus `nnoremap`". Nunca confíe en la configuración de sus usuarios.
Vim es viejo, vasto y complicado. Al escribir un plugin, debe asumir que los usuarios tendrán todas las variaciones de cada configuración.

Entonces, ¿cómo puede evitar esta ridiculez? Resulta que Vim tiene **dos conjuntos adicionales de operadores de comparación** para lidiar con esto.

Ejecute los siguientes comandos:

```vimscript
:set noignorecase
:if "foo" ==? "FOO"
:    echom "first"
:elseif "foo" ==? "foo"
:    echom "second"
:endif
```

Vim muestra `first` porque `==?` es el operador de comparación que "no distingue mayúsculas y minúsculas, no importa lo que el usuario haya configurado". Ahora ejecute los siguientes comandos:

```vimscript
:set ignorecase
:if "foo" ==# "FOO"
:    echom "one"
:elseif "foo" ==# "foo"
:    echom "two"
:endif
```

Vim muestra `two` porque `==#` es el operador de comparación que "distingue mayúsculas y minúsculas sin importar lo que el usuario haya configurado".

La moraleja de esta historia es que **siempre debe usar comparaciones explícitas**, sensibles a mayúsculas y minúsculas. **Usar las formas normales es incorrecto** y se romperá en algún momento. Ahorrase el problema y escriba el carácter adicional.

Cuando está comparando enteros, esta distinción obviamente no importa. Sin embargo, creo que es mejor usar las comparaciones que distinguen entre mayúsculas y minúsculas en todas partes (incluso cuando no son estrictamente necesarias), antes que olvidarlas en un lugar en el que sean necesarias.

Usar `==#` y `==?` con enteros funcionará bien, y si los cambia a strings en el futuro funcionará correctamente. Si prefiere usar `==` para enteros, está bien, solo recuerde que tendrá que cambiar la comparación si las cambia a strings en el futuro.

### Ejercicios

Juegue un poco con `:set ignorecase` y `:set noignorecase` y observe cómo actúan las distintas comparaciones.

Lea `:help ignorecase` para ver por qué alguien puede querer configurar esa opción.

Lea `:help expr4` para ver todos los operadores de comparación disponibles.
