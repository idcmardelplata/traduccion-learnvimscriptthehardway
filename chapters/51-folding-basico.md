
# Folding básico

Si nunca ha usado el plegado de código en Vim, no sabe lo que se está perdiendo.

Lea `:help usr_28` y pase un tiempo jugando con él en su trabajo normal.
Regrese a este capítulo una vez que lo tenga en sus dedos.

### Tipos de folding

Vim admite seis formas diferentes de definir cómo se debe plegar el texto.

#### Manual

Los pliegues se crean a mano y Vim los almacena en la RAM.
Cuando cierra Vim, desaparecen y debe volver a crearlos la próxima vez que edite el archivo.

Este método puede ser útil si lo combina con algunos de los mapeos personalizados para facilitar la creación de pliegues.
No haremos eso en este libro, pero téngalo en cuenta en caso de que encuentre un caso en el que pueda ser útil.

#### Marcas

Vim dobla su código en función de los caracteres en el texto actual.

Por lo general, estos caracteres se colocan en comentarios (como `//` y `{{{`), pero algunos lenguajes puede salirse con la suya utilizando algo en la sintaxis del lenguaje, como `{`y`}` en los archivos Javascript por ejemplo.

Puede parecer feo abarrotar su código con comentarios que son exclusivamente para su editor de texto, pero la ventaja es que le permite crear pliegues a mano para un archivo específico.
Esto puede ser realmente bueno si está trabajando con un archivo grande que desea organizar de una manera muy específica.

#### Dif

Un modo de plegado especial utilizado al diferenciar archivos.
No hablaremos de esto en absoluto porque Vim lo maneja automáticamente.

#### Expr

Esto le permite usar una pieza personalizada de Vimscript para definir dónde ocurren los pliegues.
Es el método más poderoso, pero también requiere más trabajo. Hablaremos de esto en el próximo capítulo.

### Potion folding

Echemos un vistazo a nuestro archivo de Potion de muestra una vez más:

```vimscript
factorial = (n):
    total = 1
    n to 1 (i):
        total *= i.
    total.

10 times (i):
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.
```

Los cuerpos de la función y el bucle están sangrados.
Esto significa que podemos obtener un pliegue básico con muy poco esfuerzo al usar el pliegue de indentacion.

Antes de comenzar, continúe y agregue un comentario sobre la linea `total *= i` así tenemos un buen bloque interno de varias líneas para probar.
Aprenderá por qué necesitamos hacer esto cuando haga los ejercicios, pero por ahora solo confíe en mí.
El archivo ahora debería verse así:

```vimscript
factorial = (n):
    total = 1
    n to 1 (i):
        # Multiply the running total.
        total *= i.
    total.

10 times (i):
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.
```

Cree una carpeta `ftplugin` en el repositorio de su plugin de `Potion` y cree una carpeta de `potion` dentro de el.
Finalmente, cree un archivo `plegable.vim` dentro de eso.

Recuerde que Vim ejecutará el código en este archivo siempre que establezca el `filetype` de un búfer en `potion` (porque está en una carpeta llamada potion).

Poner todo el código relacionado con el plegado en su propio archivo es generalmente una buena idea y nos ayudará a mantener organizadas las diversas funcionalidades de nuestro plugin.

Agregue la siguiente línea a este archivo:

```vimscript

setlocal foldmethod=indent
```

Cierre Vim y abra el archivo `factorial.pn` nuevamente. Juegue con el nuevo plegado con `zR`, `zM` y `za`.

¡Una línea de Vimscript nos dio algunos pliegues útiles! ¡Eso es muy bonito!

Puede notar que las líneas dentro del bucle interno de la función `factorial` no se pliegan aunque tengan indentacion. ¿Que esta pasando?

Resulta que, por defecto, Vim ignorará las líneas que comienzan con un carácter `#` cuando se usa el plegado de indentacion.
Esto funciona muy bien cuando edita archivos `C` (donde `#` señala una directiva de preprocesador)
pero no es muy útil cuando edita otros tipos de archivos.

Agreguemos una línea más al archivo `ftplugin/potion/folding.vim` para solucionar esto:

```vimscript
setlocal foldmethod=indent
setlocal foldignore=
```

Cierre y vuelva a abrir `factorial.pn` y ahora el bloque interno se plegará correctamente.

### Ejercicios

* Lea `:help foldmethod`.

* Lea `:help fold-manual`.

* Lea `:help fold-marker` y `:help foldmarker`.

* Lea `:help fold-indent`.

* Lea `:help fdl` y `:help foldlevelstart`.

* Lea `:help foldminlines`.

* Lea `:help foldignore`.
