
# Funciones

Como la mayoría de los lenguajes de programación, Vimscript tiene funciones. Echemos un vistazo a cómo crearlas, y luego hablaremos sobre algunas de sus peculiaridades.

Ejecute el siguiente comando:

```vimscript
:function meow()
```

Podría pensar que esto comenzaría a definir una función llamada `meow`. Desafortunadamente, este no es el caso, y ya nos hemos encontrado con uno de los caprichos de Vimscript.

*¡Las funciones de Vimscript deben comenzar con una letra mayúscula si no tienen ámbito!*

Incluso si agrega un scope a una función (de lo que hablaremos más adelante), de todos modos también debe poner en mayúscula la primera letra de los nombres de funciones. La mayoría de los programadores de Vimscript lo hacen, así que no rompa la convención.

Bien, vamos a definir una función de verdad esta vez. Ejecute los siguientes comandos:

```vimscript
:function Meow()
:  echom "Meow!"
:endfunction
```

Esta vez felizmente Vim definirá la función. Vamos a intentar ejecutarla:

```vimscript
call Meow()
```

Vim mostrará `Meow!` como se esperaba.

Intentemos retornar un valor. Ejecute los siguientes comandos:

```vimscript
:function GetMeow()
:  return "Meow String!"
:endfunction
```

Ahora pruébelo ejecutando este comando:

```vimscript
echom GetMeow()
```

Vim llamará a la función y le dará el resultado a `echom`, que mostrará `Meow String!`

### Llamando Funciones

Ya podemos ver que hay dos formas diferentes de llamar a funciones en Vimscript.

Cuando quiera llamar directamente a una función, use el comando `call`. Ejecute los siguientes comandos:

```vimscript
:call Meow()
:call GetMeow()
```

El primero mostrará `Meow!` Pero el segundo no muestra nada. El valor de retorno se elimina cuando usa `call`, por lo que esto solo es útil cuando la función tiene efectos secundarios.

La segunda forma de llamar a funciones es en expresiones. No es necesario utilizar `call` en este caso, solo debe nombrar la función. Ejecute el siguiente comando:

```vimscript
:echom GetMeow()
```

Como vimos antes, esto llama a `GetMeow` y pasa el valor de retorno a `echom`.

### Retorno Implicito

Ejecute el siguiente comando:

```vimscript
:echom Meow()
```

Esto mostrará dos líneas: `Meow!` y `0`. El primero obviamente viene del `echom` dentro de `Meow`. El segundo nos muestra que si una función de Vimscript no devuelve un valor, retorna implícitamente `0`. Vamos a usar esto para nuestra ventaja.

Ejecute los siguientes comandos:

```vimscript
:function TextwidthIsTooWide()
:  if &l:textwidth ># 80
:    return 1
:  endif
:endfunction
```

Esta función utiliza muchos conceptos importantes que hemos visto antes:

* Las declaraciones `if`
* Se tratan las opciones como variables
* Localiza estas variables de opción
* Comparaciones sensibles a mayúsculas y minúsculas


Si alguna de estas cosas no le resulta familiar, debería retroceder algunos capítulos y leer sobre ellas.

Ahora hemos definido una función que nos dirá si la configuración del `textwidth` es "demasiado ancha" en el búfer actual (porque 80 caracteres es, por supuesto, el ancho correcto para cualquier cosa que no sea HTML).

Vamos a intentar usarlo. Ejecute los siguientes comandos:

```vimscript
:set textwidth=80
:if TextwidthIsTooWide()
:  echom "WARNING: Wide text!"
:endif
```

¿Qué hicimos aquí?

* Primero configuramos el `textwidth` globalmente a `80`.
* Luego ejecutamos una instrucción `if` que verificó si `TextwidthIsTooWide()` era `true`.
* Esto terminó no siendo el caso, así que el cuerpo del `if` no fue ejecutado.

Como nunca devolvimos un valor explícitamente, Vim devolvió `0` desde la función, que es `false`. Intentemos cambiar eso. Ejecute los siguientes comandos:

```vimscript
:setlocal textwidth=100
:if TextwidthIsTooWide()
:  echom "WARNING: Wide text!"
:endif
```

Esta vez, la sentencia `if` en la función se ejecuta, devuelve `1`, por lo que `if` ejecuta su cuerpo.

### Ejercicios

Lea `:help :call`. Ignore cualquier cosa sobre "rangos" por ahora. ¿Cuántos argumentos puedes pasar a una función? ¿Es esto sorprendente?

Lea el primer párrafo de `:help E124` y descubra qué caracteres puede usar en los nombres de funciones. ¿Están bien los guiones bajos? Guiones? ¿Caracteres acentuados? Caracteres Unicode? Si no queda claro en la documentación, pruébelos y véalos.

Lea `:help return`, ¿Cuál es la "forma corta" de ese comando (que le dije que nunca usaras)? ¿Es lo que esperaba? ¿Si no, porque no?

[ Siguiente ](http://learnvimscriptthehardway.stevelosh.com/chapters/24.html)
