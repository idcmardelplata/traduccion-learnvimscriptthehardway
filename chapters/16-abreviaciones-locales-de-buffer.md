
# Bufers Locales - Abreviaciones

Ese último capítulo fue un monstruo, así que vamos a abordar algo más fácil. Hemos visto cómo definir mapeos y opciones de búfer locales, así que apliquemos la misma idea a las abreviaturas.

Abra sus archivos `foo` y `bar` de nuevo, cambie a `foo` y ejecute el siguiente comando:

```vimscript
:iabbrev <buffer> --- &mdash;
```

Mientras aún está en foo ingrese el modo de inserción y escriba el siguiente texto:

```vimscript
Hello --- world.
```

Vim reemplazará el `---` por usted. Ahora cambie a `bar` y pruébelo. No debería sorprender que no se reemplace, porque definimos que la abreviatura es local al búfer `foo`.

### Autocomandos y Abreviaciones

Vamos a emparejar estas abreviaturas de búfer locales con autocomandos para configurarlas y hacernos de un pequeño sistema de "snippet".

Ejecute los siguientes comandos:

```vimscript
:autocmd FileType python     :iabbrev <buffer> iff if:<left>
:autocmd FileType javascript :iabbrev <buffer> iff if ()<left>
```

Abra un archivo Javascript y pruebe la abreviatura `iff`. Luego abra un archivo de Python y pruébelo allí también. Vim realizará la abreviatura correspondiente según el tipo de archivo actual.

### Ejercicios

Cree algunas abreviaturas de "snippets" más para algunas de las cosas que suela usar mas a menudo en tipos específicos de archivos. Algunos buenos candidatos son `return` para la mayoría de los lenguajes, `function` para javascript y cosas como `&ldquo;` y `&rdquo;` para archivos HTML.

Agregue estos "snippets" a su archivo `~/.vimrc`.

Recuerde: la mejor manera de aprender a usar estos nuevos `snippets` es deshabilitar la forma antigua de hacer las cosas.
Ejecute `:iabbrev <buffer> return NOPENOPENOPE` le obligará a usar su abreviatura. Agregue estos `snippets` de "entrenamiento" para que coincidan con todos los que creó para ahorrar tiempo.
