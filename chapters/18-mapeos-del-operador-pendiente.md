
# Mapeo del Operador Pendiente

En este capítulo vamos a explorar un agujero de conejo más en el sistema de mapeos de Vim: "mapeos del operador pendiente". Retrocedamos por un segundo y asegurémonos de que tenemos un vocabulario claro.

Un operador es **un comando que espera que usted ingrese un comando de movimiento, y luego hace algo en el texto entre el lugar donde se encuentra actualmente y el movimiento al que le llevaría**.

Algunos ejemplos de operadores son `d`, `y`, y `c`. Por ejemplo:

| Tecla | Operador     | Movimiento               |
|-------|--------------|--------------------------|
| `dw`    | `delete`   | a la siguiente palabra   |
| `ci(`   | `change`   | dentro de los paréntesis |
| `yt,`   | `yank`     | hasta la coma            |


### Mapear movimientos

Vim le permite crear nuevos movimientos que funcionan con todos los comandos existentes. Ejecute el siguiente comando:

```vimscript
:onoremap p i(
```

Ahora escriba el siguiente texto en un búfer:

```vimscript
return person.get_pets(type="cat", fluffy_only=True)
```

Ponga su cursor en la palabra "cat" y escriba `dp`. ¿Que pasó? Vim borró todo el texto dentro de los paréntesis. Puede pensar en este nuevo movimiento como "parámetros".

El comando `onoremap` le dice a Vim que cuando está esperando un movimiento para entregarle a un operador y ve `p,` debe tratarlo como `i (`.

Cuando ejecutamos `dp` era como decir "eliminar parámetros", lo que Vim traduce como "eliminar dentro paréntesis ".

Podemos usar este nuevo mapeo inmediatamente con todos los operadores. Escriba el mismo texto que antes en el búfer (o simplemente deshaga el cambio):

```vimscript
return person.get_pets(type="cat", fluffy_only=True)
```

Ponga su cursor en la palabra "cat" y escriba `cp`. ¿Que pasó? Vim eliminó todo el texto dentro de los paréntesis, pero esta vez lo dejó en el modo de inserción porque utilizó "change" en lugar de "delete".

Probemos con otro ejemplo. Ejecute el siguiente comando:

```vimscript
:onoremap b /return<cr>
```

Ahora escriba el siguiente texto en un búfer:

```python
def count(i):
    i += 1
    print i

    return foo
```

Coloque el cursor en la `i` en la segunda línea y presione `db`. ¿Que pasó? Vim eliminó todo el cuerpo de la función, hasta el `return`, puesto que nuestro mapeo usó la búsqueda normal de Vim para encontrar.

Cuando intente pensar en cómo definir un nuevo movimiento de *operador pendiente*, puede pensarlo de la siguiente manera:

    1. Comience en la posición del cursor.
    2. Entre en modo visual (charwise).
    3. ... las teclas de mapeo van aquí ...
    4. Ahora debe seleccionarse todo el texto que desea incluir en el movimiento.

Es su trabajo completar el paso tres con las teclas apropiadas.

### Cambiando el la posición inicial del cursor

Es posible que ya haya visto un problema en lo que hemos aprendido hasta ahora. Si nuestros movimientos siempre tienen que comenzar en la posición actual del cursor, esto limita lo que podemos hacer.

Vim no tiene el hábito de limitar lo que puede hacer, así que, por supuesto, hay una manera de solucionar este problema. Ejecute el siguiente comando:

```vimscript
:onoremap in( :<c-u>normal! f(vi(<cr>
```

Esto puede parecer aterrador, pero intentémoslo. Ingrese el siguiente texto en el búfer:

```vimscript
print foo(bar)
```

Coloque el cursor en algún lugar de la palabra `print` y escriba `cin (`. Vim eliminará el contenido de los paréntesis y lo colocará en modo de inserción entre ellos.

Puede pensar que esta asignación significa "entre los siguientes paréntesis", y ejecutara el operador en el texto dentro del siguiente conjunto de paréntesis en la línea actual.

Vamos a hacerle un compañero llamado "dentro de los últimos paréntesis" ("previo" sería un palabra mejor, pero ocultaría el movimiento del "párrafo"). Ejecute el siguiente comando:

```vimscript
:onoremap il( :<c-u>normal! F)vi(<cr>
```

Pruébelo en un texto propio para asegurarse de que funciona.

Entonces, ¿cómo funcionan estos mapeos? Primero, el `<c-u>` es algo especial que puede ignorar por ahora, solo confíe en mí que debe estar allí para que los mapeos funcionen en todos los casos. Si eliminamos eso nos quedamos con:

```vimscript
:normal! F)vi(<cr>
```

`:normal` es algo de lo que hablaremos en un capítulo posterior, pero por ahora es suficiente saber que es un comando que se usa para simular que se presionan las teclas en el modo normal. Por ejemplo, ejecutando `:normal! dddd` eliminará dos líneas, al igual que al presionar `dddd`. El `<cr>` al final de la asignación es lo que ejecuta el comando `:normal!`.

Así que ahora sabemos que el mapeo es básicamente ejecutando el último bloque de teclas:

```vimscript
F)vi(
```

Esto es bastante simple:

    `F)`: Mueve hacia atrás al carácter `)` más cercano.
    `vi (`: Selecciona visualmente dentro de los paréntesis.

Terminamos con el texto que queremos operar seleccionado visualmente, y Vim realiza la operación de manera normal.

### Reglas generales

Una buena manera de mantener a raya las múltiples formas de crear mapeos de operador pendiente es recordar las siguientes dos reglas:

**Si su mapeo de operador pendiente termina con algún texto seleccionado visualmente, Vim operará en ese texto.
De lo contrario, Vim operará en el texto entre la posición del cursor original y la nueva posición.**

### Ejercicios

Cree mapeos de operador pendiente para "alrededor de paréntesis siguientes" y "alrededor de paréntesis anteriores".

Cree mapeos similares para `in/around` de la próxima/ultima para llaves.

Lea `:help omap-info` y vea si puede descifrar para qué sirve `<c-u>` en los ejemplos.
