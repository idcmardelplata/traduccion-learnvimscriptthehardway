
# Teoría del movimiento de secciones

Si nunca ha usado los comandos de movimiento de sección de Vim `([[,]], [] y] [)` tome un segundo y lea la ayuda para ellos ahora.
Continúe y lea `:help section` también.

Confundido todavía? Está bien, así me paso la primera vez que leí esas cosas.
Vamos a tomar un desvío rápido de escribir código para aprender cómo funcionan estos movimientos, y luego en el próximo capítulo haremos que nuestro plugin de Potion los soporte.

### Archivos Nroff

Los cuatro comandos de "movimiento de sección" están diseñados conceptualmente para moverse entre "secciones" de un archivo.

Todos estos comandos están diseñados para funcionar con archivos `nroff` de forma predeterminada. `Nroff` es un lenguaje como `LaTeX` o `Markdown`, y se usa para escribir texto que se formateará más tarde (en realidad es el formato utilizado por las páginas de manual de UNIX).

Los archivos `Nroff` usan un cierto conjunto de "macros" para definir "encabezados de sección". Por ejemplo, aquí hay un extracto de la página de manual de awk:

```awk
.SH NAME                                                     ***
awk \- pattern-directed scanning and processing language
.SH SYNOPSIS                                                 ***
.B awk
[
.BI \-F
.I fs
]
[
.BI \-v
.I var=value
]
[
.I 'prog'
|
.BI \-f
.I progfile
]
[
.I file ...
]
.SH DESCRIPTION                                              ***
.I Awk
scans each input
.I file
for lines that match ...
```

Las líneas que comienzan con `.SH` son encabezados de sección.
Los he marcado con `***`. Los cuatro comandos de movimiento de sección moverán el cursor entre estas líneas de encabezado de sección.

Vim considera cualquier línea que comience con un `.` y una de las macros de encabezado `nroff` para ser un encabezado de sección,
¡incluso cuando no esté editando un archivo `nroff`!

Puede cambiar las macros cambiando la configuración de las secciones, pero Vim aún requiere un punto al comienzo de la línea,
y las macros deben ser pares de caracteres, por lo que esa configuración no agrega suficiente flexibilidad para los archivos de Potion.

### Braces

Los comandos de movimiento de sección también buscan una cosa más: una llave de apertura o cierre `({o})` como primer carácter en una línea.

`[[y]]` busca llaves de apertura, mientras que `[]` y `][` busca llaves de cierre.

Este "truco" adicional le permite moverse fácilmente entre secciones de lenguajes tipo C.
Sin embargo, estas reglas son siempre las mismas sin importar en qué tipo de archivo se encuentre.

Ponga lo siguiente en un búfer:

```basic
Test           A B
Test

.SH Hello      A B

Test

{              A
Test
}                B

Test

.H World       A B

Test
Test           A B
```

Ahora ejecute `:set filetype=basic` para decirle a Vim que este es un archivo BASIC, y pruebe los comentarios de movimiento de sección.

Los comandos `[[` y `]]` se moverán entre las líneas marcadas con `A`, mientras que `[]` y `][` se moverán entre las líneas marcadas con `B`.

¡Esto nos muestra que Vim siempre usa estas mismas dos reglas para el movimiento de sección, incluso para lenguajes donde ninguno tiene sentido (como BASIC)!


### Ejercicios

* Lea `:help section` nuevamente, ahora que conoce la historia del movimiento de secciones.

* Lea `:help sections` solo por el gusto de hacerlo.
