
# Números


Ahora es el momento de comenzar a observar de cerca los diferentes tipos de
variables que puede usar. Primero repasaremos los tipos numéricos de Vim.

Vimscript tiene dos tipos de variables numéricas: Números y Flotantes. Un
número es un entero con signo de 32 bits. Un flotante es un número de punto
flotante.

Puede especificar los números de diferentes maneras. Ejecute el siguiente
comando:
```vimscript
:echom 100
```

No hay sorpresas aquí: Vim muestra 100. Ahora ejecute este comando:
```vimscript
echom 0xff
```

Esta vez, Vim muestra 255. Puede especificar números en notación hexadecimal pre fijándolos con `0x` o `0X`. Ahora ejecute este comando:

```vimscript
echom 010
```
También puede usar octal al comenzar un número con un `0`. Tenga cuidado con
esto, porque es fácil cometer errores. Pruebe los siguientes comandos:

```vimscript
:echom 017
:echom 019
```


Vim imprimirá 15 para el primer comando, porque 17 en octal es igual a 15
en decimal. Para el segundo comando, Vim lo trata como un número decimal,
aunque comienza con un 0, porque no es un número octal válido.


Debido a que Vim hace algo incorrecto en este caso, recomiendo evitar el uso de números octales cuando sea posible.

## Formatos Float

Los float también se pueden especificar de múltiples maneras. Ejecute el siguiente comando:


```vimscript
:echo 100.1
```

Fíjese en que estamos usando `echo` aquí y no como lo solemos hacer. Hablaremos de por qué en un momento.

Vim muestra 100.1 como se esperaba. También puede usar la notación exponencial. Ejecute este comando:


```vimscript
:echo 5.45e+3
```

Vim muestra 5450.0. También se puede utilizar un exponente negativo. Ejecute este comando:


```vimscript
:echo 15.45e-2
```

Vim muestra 0.1545. El + o - antes de la potencia de diez es opcional. Si se omite, entonces se asume que es positivo. Ejecute el siguiente comando:


```vimscript
:echo 15.3e9
```

Vim mostrará 1.53e10, que es equivalente. El punto decimal y el número que sigue no son opcionales. Ejecute el siguiente comando y vea que se bloquee:


```vimscript
:echo 5e10
```

## Coerción


Cuando combina un Número y un Flotante mediante aritmética comparación o cualquier otra operación, Vim convertirá el Número en un Flotante, lo que dará como resultado un Flotante.
Ejecute el siguiente comando:


```vimscript
:echo 2 * 2.0
```

Vim muestra `4.0`

## División

Al dividir dos números, el resto se elimina. Ejecute el siguiente comando:


```vimscript
:echo 3 / 2
```

Vim muestra 1. Si desea que Vim realice una división de punto flotante, uno de los números debe ser un flotante, lo que hará que el otro numero también sea convertido en un flotante. Ejecute este comando:

```vimscript
:echo 3 / 2.0
```

Vim muestra `1.5`. El `3` es transformado a un flotante, y luego se realiza la división de punto flotante normalmente.

### Ejercicios

Lea `:help Float`. ¿Cuándo podría no funcionar un número de punto flotante en Vimscript?

Lea `:help floating-point-precision`. ¿Qué podría significar esto si está escribiendo un plugin de Vim que trata con números de punto flotante?
