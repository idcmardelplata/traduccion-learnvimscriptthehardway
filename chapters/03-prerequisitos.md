# Prerequisitos

Para usar este libro, debe tener instalada la última versión de Vim, que es la versión 7.3 en el momento de escribir este documento. Las nuevas versiones de Vim son casi siempre compatibles con versiones anteriores, por lo que todo en este libro debería funcionar bien con cualquier versión después de la versión 7.3 también.

Nada en este libro es específico para Vim de consola o VIM de GUI como gVim o MacVim. Puede usar lo que prefiera.

Debería estar cómodo editando archivos en Vim. Debe conocer la terminología básica de Vim como "búfer", "ventana", "modo normal", "modo de inserción" y "objetos de texto".

Si aún no estás en ese punto, debe seguir el programa `vimtutor`, usar Vim exclusivamente durante un mes o dos, y regresar cuando tenga a Vim quemado en sus dedos.

También necesitarás tener algo de experiencia en programación. Si nunca has programado, eche un vistazo a [ Learn Python the Hard Way ](http://learnpythonthehardway.org/) primero y vuelve a este libro cuando haya terminado.

# Crear un archivo Vimrc:

Si ya sabe qué es un archivo` ~/.vimrc` y tiene uno, continúe con el siguiente capítulo.

Un archivo `~/.vimrc` es un archivo que usted debe crear y que contiene algún código Vimscript. Vim ejecutará automáticamente el código dentro de este archivo cada vez que se inicie.

En Linux y Mac OS X, este archivo se encuentra en su directorio de inicio y se llama `.vimrc`.

En Windows, este archivo se encuentra en su carpeta de inicio y se llama `_vimrc`.

Para encontrar fácilmente la ubicación y el nombre del archivo en cualquier sistema operativo, ejecute `:echo $MYVIMRC` en Vim. La ruta se mostrará en la parte inferior de la pantalla.

Cree este archivo si aun no existe.
