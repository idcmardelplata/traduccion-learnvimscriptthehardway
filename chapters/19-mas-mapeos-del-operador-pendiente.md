
# Mas sobre mapeos del operador pendiente

La idea de operadores y movimientos es uno de los conceptos más importantes en Vim, y es una de las razones más importantes por las que Vim es tan eficiente. Vamos a practicar la definición de nuevos movimientos un poco más, porque extender esta poderosa idea hace que Vim sea aún más poderoso.

Digamos que esta escribiendo un texto en Markdown. Si no ha usado Markdown antes, no se preocupe, para nuestros propósitos aquí es muy simple. Escriba lo siguiente en un archivo:

```markdown
Topic One
=========

This is some text about topic one.

It has multiple paragraphs.

Topic Two
=========

This is some text about topic two.  It has only one paragraph.
```

Las líneas "subrayadas" con el caracter `=` son tratadas como encabezados por markdown. vamos a crear algunos mapeos que nos permitan movernos por los encabezados. Ejecute el siguiente comando:

```vimscript
:onoremap ih :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rkvg_"<cr>
```

Este mapeo es bastante complicado, así que ponga su cursor en uno de los párrafos (no los encabezados) y escriba `cih`. Vim eliminará el encabezado de cualquier sección en la que se encuentre y lo pondrá en modo de inserción ("cambiar dentro del encabezado").

Este mapeo utiliza algunas cosas que nunca hemos visto antes, así que echemos un vistazo a cada pieza individualmente. La primera parte del mapeo, `:onoremap ih` es solo el comando de mapeo que hemos visto antes, así que lo omitiremos. Seguiremos ignorando el <c-u> por el momento también.

Ahora estamos viendo el resto de la línea:

```vimscript
:execute "normal! ?^==\\+$\r:nohlsearch\rkvg_"<cr>
```

### Normal

El comando `:normal` toma un conjunto de caracteres y realiza las mismas acciónes que haría si se escribiera en modo normal. Vamos a entrar en mayor detalle en un capítulo posterior, pero ya lo hemos visto varias veces, así que es hora de al menos probar. Ejecute este comando:

```vimscript
:normal gg
```

Vim se moverá a la parte superior del archivo. Ahora ejecute este comando:

```vimscript
:normal >>
```

Vim indentara la línea actual.

Por ahora, no se preocupe por el `!` después del `normal` en nuestro mapeo. Hablaremos de eso más tarde.

### Execute

El comando `execute` toma un string Vimscript (que veremos detalladamente más adelante) y la ejecuta como un comando.

Ejecute esto:

```vimscript
:execute "write"
```

Vim guardara su archivo, como si hubiera escrito `:write <cr>`. Ahora ejecute este comando:

```vimscript
:execute "normal! gg"
```

Vim ejecutara `:normal! gg`, que, como acabamos de ver, lo moverá a la parte superior del archivo. Pero, ¿por qué molestarse con esto cuando podríamos simplemente ejecutar el comando `normal!` en sí?

Observe el siguiente comando e intente adivinar qué hará:

```vimscript
:normal! gg/a<cr>
```

Parece que debería:

    1. Mover a la parte superior del archivo.
    2. Iniciar una búsqueda.
    3. Rellenar "a" como el objetivo a buscar.
    4. Presiona return para realizar la búsqueda.

Si lo ejecuta, Vim se moverá a la parte superior del archivo y nada más!

El problema es que `normal!` no reconoce "caracteres especiales" como `<cr>`. Hay varias maneras de evitar esto, pero la más fácil de leer y utilizar es `execute`.

Cuando `execute` busca en el string que le pasamos para ejecutar, sustituirá cualquier carácter especial que encuentre antes de ejecutarlo. En este caso, `\r` es una secuencia de escape que significa "retorno de carro". La doble barra invertida también es una secuencia de escape que coloca una barra invertida literal en el string.

Si realizamos este reemplazo en nuestro mapeo y observamos el resultado, podemos ver que la asignación se realizará:

```vimscript
:normal! ?^==\+$<cr>:nohlsearch<cr>kvg_
                ^^^^           ^^^^
                 ||             ||
These are ACTUAL carriage returns, NOT the four characters
"left angle bracket", "c", "r", and "right angle bracket".
```

Así que ahora  `normal!` ejecutará estos caracteres como si los hubiéramos escrito en modo normal. Vamos a dividirlos en partes para averiguar qué están haciendo:

```vimscript
?^==\+$
:nohlsearch
kvg_
```

La primera pieza, `?^==\+$` realiza una búsqueda hacia atrás para cualquier línea que conste de dos o más signos iguales y nada más. Esto dejará nuestro cursor en el primer carácter de la línea de signos iguales.

Estamos buscando hacia atrás porque cuando dice "cambiar dentro del encabezado" mientras su cursor está en una sección del texto, probablemente quiera cambiar el encabezado de esa sección, no la siguiente.

La segunda pieza es el comando `:nohlsearch`. Esto simplemente borra el resaltado de la búsqueda que acabamos de realizar para que no distraiga.

La pieza final es una secuencia de tres comandos de modo normal:

. `k`: subir una linea, como estábamos en el primer carácter de la línea de signos iguales, ahora estamos en el primer carácter del texto del encabezado.
. `v`: entrar en modo visual (carácter).
. `g_`: mueve al último carácter que no está en blanco de la línea actual. Usamos esto en lugar de `$` porque `$` también destacaría el carácter de nueva línea, y esto no es lo que queremos.

### Resultados

Eso fue mucho trabajo, pero ahora que hemos examinado cada parte del mapeo. Recuerde:

* Creamos un mapeo de operador pendiente para "dentro del encabezado de esta sección".
* Usamos `execute` y `normal!` para ejecutar los comandos en el modo normal, puesto que necesitábamos seleccionar el encabezado y permitirnos usar caracteres especiales en ellos.
* Nuestro mapeo busca la línea de signos iguales que denota un encabezado y selecciona visualmente el texto del encabezado que se encuentra arriba.
* Vim se encarga del resto.

Veamos un mapeo más antes de continuar. Ejecute el siguiente comando:

```vimscript
:onoremap ah :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rg_vk0"<cr>
```

Inténtelo poniendo el cursor en el texto de una sección y escribiendo `cah`. Esta vez, Vim eliminará no solo el texto del encabezado sino también la línea de signos iguales que denota un encabezado. Puede pensar en este movimiento como "alrededor del encabezado de esta sección".

¿Qué tiene de diferente este mapeo? Mirémoslos de lado a lado:

```vimscript
:onoremap ih :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rkvg_"<cr>
:onoremap ah :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rg_vk0"<cr>
```

La única diferencia con el mapeo anterior es el final, donde seleccionamos el texto para operar:

```vimscript
inside heading: kvg_
around heading: g_vk0
```

El resto del mapeo es el mismo, por lo que aún comenzamos con el primer carácter de la línea de signos iguales. Desde allí:

    `g_`:mueve al último carácter que no está en blanco en la línea.
    `v` :entrar en modo visual (carácter).
    `k` :subir una linea, esto nos pone en la línea que contiene el texto del encabezado.
    `0` :moverse al primer carácter de la línea.

El resultado es que tanto el texto como los signos iguales terminan seleccionados visualmente, y Vim realiza la operación en ambos.

### Ejercicios

Markdown también puede tener encabezados delimitados con líneas `-` de caracteres. Ajuste la expresión regular en estos mapeos para que funcione con cualquier tipo de encabezado. Es posible que desee revisar `:help pattern-overview`. Recuerde que la expresión regular está dentro de un string, por lo que las barras invertidas deberán escaparse.

Agregue dos autocomandos a su archivo `~/.vimrc` para crear estos mapeos. Asegúrese de mapearlos solo en los búferes apropiados, y asegúrese de agruparlos para que no se dupliquen cada vez que recargue el archivo.

Lea `:help` normal.
Lea `:help execute`.
Lea `:help expr-quote`. para ver las secuencias de escape que puede usar en strings.

Cree un mapeo de operador pendiente "dentro de la próxima dirección de correo electrónico" para que pueda decir "cambiar dentro de la siguiente dirección de correo electrónico". `in@` es un buen candidato para mapear las teclas. Probablemente querrá usar `/`... alguna regex ... `<cr>` para esto.
