
# Ejecute

El comando `execute` se usa para evaluar un `string` como si fuera un comando de Vimscript. Lo vimos en un capítulo anterior, pero ahora que sabemos un poco más sobre los strings en Vimscript, vamos a echar otro vistazo.

## Funcionamiento básico

Ejecute el siguiente comando:


```vimscript
:execute "echom 'Hello, world!'"
```

Vim evalúa `echom 'Hello, world!'` como un comando y obedientemente lo imprime en la pantalla y en el registro de mensajes. `execute` es una herramienta muy poderosa porque le permite construir comandos a partir de cadenas arbitrarias.

Probemos un ejemplo más útil. Abra cualquier archivo en Vim, luego use `:edit foo.txt` en la misma ventana para abrir un nuevo búfer. Ahora ejecute el siguiente comando:


```vimscript
:execute "rightbelow vsplit " . bufname("#")
```

Vim abrirá el primer archivo en una división vertical a la derecha del segundo archivo. ¿que fue Lo que pasó aquí?

Primero, Vim construye la cadena de comandos al concatenar `"rightbelow vsplit"` con el resultado de la llamada a `bufname("#")`.

Veremos la función más adelante, pero por ahora solo confíe en que devuelve la ruta del búfer anterior. Puede jugar con él usando `echom` si quiere verlo por usted mismo.

Una vez que se evalúa `bufname`, Vim crea el string `"rightbelow vsplit bar.txt"`. El comando `execute` evalúa esto como un comando de Vimscript que abre la división con el archivo.

## Es peligroso execute

En la mayoría de los lenguajes de programación, el uso de la construcción "eval" para evaluar strings como código de programa está mal visto (para ponerlo a la ligera). El comando `execute` de Vimscript no tiene el mismo estigma por dos razones.

* Primero, la mayoría del código de Vimscript solo recibe información de una sola persona: el usuario. Si el usuario desea ingresar un string delicado que hará que un comando `execute` haga algo malo, ¡es su computadora! Compare esto con otros lenguajes, donde los programas constantemente reciben información de usuarios no confiables. Vim es un entorno único donde las preocupaciones de seguridad normales simplemente no son comunes.

* La segunda razón es que, dado que Vimscript a veces tiene una sintaxis arcana y delicada, `execute` es a menudo la forma más fácil y sencilla de hacer algo. En la mayoría de los otros lenguajes, usar una construcción "eval" no le ahorrará mucha escritura de código, pero en Vimscript puede colapsar muchas líneas en una sola.

## Ejercicios

Lea `:help execute` para tener una idea de algunas de las cosas que puede y no puede hacer con `execute`. No bucee demasiado profundo todavía, lo revisaremos muy pronto.

Lea `:help leftabove`, `:help rightbelow`, `:help :split`, y `:help :vsplit` (note los dos puntos extra en los dos últimos temas).

Agregue un mapeo a su archivo `~/.vimrc` para que abra el búfer anterior en una división de su elección (vertical / horizontal, arriba / abajo / izquierda / derecha).
