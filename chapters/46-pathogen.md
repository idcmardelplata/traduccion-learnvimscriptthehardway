
# Una nueva esperanza: diseño de plugins con Pathogen.

El diseño básico de Vim para archivos de plugins tiene sentido si solo está agregando un archivo aquí y allá para personalizar su propia experiencia de Vim,
pero se convierte en un desastre cuando desea utilizar plugins que otras personas han escrito.

En el pasado, cuando quería usar un plugin que alguien más escribió, descargaba los archivos y los colocaba, uno por uno, en los directorios apropiados.
También podía usar `zip` o `tar` para ubicarlos por usted.

Hay algunos problemas importantes con este enfoque:
* ¿Qué sucede cuando quiere actualizar un plugin?
  Puede sobrescribir los archivos antiguos, pero ¿cómo sabe si el autor eliminó un archivo que ahora necesita eliminar a mano?

* ¿Qué sucede si dos plugins tienen un archivo con el mismo nombre (como `utils.vim` o algo genérico como `esp`)?
*  A veces, simplemente puede cambiarle el nombre, pero si está en `autoload/` u otro directorio donde los nombres importan, debe editar el plugin usted mismo,
  y eso no es divertido.

A la gente se le ocurrieron varios trucos para intentar facilitar las cosas, como `Vimballs`.
Afortunadamente, no necesitamos sufrir más por estos hacks feos. *Tim Pope* creó un maravilloso plugin llamado `Pathogen`
que hace que la administración de múltiples plugins sea muy fácil, siempre y cuando los autores de plugins estructuren sus plugins de una manera sensata.

Echemos un vistazo rápido a cómo funciona `Pathogen` y lo que debemos hacer para que nuestro plugin sea compatible.

### Runtimepath

Cuando Vim busca archivos en un directorio específico, como en `syntax/,` no mira solamente en un solo lugar.
Al igual que la variable de entorno `PATH` en sistemas `Linux/Unix/BSD`,
Vim tiene una configuración llamada `runtimepath` que le dice dónde encontrar los archivos para cargar.

Cree un directorio llamado `colors` en su escritorio.
Cree un archivo en ese directorio llamado `mycolor.vim` (puede dejarlo vacío para esta demostración).
Abra Vim y ejecute este comando:

```vimscript
:color mycolor
```
Vim mostrará un error, porque no sabe buscar archivos en su escritorio.
Ahora ejecute este comando:

```vimscript
:set runtimepath=/Users/sjl/Desktop
```

Obviamente necesitará cambiar la ruta para que coincida con la ruta de su propio escritorio.
Ahora intente el comando `color` nuevamente:

```vimscript
:color mycolor
```

Esta vez Vim no arroja un error, porque pudo encontrar el archivo `mycolor.vim`.
Debido a que el archivo estaba en blanco, en realidad no hizo nada, pero sabemos que se encontró porque no arrojó un error.

### Pathogen

El plugin `Pathogen` agrega automáticamente rutas a su `runtimepath` cuando carga Vim.
Todos los directorios dentro de `~/.vim/bundle/` se agregarán al `runtimepath`.

Esto significa que cada directorio dentro de `bundle/` debe contener algunos o todos los directorios estándar de plugins de Vim,
como `colors/` y `syntax/`. Vim ahora puede cargar archivos de cada uno de esos directorios, lo que hace que sea sencillo mantener
los archivos de cada `plugin` dentro de su propio directorio.

Esto hace que sea trivial actualizar los `plugins`.
Simplemente puede eliminar por completo el directorio del plugin anterior y reemplazarlo con la nueva versión.
Si mantiene su directorio `~/.vim` bajo control de versiones (y debería hacerlo) puede usar los subrepos de Mercurial o los submódulos de Git
para chequear directamente el repositorio de cada plugin en `~/.vim/bundle/` y luego actualizarlo con un simple `hg pull`; `hg update` o un `git pull origin master`

### Siendo compatibles con Pathogen

Cuando escribamos nuestro plugin de `Potion`, vamos a querer que nuestros usuarios lo instalen y usen vía **Pathogen**,
Hacer esto es simple: ¡simplemente colocamos nuestros archivos en los directorios apropiados dentro del repositorio del plugin!

El repositorio de nuestro plugin tendrá esta estructura:

```bash
potion/
    README
    LICENSE
    doc/
        potion.txt
    ftdetect/
        potion.vim
    ftplugin/
        potion.vim
    syntax/
        potion.vim
    ... etc ...
```

Podemos poner esto en GitHub o Bitbucket y los usuarios simplemente pueden clonarlo dentro de `bundle/` y todo funcionara!.

### Ejercicios

Instale Pathogen si aún no lo ha hecho.

* Cree un repositorio Mercurial o Git para su plugin, llamado `Potion`.
Puede colocarlo en cualquier lugar que desee y vincularlo en `~/.vim/bundle/potion/` o simplemente ponerlo directamente en `~/.vim/bundle/potion/`.

* Cree los archivos `README` y `LICENSE` en el repositorio y hágales un commit.

* Suba los cambios al repositorio (sea Bitbucket, Gitlab o Github)

* Lea `:help runtimepath`
