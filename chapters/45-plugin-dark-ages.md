
# Diseño de plugins en la Edad Media

Lo primero de lo que tenemos que hablar es cómo estructurar nuestro plugin.
En el pasado esto fue un asunto desordenado, pero ahora hay una herramienta que hace que el proceso de instalación de plugins de Vim sea mucho,
mucho más sensato.

Primero tenemos que revisar el diseño básico,
luego hablaremos sobre cómo restaurar nuestra cordura.

# Estructura básica

Vim Vanilla admite la división de plugins en varios archivos.
Hay varios directorios diferentes que puede crear en `~/.vim/` que significan varias cosas.

Cubriremos los directorios más importantes ahora, pero no se estese demasiado por ellos.
Los revisaremos uno a la vez mientras creamos nuestro plugin de Potion.

Antes de continuar, necesitamos hablar sobre algo de vocabulario.

He estado usando la palabra "plugin" para significar "un gran pedazo de Vimscript que hace un montón de cosas relacionadas".
Vim tiene un significado más específico de "plugin", que es "un archivo en el directorio `~/.vim/plugin/`".

La mayoría de las veces usaré la primera definición. Trataré de ser claro cuando me refiera al segundo.



### ~/.vim/colors/

Los archivos dentro de `~/.vim/colors/` se tratan como esquemas de color. Por ejemplo: si ejecuta `:color mycolors` Vim buscará un archivo en `~/.vim/colors/mycolors.vim` y lo ejecutará. Ese archivo debe contener todos los comandos de Vimscript necesarios para generar su combinación de colores.

No vamos a cubrir esquemas de color en este libro.
Si desea crear el suyo propio, debe copiar un esquema existente y modificarlo. Recuerde esto: `:help` es su amigo.



### ~/.vim/plugin/

Los archivos dentro de `~/.vim/plugin/` se ejecutarán cada vez que se inicie Vim.
Estos archivos están destinados a contener código que siempre desea cargar cada vez que inicia Vim.



### ~/.vim/ftdetect/

Todos los archivos en `~/.vim/ftdetect/` también se ejecutarán cada vez que inicie Vim.

`ftdetect` significa "detección de tipo de archivo".
Los archivos en este directorio deben configurar comandos automáticos que detecten y configuren el tipo de archivo, y nada más.
Esto significa que nunca deben tener más de una o dos líneas de largo.



### ~/.vim/ftplugin/

Los archivos en `~/.vim/ftplugin/` son diferentes.

¡El nombre de estos archivos es importante!
Cuando Vim establece el tipo de archivo de un búfer en un valor, busca un archivo en `~/.vim/ftplugin/` que coincida.
Por ejemplo: si ejecuta `set filetype=derp`, Vim buscará `~/.vim/ftplugin/derp.vim`. Si ese archivo existe, lo ejecutará.

Vim también admite directorios dentro de `~/.vim/ftplugin/`.
Para continuar con nuestro ejemplo: `set filetype=derp` también hará que Vim ejecute todos los archivos `*.vim` dentro de `~/.vim/ftplugin/derp/`.
Esto le permite dividir los archivos `ftplugin` de su plugin en grupos lógicos.

Debido a que estos archivos se ejecutan cada vez que se establece el tipo de archivo de un búfer,
¡solo deben establecer opciones locales de búfer!
¡Si establecen opciones a nivel global, las sobrescribirán para todos los buffers abiertos!



### ~/.vim/indent/

Los archivos dentro de `~/.vim/indent/` son muy parecidos los archivos dentro de `ftplugin/`, estos son cargados basandose en sus nombres.

Los archivos `indent` deben establecer opciones relacionadas con la indentacion para sus tipos de archivos, y esas opciones deben ser
locales al buffer.

Si, simplemente se podria poner ese codigo en los archivos `ftplugin`, pero es mejor separarlo para que otros usuarios de Vim entiendan
lo que esta haciendo. Es solamente una convencion, pero seamos considerados y sigamos las convenciones.



### ~/.vim/compiler/

Los archivos dentro de `~/.vim/compiler/` funcionan exactamnte igual que los archivos `indent`, con la diferencia que estos
archivos establecen opciones relacionadas con el compilador usado en el buffer actual basado en sus nombres.

En realidad no se preocupe por lo que significa *opciones relacionadas con el compilador* en este momento. Lo cubriremos mas tarde.



### ~/.vim/after/

El directorio `~/.vim/after/` es un poco *hack*. Los archivos dentro de este directorio se cargaran cada vez que se inicie Vim,
pero después de que se carguen los archivos de `~/.vim/plugin/`.

Esto le permite sobreescribir los archivos internos de Vim.
En la práctica, rara vez necesitará esto, así que no se preocupe por eso hasta que se encuentre pensando "Vim establece la opción x, pero yo quiero algo diferente".



### ~/.vim/autoload/

El directorio `~/.vim/autoload/` es un *hack* increíblemente importante.
Suena mucho más complicado de lo que realmente es.

En una palabra: `autoload` es una forma de retrasar la carga del código de su plugin hasta que sea realmente necesario.
Cubriremos esto con más detalle más adelante cuando refactoricemos el código de nuestro plugin para aprovecharlo.



### ~/.vim/doc/

Finalmente, el directorio `~/.vim/doc/` es donde puede agregar documentación para su plugin.
Vim se enfoca mucho en la documentación (como lo demuestran todos los comandos de ayuda que hemos estado ejecutando),
por lo que es importante documentar sus plugins.



### Ejercicios

Vuelve a leer este capítulo. No estoy bromeando.
Asegúrese de comprender (de una manera muy aproximada) lo que hace cada directorio del que hemos hablado.

De manera adicional puede mirar en algunos plugins de Vim que usa y observar cómo estructuran sus archivos.
