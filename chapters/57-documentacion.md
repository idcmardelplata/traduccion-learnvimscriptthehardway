
# Documentación

Nuestro plugin de Potion tiene muchas funciones útiles, pero no será realmente útil para nadie a menos que lo documentemos para que sepan lo que puede hacer.

La propia documentación de Vim es excelente.
No es demasiado prolija, pero es extremadamente minuciosa.
También ha inspirado a muchos autores de plugins a documentar muy bien sus propios plugins, lo que ha resultado en una maravillosa cultura de documentación sólida en la comunidad Vimscript.

### ¿Como funciona la documentacion?

Cuando lee un `:help` en Vim, seguramente ha notado que algunas cosas estan resaltadas de manera diferente a otras.
Echemos un vistazo a cómo funciona esto.

Abra cualquier tema de ayuda (como `:help help`) y ejecute `:set filetype?`. Vim mostrará `filetype=help`.
Ahora ejecute :set `filetype=text`, y verá que el resaltado desaparece. Escriba `:set filetype=help` nuevamente y volverá.

¡Resulta que los archivos de ayuda de Vim son simplemente archivos de texto resaltados en sintaxis como cualquier otro formato de archivo!
Esto significa que puede escribir el suyo y obtener el mismo resaltado.

Cree un archivo llamado `doc/potion.txt` en su repositorio de plugins.
`Vim/Pathogen` busca archivos dentro de las carpetas `doc` al indexar los temas de ayuda, por lo que aquí es donde escribiremos la ayuda para nuestro plugin.

Abra este archivo en Vim y ejecute `:set filetype=help` para que pueda ver el resaltado de sintaxis mientras escribe.

### Encabezado de ayuda

El formato de los archivos de ayuda es una cuestión de gusto personal.
Dicho esto, hablaré sobre una forma de estructurarlos que parece ser popular entre la comunidad moderna de Vimscript.

La primera línea del archivo debe contener el nombre de archivo del archivo de ayuda, seguido de una descripción de una línea de lo que hace el plugin.
Agregue lo siguiente como la primera línea de su archivo `potion.txt`:

```txt
*potion.txt* functionality for the potion programming language
```

Rodear una palabra con asteriscos en un archivo de ayuda crea una "etiqueta" a la que se puede saltar.
Ejecute `:Helptags` para indicarle a Pathogen que reconstruya el índice de etiquetas de ayuda, y luego abra una nueva ventana de Vim y ejecute
`:help potion.txt`. Vim abrirá su documento de ayuda como cualquier otro.

A continuación, debe poner el título de su plugin junto con una descripción más larga.
A algunos autores (incluyéndome a mí) les gusta divertirnos un poco con esto y usar un poco de arte ASCII para darle vida a las cosas.
Agregue una bonita sección de título al archivo `potion.txt`:

```txt
*potion.txt* functionality for the potion programming language

                      ___      _   _              ~
                     / _ \___ | |_(_) ___  _ __   ~
                    / /_)/ _ \| __| |/ _ \| '_ \  ~
                   / ___/ (_) | |_| | (_) | | | | ~
                   \/    \___/ \__|_|\___/|_| |_| ~

          Functionality for the Potion programming language.
        Includes syntax highlighting, code folding, and more!
```

Obtuve esas divertidas letras ejecutando el comando `figlet -f ogre "Potion"`. `Figlet` es un pequeño gran programa para generar texto artístico ASCII.
Los caracteres `~` al final de las líneas aseguran que Vim no intente resaltar u ocultar caracteres individuales dentro del arte.

### ¿Que documentar?

Luego, generalmente viene una tabla de contenido.
Primero, sin embargo, decidamos qué queremos documentar realmente.

Cuando escribo documentación para un nuevo plugin, generalmente comienzo con la siguiente lista de secciones y trabajo desde allí:

* Introduction
* Usage
* Mappings
* Configuration
* License
* Bugs
* Contributing
* Changelog
* Credits

Si el plugin es grande y requiere un "overview", escribo una sección introductoria que resume cómo funcionan las cosas.
De lo contrario, me saltearé eso y seguiré adelante.

Una sección de "uso" debería explicar, en general, cómo el usuario realmente usará su plugin.
Si van a interactuar con él a través de mapeos, digales eso.
Si no hay demasiados mapeos, simplemente puede enumerarlos aquí; de lo contrario, puede crear una sección separada de "mapeos" que los enumere todos.

La sección "configuración" debe enumerar todas y cada una de las variables modificables por el usuario, junto con sus efectos y su valor predeterminado.

La sección "licencia" debe especificar en qué licencia se encuentra el código del plugin, así como una URL donde el usuario puede encontrar el texto completo de esa licencia.
No incluya el texto completo en el archivo de ayuda real, la mayoría de los usuarios saben lo que significan las licencias comunes y esto simplemente llena las cosas.

La sección de "errores" debe ser corta y dulce.
Enumere cualquier error importante que conozca pero que no haya solucionado, y dígale al usuario cómo puede informarle de los nuevos errores que encuentre.

Si desea que sus usuarios puedan contribuir con correcciones de errores y características para el plugin, necesitarán saber cómo hacerlo.
¿Deberían enviar un pull request en GitHub? ¿En Bitbucket? ¿en Gitlab? ¿Enviar un parche a un correo electrónico? ¿Alguno / todo lo anterior?
Incluir una sección de "contribución" deja en claro cómo prefiere recibir el código.

Es maravilloso incluir un registro de cambios para que cuando los usuarios actualicen su plugin de la versión X a la versión Y puedan ver de inmediato lo que cambió. Además, resalto que le recomiendo que elija un esquema de versiones sensatas como el control de versiones semántico para su plugin y que se adhiera a él.
Sus usuarios se lo agradecerán.

Finalmente, me gusta incluir una sección de "créditos" para mencionar mi propio nombre, enumerar otros plugins en los que este se inspiró, agradecer a los colaboradores, y así sucesivamente.

Este suele ser un buen punto de partida.
Para plugins mas específicos, puede sentir la necesidad de desviarse de esta lista, y eso está completamente bien.
No hay reglas estrictas y rápidas, excepto las siguientes:

* Sea minucioso
* No sea demasiado prolijo.
* Lleve al usuario en un viaje desde no tener idea de cuál es su plugin hasta ser un usuario experto en él.

### Tabla de contenido

Ahora que tenemos una idea de qué secciones incluiremos, agregue lo siguiente al archivo `potion.txt`:

```txt
====================================================================
CONTENTS                                            *PotionContents*

    1. Usage ................ |PotionUsage|
    2. Mappings ............. |PotionMappings|
    3. License .............. |PotionLicense|
    4. Bugs ................. |PotionBugs|
    5. Contributing ......... |PotionContributing|
    6. Changelog ............ |PotionChangelog|
    7. Credits .............. |PotionCredits|
```

Hay un par de cosas a tener en cuenta aquí.
Primero, la línea de caracteres = será sintaxis resaltada.
Puede usar estas líneas para dividir visualmente las secciones de su documento de ayuda.
También puede usar líneas de caracteres `-` para subsecciones, si lo desea.

* `PotionContents` * creará otra etiqueta, por lo que un usuario puede escribir `:help PotionContents` e ir directamente a la tabla de contenido.

Cada una de las palabras rodeadas por | caracteres crea un enlace a una etiqueta.
Los usuarios pueden colocar el cursor sobre la palabra en el archivo de ayuda y presionar <c -]> para saltar a la etiqueta, como si hubieran escrito
`:help TheTag`. También pueden hacer clic en ellos con el mouse.

Vim ocultará estos caracteres `*` y `|` y la sintaxis los resaltara, por lo que el resultado será una buena y bonita tabla de contenido que las personas pueden usar para llegar a lo que están buscando.

# Secciones

Puede crear encabezados de sección como este:

```txt
====================================================================
Section 1: Usage                                       *PotionUsage*

This plugin with automatically provide syntax highlighting for
Potion files (files ending in .pn).

It also...
```

Asegúrese de crear los tags correctos con los caracteres `*` para que todos los enlaces en su tabla de contenido funcionen correctamente.

Continúe y cree encabezados para cada sección en la tabla de contenido.

### Ejemplos

Podría intentar repasar toda la sintaxis de los archivos de ayuda y cómo usarlos, pero es realmente una cuestión de gustos.
Entonces, en su lugar, le daré una lista de varios plugins de Vim con una extensa documentación.

Para cada uno, copie la fuente sin formato de la documentación en un búfer de Vim y configure su tipo de archivo en `help ` para ver cómo se representa.
Vuelva a `text` cuando quiera ver cómo se creó un efecto.

Puede resultarle útil utilizar sus habilidades de Vimscript para crear un mapeo de teclas para alternar los tipos de archivo de `help` y `text` para el búfer actual.


* [ Clam ](https://github.com/sjl/clam.vim/blob/master/doc/clam.txt), mi propio plugin para trabajar con comandos de shell. Es un ejemplo bastante corto que afecta a la mayoría de las secciones de las que hablé.

* El [ NEERD tree ](https://github.com/scrooloose/nerdtree/blob/master/doc/NERD_tree.txt) es un plugin de navegación de archivos de Scrooloose. Tenga en cuenta la estructura general, así como también cómo resume las asignaciones en una lista fácil de leer antes de explicar cada una en detalle.

* [ Surround ](https://github.com/tpope/vim-surround/blob/master/doc/surround.txt), es un plugin para manejar caracteres "envueltos" de Tim Pope. Tenga en cuenta la falta de una tabla de contenido, el estilo diferente de los encabezados de sección y los encabezados de columna de la tabla. Averigüe cómo se hicieron estas cosas y decida si le gustan. Es cuestión de gustos

* [ Splice ](https://github.com/sjl/splice.vim/blob/master/doc/splice.txt), mi propio plugin para resolver conflictos de fusión de tres vías en sistemas de control de versiones. Observe cómo se formatean las listas de asignaciones y cómo utilicé diagramas de arte ASCII para explicar los diseños. A veces una imagen realmente vale más que mil palabras.

Recuerde que toda la documentación de Vanilla Vim también se puede utilizar como ejemplo.
Eso debería darle mucho para estudiar y aprender.

### Escriba!

Ahora que ha visto cómo algunos otros plugins estructuraron y escribieron su documentación, complete las secciones para su plugin Potion.

Si no está acostumbrado a escribir documentación técnica, esto podría ser un desafío.
Aprender a escribir ciertamente no es simple, pero al igual que cualquier otra habilidad definitivamente requiere práctica,
¡así que solo hagalo! No necesita ser perfecto y siempre puede mejorarlo más tarde.

No tenga miedo de escribir algo de lo que no esté completamente seguro y luego tírelo y vuelva a escribirlo más tarde.
A menudo, solo poner algo en el búfer hará que su mente esté de humor para escribir.
Todavía estará en el control de versiones si alguna vez quiere recuperarlo.

Una buena manera de comenzar es imaginar que tienes un amigo que también usa Vim sentado a su lado.
Nunca antes han usado su plugin, pero están intrigados y su objetivo es convertirlo en un usuario experto.
Pensar en explicarle cosas a un ser humano real lo ayudará a mantenerse en tierra y evitar ser demasiado técnico antes de establecer una buena visión general de cómo funcionan las cosas.

Si todavía está atascado y siente que no está preparado para el desafío de escribir la documentación para un plugin completo, intente hacer algo más pequeño.
Elija un mapeo en su archivo `~/.vimrc` y documentelo completamente en un comentario.
Explique para qué sirve, cómo usarlo y cómo funciona. Por ejemplo, esto está en mi propio archivo `~/.vimrc`:

```vimscript
" "Uppercase word" mapping.
"
" This mapping allows you to press <c-u> in insert mode to convert the
" current word to uppercase.  It's handy when you're writing names of
" constants and don't want to use Capslock.
"
" To use it you type the name of the constant in lowercase.  While
" your cursor is at the end of the word, press <c-u> to uppercase it,
" and then continue happily on your way:
"
"                            cursor
"                            v
"     max_connections_allowed|
"     <c-u>
"     MAX_CONNECTIONS_ALLOWED|
"                            ^
"                            cursor
"
" It works by exiting out of insert mode, recording the current cursor
" location in the z mark, using gUiw to uppercase inside the current
" word, moving back to the z mark, and entering insert mode again.
"
" Note that this will overwrite the contents of the z mark.  I never
" use it, but if you do you'll probably want to use another mark.
inoremap <C-u> <esc>mzgUiw`za
```

Es mucho más corto que la documentación para un plugin completo, pero es un buen ejercicio que lo ayudará a practicar la escritura.
También es muy útil para las personas que leen su `~/.vimrc` si lo coloca en Bitbucket o GitHub o Gitlab.

### Ejercicios

Escriba la documentación para cada sección del plugin Potion.

Lea `:help help-writing` para obtener ayuda sobre cómo escribir ayuda.

Lea `:help :left`, `:help :right` y `:help :center` para conocer tres comandos útiles para que su arte ASCII sea perfecto.
