
# Caso de estudio, Operador Grep, parte uno.

En este capítulo y el siguiente, veremos cómo crear un snippet de Vimscript bastante complicado.
Hablaremos de varias cosas que no hemos visto antes, así también cómo algunas de las cosas que hemos estudiado encajan en la práctica.

Mientras trabaja en este caso de estudio, asegúrese de buscar cualquier cosa que no conozca en la ayuda `:help`.
Si pasa por alto sin entenderlo todo, no aprenderá mucho.

### Grep

Si nunca ha usado `:grep`, tómese un minuto para leer `:help :grep` y `:help: make` ahora.
Lea `:help quickfix-window` si nunca ha usado `quickfix` antes.

En pocas palabras `:grep ...` ejecutará un programa `grep` externo con los argumentos que proporcione,
analizará el resultado y llenará la lista de `quickfix` para que pueda saltar a los resultados dentro de Vim.

Nuestro ejemplo hará que sea más fácil invocar al comando `grep` al agregar un "operador grep" que puede usar con cualquiera de los movimientos incorporados
(o personalizados) de Vim para seleccionar el texto que desea buscar.

### Uso

Lo primero en lo que debe pensar al crear cualquier pieza de Vimscript no trivial es:
"¿cómo se utilizará esta funcionalidad?".
Trate de encontrar una manera suave, fácil e intuitiva de invocarlo.


Esta vez voy a hacer ese paso por usted:

* Vamos a crear un "operador grep" y enlazarlo a `<leader>g`.
* Actuará como cualquier otro operador de Vim y tomará un movimiento (como `w` o `i{`).
* Realizará la búsqueda inmediatamente y abrirá la ventana de `quickfix` para mostrar los resultados.
* No saltará al primer resultado, ya que puede ser discordante si el primer resultado no es lo que está esperando.

Algunos ejemplos de cómo podríamos usarlo:


* `<leader>giw`   : Grep para la palabra debajo del cursor.
* `<leader>giW`   : Grep para la PALABRA debajo del cursor.
* `<leader>gi'`   : Grep para el contenido entre comillas simples.
* `viwe<leader>g` : seleccione visualmente una palabra, extiende la selección hasta el final de la palabra que la sigue, luego usa grep para el texto seleccionado.

Hay muchas, muchas otras formas de usar esto.
Puede parecer que tomará una gran cantidad de codigo, pero en realidad todo lo que tenemos que hacer es implementar la funcionalidad de "operador" y Vim se encargará del resto.

### Un bosquejo preliminar

Una cosa que a veces es útil cuando se escriben fragmentos complicados de Vimscript es simplificar su objetivo
e implementarlo para tener una idea de la "forma" que tomará su solución final.


Simplifiquemos nuestro objetivo de: "crear un mapeo para buscar la palabra debajo del cursor".
Esto es útil pero debería ser fácil, para que podamos ejecutar algo mucho más rápido.
Mapearemos esto a `<leader>g` por ahora.

Comenzaremos con un esqueleto del mapeo y lo completaremos a medida que avancemos.
Ejecute este comando:


```vimscript
:nnoremap <leader>g :grep -R something .<cr>
```

Si ha leído `:help grep`, esto debería ser bastante fácil de entender.
Hemos visto muchos mapeos antes, y no hay nada nuevo aquí.

Obviamente no hemos terminado todavía,
así que refinemos este mapeo hasta que cumpla con nuestro objetivo simplificado.

### El término de búsqueda

Primero necesitamos buscar la palabra debajo del cursor, no el string somethig.
Ejecute el siguiente comando:


```vimscript
:nnoremap <leader>g :grep -R <cword> .<cr>
```

Ahora pruébelo. `<cword>` es un bit especial de texto que puede usar en el modo de línea de comandos de Vim,
y Vim lo reemplazará con "la palabra debajo del cursor" antes de ejecutar el comando.


Puede usar `<cWORD>` para obtener una PALABRA en lugar de una palabra.
Ejecute este comando:


```vimscript
:nnoremap <leader>g :grep -R <cWORD> .<cr>
```

Ahora intente el mapeo cuando el cursor esté sobre algo como `foo-bar`.
Vim usara grep para `foo-bar` en lugar de solo una parte de la palabra.

Todavía hay un problema con nuestro término de búsqueda:
si hay algún caracter especial de shell en él, Vim los pasará felizmente al comando externo grep, que explotará (o peor: hará algo terrible).

Siga adelante e intente esto para asegurarse de que se rompa.
Escriba `foo;ls` en un archivo y ejecuta el mapeo mientras el cursor está sobre él.
El comando grep fallará, y Vim ejecutará un comando `ls` también.
Claramente, esto podría ser malo si la palabra contenía un comando más peligroso que `ls`.

Para tratar de arreglar esto, entrecomillaremos el argumento en la llamada grep.
Ejecute este comando:


```vimscript
:nnoremap <leader>g :grep -R '<cWORD>' .<cr>
```

La mayoría de los shells tratan el texto entrecomillados simples como (casi) literal, por lo que nuestro mapeo es mucho más sólida ahora.

### Escapar de los argumentos del comando shell

Todavía hay un problema más con el término de búsqueda.
Pruebe el mapeo en la palabra `that's`.
¡No funcionará, porque la comilla simple dentro de la palabra interfiere con las comillas en el comando grep!

Para solucionar esto podemos usar la función `shellescape` de Vim. Lea `:help escape()` y `:help shellescape()` para ver cómo funciona (es bastante simple).

Debido a que `shellescape()` funciona en strings de Vim, necesitaremos compilar dinámicamente el comando con `execute`.
Primero ejecute el siguiente comando para transformar el mapeo `:grep` en `:ejecute "..."` de la siguiente forma:


```vimscript
:nnoremap <leader>g :execute "grep -R '<cWORD>' ."<cr>
```

Pruébelo y asegúrese de que todavía funciona. Si no, encuentre algún error tipográfico y corrijalo.
Luego ejecute el siguiente comando, que usa `shellescape` para arreglar el término de búsqueda:

```vimscript
:nnoremap <leader>g :execute "grep -R " . shellescape("<cWORD>") . " ."<cr>
```

Pruébelo ejecutándolo en una palabra normal como foo.
Funcionará correctamente. Ahora pruébalo en una palabra con una cita, como si eso fuera.
¡Todavía no funciona! ¿Que pasó?

El problema es que Vim realizó la llamada `shellescape()` antes de expandir strings especiales como `<cWORD>` en la línea de comandos.
Así que Vim-shell escapó de la cadena literal "<cWORD>" (que no hizo nada más que agregarle comillas simples) y luego la concatenó con las cadenas de nuestro comando grep.

Puede ver esto ejecutando el siguiente comando:


```vimscript
:echom shellescape("<cWORD>")
```

Vim mostrara `'<cWORD>'`. Tenga en cuenta que esas comillas son en realidad parte de la cadena.
Vim lo ha preparado para su uso como un argumento de comando de shell.

Para solucionar esto, usaremos la función `expand()` para forzar la expansión de `<cWORD>` en el string real antes de que se pase a shellescape.

Rompamos esto y veamos cómo funciona, en pasos.
Coloque el cursor sobre una palabra con una comilla simple, como `that's`, y ejecute el siguiente comando:


```vimscript
:echom expand("<cWORD>")
```

Las salida de Vim se debe a que `expand("<cWORD>")` devolverá la palabra actual debajo del cursor como un string de Vim.
Ahora vamos a agregar `shellescape` de nuevo en:


```vimscript
:echom shellescape(expand("<cWORD>"))
```

Esta vez Vim muestra `'that'\''s'`. Si esto parece un poco gracioso, probablemente no haya tenido el placer de envolver su cerebro en `shell-quoting` en toda su loca gloria.

Por ahora, no se preocupe por eso. Solo confíe en que Vim ha tomado la cadena de `expand` y se ha escapado correctamente.

Ahora que sabemos cómo obtener una versión completamente escapada de la palabra debajo del cursor, ¡es hora de concatenarla en nuestro mapeo!
Ejecute el siguiente comando:


```vimscript
:nnoremap <leader>g :exe "grep -R " . shellescape(expand("<cWORD>")) . " ."<cr>
```

Pruébalo. Este mapeo no se romperá si la palabra que buscamos contiene caracteres extraños.

El proceso de comenzar con un trivial bit de Vimscript y transformarlo poco a poco en algo más cercano a su objetivo es uno que usará a menudo.


### Limpiando

Todavía hay un par de cosas pequeñas que cuidar antes de que finalice nuestro mapeo.
Primero, dijimos que no queremos ir al primer resultado automáticamente, y podemos usar `grep!` en lugar de `grep` simple para hacer eso.
Ejecute este comando:


```vimscript
:nnoremap <leader>g :execute "grep! -R " . shellescape(expand("<cWORD>")) . " ."<cr>
```

Inténtelo de nuevo y nada parecerá que sucede.
Vim ha llenado la `quickfix` con los resultados, pero aún no la hemos abierto.
Ejecute el siguiente comando:


```vimscript
:nnoremap <leader>g :execute "grep! -R " . shellescape(expand("<cWORD>")) . " ."<cr>:copen<cr>
```

Ahora intente el mapeo y verá que Vim abre automáticamente la ventana de `quickfix` con los resultados de la búsqueda.
Todo lo que hicimos fue agregar :`copen <cr>` al final de nuestro mapeo.

Como toque final, eliminaremos todas las salidas de grep que Vim muestra durante la búsqueda.
Ejecute el siguiente comando:


```vimscript
:nnoremap <leader>g :silent execute "grep! -R " . shellescape(expand("<cWORD>")) . " ."<cr>:copen<cr>
```

Hemos terminado, así que pruébelo y admire su arduo trabajo.
El comando `silent` simplemente ejecuta el comando que lo sigue mientras oculta cualquier mensaje que normalmente mostraría.

### Ejercicios

* Agregue el mapeo que acabamos de crear a su archivo `~/.vimrc`.
* Lea `:help :grep` si no lo ha leido previamente.
* Lea `:help cword`.
* Lea `:help cnext` y `:help cprevious`. Pruébalos después de usar tu nuevo mapeo grep.
* Configure mapeos para `:cnext` y `:cprevious` para que sea más fácil ejecutar rápidamente las coincidencias.
* Lea `:help expand`
* Lea `:help copen`
* Agregue una altura al comando `:copen` en el mapeo que creamos para asegurarnos de que la ventana de `quickfix` esté abierta a la altura que prefiera.
* Lea `:help silent`
