
# Detectando el tipo de archivo

Creemos un archivo de `Potion` que podamos usar como muestra mientras trabajamos en nuestro plugin.
Para ello, cree un archivo llamado `factorial.pn` en algún lugar y coloque el siguiente código de `Potion` dentro:

```potion
factorial = (n):
    total = 1
    n to 1 (i):
        total *= i.
    total.

10 times (i):
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.
```

Este código crea una función factorial simple y la llama diez veces, imprimiendo los resultados cada vez.
Siga adelante y ejecútelo con `Potion factorial.pn`. La salida debería verse así:

```potion
0! is: 0
1! is: 1
2! is: 2
3! is: 6
4! is: 24
5! is: 120
6! is: 720
7! is: 5040
8! is: 40320
9! is: 362880
```

Si no obtiene esta salida, o si obtiene un error, deténgase y descubra qué salió mal.
El código debería funcionar bien exactamente como está.

Tómese un tiempo para comprender cómo funciona el código.
Consulte los documentos de Potion. No es algo crítico para entender Vimscript, pero lo convertirá en un mejor programador.


## Detectar archivos de Potion

Abra `factorial.pn` en Vim y ejecute el siguiente comando:

```vimscript
:set filetype?
```

Vim mostrará `filetype=` porque aún no sabe como funciona un archivo .pn. Vamos a arreglar eso.

Cree el fichero `ftdetect/potion.vim` en el repositorio de su plugin. Y ponga las siguientes líneas en él:

```vimscript
au BufNewFile,BufRead *.pn set filetype=potion
```

Esto crea un autocomando: un comando para establecer el tipo de archivo de los archivos terminados con extensión `.pn` de `Potion`.
Muy claro.

Tenga en cuenta que no usamos un grupo de autocomandos como lo haríamos normalmente.
Vim envuelve automáticamente el contenido de los archivos `ftdetect/*.vim` en grupos de autocomando para usted, por lo que no necesita preocuparse por ello.

Cierre el archivo `factorial.pn` y vuelva a abrirlo.
Ahora ejecute el comando anterior nuevamente:


```vimscript
:set filetype?
```

Esta vez, Vim muestra `filetype=potion`.
Cuando Vim se inició, cargó el grupo de autocomando dentro de `~/.vim/bundle/potion/ftdetect/potion.vim`,
y cuando abrió `factorial.pn` se activó el autocomando, configurando el tipo de archivo en Potion.

Ahora que le hemos enseñado a Vim a reconocer los archivos de Potion, podemos pasar a crear realmente un comportamiento útil en nuestro plugin.

## Ejercicios


* Leer `:help ft`. No se preocupe si no entiende todo lo que hay allí.
* Leer `:help setfiletype`.
* Modifique el script `ftdetect/potion.vim` del plugin `Potion` para usar `setfiletype` en lugar de `set filetype`.
