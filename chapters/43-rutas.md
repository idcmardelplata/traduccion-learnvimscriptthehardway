
# Rutas

Vim es un editor de texto, y los editores de texto (generalmente) trabajan con archivos de texto.
Los archivos de texto viven en sistemas de archivos, y para especificar archivos utilizamos rutas.
Vimscript tiene algunas utilidades integradas que pueden ser extremadamente útiles cuando necesita trabajar con rutas.

### Rutas absolutas

A veces es útil poder obtener la ruta absoluta de un determinado archivo para usarlo con scripts externos.
Ejecute los siguientes comandos:

```vimscript
:echom expand('%')
:echom expand('%:p')
:echom fnamemodify('foo.txt', ':p')
```

El primer comando muestra la ruta relativa de cualquier archivo que esté editando actualmente. `%` significa "el archivo actual".
Vim también admite otros strings que puede usar con `expand()`.

El segundo comando muestra la ruta completa y absoluta de ese archivo. El `:p` en el string le dice a Vim que desea la ruta absoluta.
Hay muchos otros modificadores que puede usar.

El tercer comando muestra una ruta absoluta al archivo `foo.txt` en el directorio actual, independientemente de si ese archivo realmente existe.
`fnamemodify()` es una función de Vim que es más flexible que `expand()` ya que puede especificar cualquier nombre de archivo,
no solo uno de los strings especiales de `expand()`.

### Listando archivos

También es posible que desee obtener una lista de archivos en un directorio específico. Ejecute el siguiente comando:

```vimscript
:echo globpath('.', '*')
```

Vim mostrará todos los archivos y directorios en el directorio actual. La función `globpath()` devuelve un string, con cada nombre separado por una nueva línea.
Para obtener una lista deberá usar `split()` usted mismo. Ejecute este comando:

```vimscript
:echo split(globpath('.', '*'), '\n')
```

Esta vez, Vim muestra una lista de Vimscript que contiene cada ruta. Si tiene nuevas líneas en sus nombres de archivo, está solo, lo siento.

Los comodines de `globpath()` funcionan principalmente como es de esperar. Ejecute el siguiente comando:

```vimscript

:echo split(globpath('.', '*.txt'), '\n')
```

Vim muestra una lista de todos los archivos `.txt` en el directorio actual.

Puede enumerar archivos de forma recursiva con `**`. Ejecute este comando:

```vimscript
:echo split(globpath('.', '**'), '\n')
```

Vim enumerará todos los archivos y directorios en el directorio actual.


`globpath()` es extremadamente poderoso. Aprenderá más cuando complete los ejercicios de este capítulo.

### Ejercicios

* Lea `:help expand()`
* Lea `:help fnamemodify()`
* Lea `:help filename-modifiers`
* Lea `:help simplify()`
* Lea `:help resolve()`
* Lea `:help globpath()`
* Lea `:help wildcards()`
