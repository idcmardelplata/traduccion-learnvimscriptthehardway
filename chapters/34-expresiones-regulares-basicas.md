
# Expresiones regulares básicas

Vim es un editor de texto, lo que significa que una gran parte de su código de Vimscript se dedicará a trabajar con texto.
Vim tiene un potente soporte para expresiones regulares, pero como es habitual hay algunas peculiaridades.

Escriba el siguiente texto en un búfer:


```python
max = 10

print "Starting"

for i in range(max):
    print "Counter:", i

print "Done"
```

Este es el texto que usaremos para experimentar con el soporte de expresiones regulares de Vimscript.
Sucede que es un código Python, pero no se preocupe si no conoces Python. Es solo un ejemplo.

Voy a asumir que sabe lo básico de las expresiones regulares.
Si no es así, debes dejar de leer este libro y comienza a leer [Learn Regex the Hard Way](http://regex.learncodethehardway.org/) de Zed Shaw. Vuelva cuando hayas terminado con eso.

### Resaltado de Sintaxis

Antes de comenzar, debemos activar el resaltado de búsqueda para poder ver lo que estamos haciendo.
Ejecute el siguiente comando:


```vimscript
:set hlsearch incsearch
```

`hlsearch` le dice a Vim que resalte todas las coincidencias en un archivo cuando realiza una búsqueda, e
`incsearch` le dice a Vim que resalte la próxima coincidencia mientras aún está escribiendo su patrón de búsqueda.

### Busqueda

Coloque el cursor en la parte superior del archivo y ejecute el siguiente comando:


```vimscript
/print
```

A medida que escribe cada letra, Vim comenzará a resaltarlas en la primera línea.
Cuando presiona `enter` para ejecutar la búsqueda, todas las instancias de `print` seran resaltadas y el cursor se moverá a la siguiente coincidencia.

Ahora intente ejecutar el siguiente comando:


```vimscript
:execute "normal! gg/print\<cr>"
```

Esto irá a la parte superior del archivo y realizará una búsqueda de `print`, lo que nos colocará en la primera coincidencia.
Lo hace usando `:execute "normal! ..."` que vimos en el capítulo anterior.

Para llegar a la segunda coincidencia en el archivo, simplemente puede agregar más comandos al final del string.
Ejecute este comando:


```vimscript
:execute "normal! gg/print\<cr>n"
```

Vim colocará el cursor en el segundo `print` en el búfer (y todas las coincidencias se resaltarán).

Vamos a tratar de ir en la dirección opuesta. Ejecute este comando:


```vimscript
:execute "normal! G?print\<cr>"
```

Esta vez nos movemos al final del archivo con `G` y usamos `?` para buscar hacia atrás en lugar de hacia adelante.

Todos estos comandos de búsqueda deben ser familiares,
la mayoría de ellos los revisamos para acostumbrarnos al lenguaje
`:execute "normal! ..."`, porque le permitirá hacer todo lo que sepa hacer en Vanilla Vim en su código de Vimscript.

### Magic

Los comandos `/` y `?` toman expresiones regulares, no solo caracteres literales.
Ejecute el siguiente comando:


```vimscript
:execute "normal! gg/for .+ in .+:\<cr>"
```

Vim se queja de que el patrón no se encuentra!
Le dije que Vim admite expresiones regulares en las búsquedas, entonces, ¿qué está pasando?
Pruebe el siguiente comando:


```vimscript
:execute "normal! gg/for .\\+ in .\\+:\<cr>"
```

Esta vez Vim resalta el bucle `"for"` como esperábamos en primer lugar.
Tómese un minuto y trate de pensar qué cambió exactamente antes de continuar.
Recuerda que `execute` ejecuta un String.


La respuesta es que hay dos razones por las que necesitamos escribir el comando como lo hicimos:

* Primero, `ejecute` toma una string, por lo que las barras invertidas dobles que usamos se convierten en barras invertidas simples cuando se normalizan.

* ¡Vim tiene cuatro "modos" diferentes de analizar expresiones regulares!
  El modo predeterminado requiere una barra invertida antes del carácter `+` para que signifique "1 o más veces el carácter anterior" en lugar de "un signo más literal".


Puede ver esto un poco más fácil simplemente ejecutando la búsqueda en Vim directamente.
Escriba el siguiente comando y presione enter:


```vimscript
/print .\+
```

Puede ver el `\+` haciendo su magia ahora.
Las barras invertidas dobles solo se usaron porque estábamos pasando el patrón como un string para ejecutar.

### Strings Literales

Como mencionamos en el capítulo sobre Strings, Vim le permite usar comillas simples para definir una "cadena literal" que pasa a través de los caracteres directamente.
Por ejemplo, la cadena `'a\nb'` tiene cuatro caracteres de longitud.

¿Podemos usar cadenas literales para evitar tener que escribir esas barras invertidas dobles? Piense en esto por un minuto o dos antes de continuar,
porque la respuesta es un poco más complicada de lo que podría pensar.

Intente ejecutar el siguiente comando (tenga en cuenta las comillas simples y las barras invertidas simples esta vez):


```vimscript
:execute 'normal! gg/for .\+ in .\+:\<cr>'
```

Vim lo mueve a la parte superior del archivo pero no lo mueve a la primera coincidencia. ¿Es esto lo que esperabas?

El comando no funciona porque necesitamos que la sequencia `\<cr`> en el patron, sea escapado en un carácter de retorno de carro real,
que indica al comando de búsqueda que se ejecute realmente.
Debido a que estamos en una cadena literal, es equivalente a escribir `/for.\+In.\+:\<cr>` en vainilla Vim, que obviamente no es lo que queremos.

¡Pero no pierda la esperanza! Recuerde que Vim le permite concatenar cadenas,
por lo que para comandos más grandes podemos usar esto para dividir la cadena en trozos más fáciles de leer.
Ejecute el siguiente comando:


```vimscript
:execute "normal! gg" . '/for .\+ in .\+:' . "\<cr>"
```

Esto concatena las tres cadenas más pequeñas antes de enviarlas para que se ejecuten,
y nos permite usar una cadena literal para la expresión regular mientras usamos cadenas normales para todo lo demás.


### Very Magic

Quizás se pregunte sobre los cuatro modos diferentes de análisis de expresiones regulares de Vimscript y
en qué se diferencian de las expresiones regulares a las que está acostumbrado en lenguajes como Python, Perl o Ruby.
Puede leer su documentación si realmente lo desea, pero si desea una solución sensata y fácil, siga leyendo.


Ejecute el siguiente comando:


```vimscript
:execute "normal! gg" . '/\vfor .+ in .+:' . "\<cr>"
```


Hemos dividido el patrón del resto del comando en su propia cadena literal de nuevo,
y esta vez comenzamos el patrón con `\v`.
Esto le dice a Vim que use su modo de análisis de expresiones regulares "very magic",
que es prácticamente lo mismo que está acostumbrado en cualquier otro lenguaje de programación.

Si simplemente inicia todas sus expresiones regulares con `\v`, nunca tendrá que preocuparse por los otros tres modos regex locos de Vimscript.

### Ejercicios

Lea `:help magic` cuidadosamente.

Lea `:help pattern-overview` para ver el tipo de cosas que Vim regexes admite. Deje de leer después de las clases de caracteres.

Edite su archivo `~/.vimrc` para agregar un mapeo que usará la coincidencia para resaltar los espacios en blanco al final como un error. Una buena tecla para usarla seria `<leader>w`.

Agregue otra asignación que borre la coincidencia anterior (quizás `<leader>W`).

Agregue un mapeo para el modo normal que inserte automáticamente `\v` cada vez que inicie una búsqueda. Si está atascado, recuerde que los mapeos de Vim son extremadamente simples y solo tiene que decirle qué teclas presionar cuando use la tecla asignada.

Agregue las opciones `hlsearch` e `incsearch` a su archivo `~/.vimrc`, configurelo como prefiera.

Lea `:help nohlsearch`. Tenga en cuenta que este es un comando y no la configuración de "modo off" de `hlsearch`!

Agregue un mapeo para "dejar de resaltar elementos de la última búsqueda" a su archivo `~/.vimrc`.
