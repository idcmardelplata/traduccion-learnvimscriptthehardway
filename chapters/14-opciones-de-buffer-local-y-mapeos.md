
# Buffers locales Opciones y Mapeos

Ahora vamos a tomar unos minutos para revisar tres cosas de las que ya hemos hablado: mapeos, abreviaturas y opciones, pero con un giro.
Vamos a configurar cada uno de ellos en un solo búfer a la vez.

El verdadero poder de esta idea se hará evidente en el próximo capítulo, pero debemos sentar las bases para ello ahora.

Para este capítulo, necesitará abrir dos archivos en Vim, cada uno en su propia división de ventana. Los llamaré `foo` y `bar`, pero puede nombrarlos como quiera.
Ponga un texto en cada uno de ellos.

### Mapeos

Cambie al archivo `foo` y ejecute los siguientes comandos:

```vimscript
:nnoremap          <leader>d dd
:nnoremap <buffer> <leader>x dd
```

Ahora permanezca en el archivo `foo`, asegúrese de estar en modo normal y escriba `<leader> d`. Vim borrará una línea. Esto no es nada nuevo.
Aún en el archivo `foo`, escriba `<leader> x`. Vim borrará una línea de nuevo. Esto tiene sentido porque también asignamos `<leader> x` a `dd`.

Ahora muévase al archivo `bar`. En el modo normal, escriba `<leader> d`. Una vez más, Vim borra la línea actual. Nada sorprendente aquí tampoco.

Ahora para el giro: mientras aún se encuentra en el archivo `bar`, escriba `<leader> x`.

En lugar de eliminar toda la línea, Vim acaba de eliminar un solo carácter. ¿Que pasó?

El `<buffer>` en el segundo comando `nnoremap` le dijo a Vim que solo considerara ese mapeo cuando estamos en el búfer donde lo definimos.

Cuando escribió `<leader> x` en el archivo `bar`, Vim no pudo encontrar un mapeo que coincidiera con el, por lo que lo trató como dos comandos: `<leader>` (que no hace nada por sí solo) y `x` (el comando normal para eliminar una solo carácter).

### Local Leader

En nuestro ejemplo, usamos `<leader> x` para nuestro mapeo local a nivel de búfer, pero esta es una forma incorrecta. En general, cuando crea un mapeo que solo se aplica a buffers específicos, debe usar `<localleader>` en lugar de `<leader>.`

El uso de dos teclas `<leader>` separadas proporciona una especie de "espacio de nombres" que le ayudará a mantener todas sus diversos mapeos directamente en su cabeza.

Es aún más importante cuando escribe un plugin para que otras personas lo utilicen. La convención de usar `<localleader>` para los mapeos locales evitará que su plugin sobrescriba el mapeo `<leader>` de otra persona que ha quemado cuidadosamente en sus dedos a lo largo del tiempo.

### Ajustes

En uno de los primeros capítulos del libro hablamos acerca de las opciones de configuración con `set`. Algunas opciones siempre se aplican a todo Vim, pero otras se pueden establecer como base para un búfer.

Cambie al archivo `foo` y ejecute el siguiente comando:

```vimscript
:setlocal wrap
```

Ahora cambie al archivo `bar` y ejecute este comando:

```vimscript
: setlocal nowrap
```

Haga su ventana de Vim más pequeña y verá que las líneas en `foo` fueron envueltas, pero las líneas en `bar` no.

Probemos otra opción. Cambie a `foo` y ejecute este comando:

```vimscript
:setlocal number
```

Ahora cambie a `bar` y ejecute este comando:

```vimscript
:setlocal nonumber
```

Ahora tiene números de línea en `foo` pero no en `bar`.

No todas las opciones se pueden utilizar con `setlocal`. Para ver si puede establecer una opción particular localmente, lea su `:help`.

He pasado por alto algunos detalles sobre cómo funcionan las opciones locales por ahora.
En los ejercicios aprenderá más sobre los detalles sangrientos.

### Ofuscación

Antes de continuar, veamos una propiedad particularmente interesante de los mapeos locales. Cambie a `foo` y ejecute los siguientes comandos:

```vimscript
:nnoremap <buffer> Q x
:nnoremap          Q dd
```

Ahora escribe `Q`. ¿Qué ha pasado?

Cuando presiona `Q`, Vim ejecutará la primera asignación, no la segunda, porque la primera asignación es más específica que la segunda.

Cambie al archivo `bar` y escriba `Q` para ver que Vim usa el segundo mapeo, porque no está ofuscado por el primero en este búfer.

### Ejercicios

Lea `:help local-options`

Lea `:help setlocal`

Lea `:help map-local`
