
# Movimiento de secciones en Potion

Ahora que sabemos cómo funciona el movimiento de sección,
vamos a remapear los comandos para que funcionen de una manera que tenga sentido para los archivos de Potion.

Primero tenemos que decidir qué "sección" debería significar para un archivo de Potion.
Hay dos pares de comandos de movimiento de sección, por lo que podemos crear dos "esquemas" y nuestros usuarios pueden usar el que prefieran.

Usemos los siguientes dos esquemas para definir dónde comienzan las secciones de Potion:

1. Cualquier línea que sigue a una línea en blanco que contiene espacios en blanco como el primer carácter o la primera línea del archivo.
2. Cualquier línea que contenga espacios en blanco como el primer carácter, un signo igual en algún lugar dentro de la línea y termina con dos puntos.

Usando una versión ligeramente expandida de nuestro archivo `factorial.pn` de muestra, esto es lo que estas reglas considerarán encabezados de sección:

```vimscript
# factorial.pn                              1
# Print some factorials, just for fun.

factorial = (n):                            1 2
    total = 1

    n to 1 (i):
        total *= i.

    total.

print_line = ():                            1 2
    "-=-=-=-=-=-=-=-\n" print.

print_factorial = (i):                      1 2
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.

"Here are some factorials:\n\n" print       1

print_line ()                               1
10 times (i):
    print_factorial (i).
print_line ()
```

Nuestra primera definición tiende a ser más liberal.
Define una sección para que sea aproximadamente un "fragmento de texto de nivel superior".

La segunda definición es más restrictiva. Define una sección para que sea (efectivamente) una definición de función.

### Mapeos personalizados

Cree un archivo `ftplugin/potion/sections.vim` en el repositorio de su plugin.
Aquí sera donde pondremos el código para el movimiento de la sección.
Recuerde que este código se ejecutará siempre que el `filetype` de un búfer esté configurado en `potion`.

Vamos a remapear los cuatro comandos de movimiento de sección, así que adelante y cree un archivo "esqueleto":

```vimscript
noremap <script> <buffer> <silent> [[ <nop>
noremap <script> <buffer> <silent> ]] <nop>

noremap <script> <buffer> <silent> [] <nop>
noremap <script> <buffer> <silent> ][ <nop>
```

Tenga en cuenta que usamos el comando `noremap` en lugar de `nnoremap`, porque queremos que funcionen también en modo de operador pendiente.
De esa manera podrá hacer cosas como `d]]` para "eliminar de aquí a la siguiente sección".

Hacemos que los mapeos sean locales en búfer para que solo se apliquen a los archivos de Potion y no funcionen a nivel gobal.

También los hacemos silenciosos, porque al usuario no le importan los detalles de cómo nos movemos entre las secciones.

### Usando una funcion

El código para realizar los movimientos de sección será muy similar para todos los diversos comandos,
así que vamos a resumirlo en una función que llamarán nuestros mapeos.

Verá esta estrategia en muchos plugins de Vim que crean una serie de mapeos similares.
Es más fácil de leer y mantener que incluir toda la funcionalidad en un montón de líneas de mapeo.

Cambie el archivo `sections.vim` para contener esto:

```vimscript
function! s:NextSection(type, backwards)
endfunction

noremap <script> <buffer> <silent> ]]
        \ :call <SID>NextSection(1, 0)<cr>

noremap <script> <buffer> <silent> [[
        \ :call <SID>NextSection(1, 1)<cr>

noremap <script> <buffer> <silent> ][
        \ :call <SID>NextSection(2, 0)<cr>

noremap <script> <buffer> <silent> []
        \ :call <SID>NextSection(2, 1)<cr>
```

Utilicé la caracteristica de continuación de línea larga de Vimscript aquí porque las líneas se estaban haciendo un poco largas para mi gusto.
Observe cómo la barra invertida para escapar de las líneas largas aparece al comienzo de la segunda línea.
Lea `:help line-continuation` para obtener más información.

Tenga en cuenta que estamos usando `<SID>` y una función de script local para evitar contaminar el espacio de nombres global con nuestra función auxiliar.

Cada mapeo simplemente llama a `NextSection` con los argumentos apropiados para realizar el movimiento. Ahora podemos comenzar a implementar `NextSection`.

### Movimiento base

Pensemos en lo que nuestra función necesita hacer.
Queremos mover el cursor a la siguiente "sección", y una forma fácil de mover el cursor a algún lugar es con los comandos `/` y `?`.

Edite `NextSection` para que se vea así:

```vimscript
function! s:NextSection(type, backwards)
    if a:backwards
        let dir = '?'
    else
        let dir = '/'
    endif

    execute 'silent normal! ' . dir . 'foo' . "\r"
endfunction
```

¡Ahora la función usa el patron `execute normal!` que hemos visto antes para realizar `/foo` o `?foo`, dependiendo del valor dado para `backwards`.
Este es un buen comienzo.

Continuando, obviamente tendremos que buscar algo más que `foo`, y ese patrón dependerá de si queremos usar la primera o segunda definición de encabezados de sección.

Cambie `NextSection` para que se vea así:

```vimscript
function! s:NextSection(type, backwards)
    if a:type == 1
        let pattern = 'one'
    elseif a:type == 2
        let pattern = 'two'
    endif

    if a:backwards
        let dir = '?'
    else
        let dir = '/'
    endif

    execute 'silent normal! ' . dir . pattern . "\r"
endfunction
```

Ahora solo necesitamos completar los patrones, así que sigamos adelante y hagámoslo.


### Secciones de texto Top Level

Reemplace la primera línea `let pattern = '...'` con lo siguiente:

```vimscript
let pattern = '\v(\n\n^\S|%^)'
```

Para entender cómo funciona la expresión regular, recuerde la definición de "sección" que estamos implementando:

> Cualquier línea que sigue a una línea en blanco que contiene un espacio no en blanco como el primer carácter, o la primera línea en el archivo


El `\v` al principio simplemente fuerza el modo "very magic" como hemos visto varias veces antes.

El resto de la expresión regular es un grupo con dos opciones. El primero, `\n\n^\S`, busca "una nueva línea, seguida de una nueva línea, seguida de un carácter que no sea un espacio en blanco".
Esto encuentra el primer conjunto de líneas en nuestra definición.

La otra opción es `%^`, que es un átomo de expresión regular de Vim que significa "comienzo del archivo".

Ahora estamos en un punto en el que podemos probar los dos primeras mapeos.
Guarde `ftplugin/potion/sections.vim` y ejecute `:set filetype=potion` en el búfer de muestra de potion.
Los comandos `[[` y `]]` deberían funcionar, pero algo extraño sucede.

### Flags de búsqueda

Notará que cuando se mueve entre las secciones, el cursor se coloca en la línea en blanco sobre la que realmente queremos mover.
Piense por qué sucede esto antes de seguir leyendo

La respuesta es que buscamos usando `/` (o `?` ) y, por defecto, Vim coloca el cursor al comienzo de las coincidencias.
Por ejemplo, cuando corre `/foo` su cursor se colocará en la `f` en `foo`.

Para decirle a Vim que coloque el cursor al final del la coincidencia en lugar del comienzo, podemos usar un flag de búsqueda.
Intente buscar en su archivo de Potion algo como esto:

```vimscript
/factorial/e
```

Vim encontrará la palabra `factorial` y lo moverá a ella.
Presione `n` algunas veces para moverse por las coincidencias.
La bandera `e` le dice a Vim que coloque el cursor al final de las coincidencias en lugar de al principio.
Pruébelo en la otra dirección también:

```vimscript
?factorial?e
```

Modifiquemos nuestra función para usar un flag de búsqueda para colocar nuestro cursor en el otro extremo de las coincidencias para esta sección:

```vimscript
function! s:NextSection(type, backwards)
    if a:type == 1
        let pattern = '\v(\n\n^\S|%^)'
        let flags = 'e'
    elseif a:type == 2
        let pattern = 'two'
        let flags = ''
    endif

    if a:backwards
        let dir = '?'
    else
        let dir = '/'
    endif

    execute 'silent normal! ' . dir . pattern . dir . flags . "\r"
endfunction
```

Hemos cambiado dos cosas aquí. Primero, establecemos una variable de `flag` dependiendo del tipo de movimiento de sección.
Por ahora solo nos preocupamos por el primer tipo, que necesitará un flag de `e`.

En segundo lugar, hemos concatenado `dir` y `flags` al string de búsqueda.
Esto agregará `?e` o `/e` dependiendo de en qué dirección estamos buscando.

Guarde el archivo, vuelva a su archivo de ejemplo de Potion y ejecute `:set ft=potion` para que los cambios surtan efecto.
¡Ahora intente `[[` y `]]` para ver que funcionan correctamente!

### Definiciones de función

Es hora de abordar nuestra segunda definición de "sección", y afortunadamente esta es mucho más sencilla que la primera.
Recordemos la definición que necesitamos implementar:

> Cualquier línea que contenga un espacio que no sea un espacio en blanco como primer carácter, un signo igual en algún lugar dentro de la línea y termine con dos puntos.

Podemos usar una expresión regular bastante simple para encontrar estas líneas.
Cambie la segunda línea `let pattern='...'` en la función a esto:

```vimscript
let pattern = '\v^\S.*\=.*:$'
```

Esta expresión regular debería parecer mucho menos aterradora que la anterior.
Lo dejaré como un ejercicio para que descubra cómo funciona: es una traducción bastante sencilla de nuestra definición.

Guarde el archivo, ejecute `:set filetype=potion` en `factorial.pn`, y pruebe los nuevos mapeos  `][` y `[]`. Deberían funcionar como se esperaba.

No necesitamos un flag de búsqueda aquí porque ponemos el cursor al comienzo de la coincidencia (el valor predeterminado) funciona bien.

### Modo Visual

Nuestros comandos de movimiento de sección funcionan muy bien en modo normal, pero necesitamos agregar un poco más para que funcionen también en modo visual.
Primero, cambie la función para que se vea así:

```vimscript
function! s:NextSection(type, backwards, visual)
    if a:visual
        normal! gv
    endif

    if a:type == 1
        let pattern = '\v(\n\n^\S|%^)'
        let flags = 'e'
    elseif a:type == 2
        let pattern = '\v^\S.*\=.*:$'
        let flags = ''
    endif

    if a:backwards
        let dir = '?'
    else
        let dir = '/'
    endif

    execute 'silent normal! ' . dir . pattern . dir . flags . "\r"
endfunction
```

Dos cosas han cambiado. Primero, la función toma un argumento adicional para saber si se llama desde el modo visual o no.
En segundo lugar, si se llama desde el modo visual, ejecutamos `gv` para restaurar la selección visual.

¿Por qué necesitamos hacer esto? Probemos algo que lo aclare.
Seleccione visualmente algún texto en cualquier búfer y luego ejecute el siguiente comando:

```vimscript
:echom "hello"
```

¡Vim mostrará `hello` pero la selección visual también se borrará!

Cuando se ejecuta un comando en modo ex con `:` la selección visual siempre se borra.
El comando `gv` vuelve a seleccionar la selección visual anterior, por lo que "deshacerá" el borrado.
Es un comando útil, y también puede ser útil en su trabajo diario.

Ahora necesitamos actualizar los mapeos existentes para pasar `0` para el nuevo argumento visual:

```vimscript
noremap <script> <buffer> <silent> ]]
        \ :call <SID>NextSection(1, 0, 0)<cr>

noremap <script> <buffer> <silent> [[
        \ :call <SID>NextSection(1, 1, 0)<cr>

noremap <script> <buffer> <silent> ][
        \ :call <SID>NextSection(2, 0, 0)<cr>

noremap <script> <buffer> <silent> []
        \ :call <SID>NextSection(2, 1, 0)<cr>
```

Nada demasiado complejo allí. Ahora agreguemos los mapeos de modo visual como la pieza final del rompecabezas:

```vimscript
vnoremap <script> <buffer> <silent> ]]
        \ :<c-u>call <SID>NextSection(1, 0, 1)<cr>

vnoremap <script> <buffer> <silent> [[
        \ :<c-u>call <SID>NextSection(1, 1, 1)<cr>

vnoremap <script> <buffer> <silent> ][
        \ :<c-u>call <SID>NextSection(2, 0, 1)<cr>

vnoremap <script> <buffer> <silent> []
        \ :<c-u>call <SID>NextSection(2, 1, 1)<cr>
```


Todas estos mapeos pasan `1` para que el argumento `visual` le diga a Vim que vuelva a seleccionar la última selección antes de realizar el movimiento.
También usan el truco `<c-u>` del que aprendimos en los capítulos del Operador Grep.


Guarde el archivo y ejecute `:set ft=potion` en el archivo de Potion y ¡listo! Pruebe sus nuevos mapeos.
Cosas como `v]]` y `d[]` deberían funcionar correctamente ahora.

### ¿Porque molestarse?


Este ha sido un capítulo largo para algunas funcionalidades aparentemente pequeñas, pero ha aprendido y practicado muchas cosas útiles en el camino:

* Usar `noremap` en lugar de `nnoremap` para crear mapeos que funcionen como movimientos .

* Usar una sola función con varios argumentos para simplificar la creación de mapeos relacionados.

* Creación de funcionalidad en una función Vimscript de forma incremental.

* Construir un string `execute 'normal! ... '` programáticamente.

* Usar búsquedas simples para moverse con expresiones regulares.

* Usar átomos de expresiones regulares especiales como `%^` (comienzo del archivo).

* Usar flags de búsqueda para modificar cómo funcionan las búsquedas.

* Manejo de mapeos de modo visual que necesitan retener la selección visual.

* Continúe y haga los ejercicios (es solo un poco de lectura de `:help` ) y luego tome un helado.
¡Se lo ha ganado después de este capítulo!

* Lea `:help search()`. Esta es una función útil para saber, pero también puede usar los flags con los comandos  con `/` y `?`

* Lea `:help ordinary-atom` para aprender sobre cosas más interesantes que puede usar en los patrones de búsqueda.
