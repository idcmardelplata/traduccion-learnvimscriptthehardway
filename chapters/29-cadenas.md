
# Strings

El siguiente tipo de variable que veremos es el `String`. Ya que Vim se trata de manipular texto, usara este tipo de dato bastante seguido.
Ejecute el siguiente comando:


```vimscript
:echom "Hello"
```

Vim hará `echo Hello`. Hasta ahora bien.

## Concatenación

Una de las cosas más comunes que querrá hacer con strings es agregarlas juntas. Ejecute este comando:


```vimscript
:echom "Hello, " + "world"
```

¿Que pasó? Vim mostró 0 por alguna razón!

Aquí está el problema: el operador de Vim `+` es solo para Números. Cuando le pasa un String a `+` Vim intentará forzarlo a un número antes de realizar la suma.
Ejecute el siguiente comando:


```vimscript
:echom "3 mice" + "2 cats"
```

Esta vez, Vim muestra 5, porque los strings están siendo transformados a los números 3 y 2 respectivamente.

Cuando dije "Número" realmente quise decir Número. ¡Vim no transformara los `Strings`  a `float`! Pruebe este comando para ver la prueba de esto:


```vimscript
:echom 10 + "10.10"
```

Vim muestra 20 porque eliminó todo después del punto decimal al transformar a 10.10 a un número.

Para combinar strings necesita usar el operador de concatenación. Ejecute el siguiente comando:


```vimscript
:echom "Hello, " . "world"
```

Esta vez Vim muestra `"Hello, Word"`. `.` es el operador para "concatenar strings" en Vim, que le permite combinar strings. No agrega espacios en blanco o cualquier otra cosa en el medio.

La coerción funciona en ambos sentidos. Mas o menos. Pruebe este comando:


```vimscript
:echom 10 . "foo"
```

Vim mostrará `10foo`. Primero obliga a que 10 sea transformado a un string, luego lo concatena con el string en el lado derecho. Sin embargo, las cosas se ponen un poco más complicadas cuando trabajamos con floats. Ejecute este comando:


```vimscript
:echom 10.1 . "foo"
```

Esta vez, Vim lanza un error, diciendo que estamos usando un float como un string. Vim le permitirá usar un string como float cuando realice una suma, pero no le permitirá usar un string como float cuando concatene.

La moraleja de esta historia es que Vim se parece mucho a Javascript: a veces le permite jugar rápido y suelto con tipos, pero es una muy mala idea hacerlo porque volverá a morderle en algún momento.

Al escribir Vimscript, asegúrese de saber cuál es el tipo de cada una de sus variables. Si necesita cambiar ese tipo, debe usar una función para cambiarlo explícitamente, incluso si no es estrictamente necesario en este momento. No confíe en la coacción de Vim porque en algún momento lo lamentará.

## Caracteres especiales

Como la mayoría de los lenguajes de programación, Vimscript le permite usar secuencias de escape en strings para representar caracteres difíciles de escribir. Ejecute el siguiente comando:


```vimscript
:echom "foo \"bar\""
```

El `\"` en el string se reemplaza con un carácter de comillas dobles, como probablemente esperaría. Las secuencias de escape funcionan principalmente como esperaba. Ejecute el siguiente comando:


```vimscript
:echom "foo\\bar"
```

Vim muestra `foo \ bar`, porque `\\` es la secuencia de escape para una barra invertida literal, como en la mayoría de los lenguajes de programación. Ahora ejecute el siguiente comando (tenga en cuenta que es un `echo` y no un `echom`):


```vimscript
echo "foo\nbar"
```

Esta vez, Vim mostrará dos líneas, foo y bar, porque el `\n` se reemplaza por una nueva línea. Ahora intente ejecutar este comando:


```vimscript
:echom "foo\nbar"
```


Vim mostrará algo como `foo^@bar`. Cuando use `echom` en lugar de `echo` con un String, Vim se hará `echo` de los caracteres exactos del string, lo que a veces significa que mostrará una representación diferente a la del echo antiguo. `^@` es la forma en que Vim dice "carácter de nueva línea".

## Strings literales

Vim también le permite usar "strings literales" para evitar el uso excesivo de secuencias de escape. Ejecute el siguiente comando:


```vimscript
:echom '\n\\'
```

Vim muestra `\n\\`. El uso de comillas simples le dice a Vim que desea que la cadena sea exactamente como está, sin secuencias de escape. La única excepción es que dos comillas simples en una fila producirán una comilla simple. Pruebe este comando:


```vimscript
:echom 'That''s enough.'
```

Vim mostrará. `That's enough`. Dos comillas simples es la única secuencia que tiene un significado especial en un string literal.

Revisaremos los strings literales cuando sean más útiles, más adelante en el libro (cuando nos sumerjamos en expresiones regulares).

## La verdad

Puede que se esté preguntando cómo trata Vim los strings cuando se usa en una sentencia `if`. Ejecute el siguiente comando:


```vimscript
:if "foo"
:  echo "yes"
:else
:  echo "no"
:endif
```

Vim mostrará no. Si se está preguntando por qué sucede esto, deberías volver a leer el capítulo sobre condicionales, porque hablamos de eso allí.

## Ejercicios

Lea `:help expr-quote`. Revise la lista de secuencias de escape que puede usar en un string normal de Vim. Descubra cómo insertar un carácter de tabulación.

Trate de encontrar una manera de insertar un carácter de tabulación en una string sin usar una secuencia de escape. Lea `:help i_CTRL-V` para una pista.

Lea `:help literal-string.`
