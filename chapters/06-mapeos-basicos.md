
# Mapeos Básicos

Si hay una característica de Vimscript que le permitirá someter a Vim a su voluntad más que a ninguna otra, es la capacidad de mapear teclas.
Las teclas de mapeo le permiten decirle a Vim:

* *Cuando presiono esta tecla, quiero que hagas esto en lugar de lo que harías normalmente.*

Vamos a comenzar mapeando teclas en el modo normal. Hablaremos acerca de cómo mapear las teclas en el modo insertar y otros modos en el siguiente capítulo.

Escriba algunas líneas de texto en un archivo y luego ejecute:

```vimscript
:map - x
```

Coloque el cursor en algún lugar del texto y presione `-`. Observe cómo Vim eliminó el carácter debajo del cursor, como si hubiera presionado `x`.

Ya tenemos una tecla para "eliminar el carácter debajo del cursor", así que vamos a cambiar ese mapeo a algo un poco más útil.

Ejecute este comando:

```vimscript
:map - dd
```

Ahora ponga el cursor en una línea en algún lugar y presione `-` otra vez. Esta vez Vim borra toda la línea, porque eso es lo que hace `dd`.

### Caracteres especiales

Puede usar `<keyname>` para informarle a Vim sobre teclas especiales. Intente ejecutar este comando:

```vimscript
:map <space> viw
```

Coloque el cursor sobre una palabra en su texto y presione la barra espaciadora. Vim seleccionará visualmente la palabra.

También puede asignar teclas modificadoras como *Ctrl* y *Alt*. Ejecute esto:

```vimscript
:map <c-d> dd
```

Ahora presionando *Ctrl + d* en su teclado ejecutará `dd`.

### Comentando

¿Recuerda en la primera lección donde hablamos de comentarios?
Es en la asignación de mapas de teclado uno de esos lugares donde no funcionan.

```vimscript
:map <space> viw " Select word
```

Si intentas presionar el espacio ahora, algo horrible casi seguro que va a suceder. ¿Por qué?

Cuando presiona la barra espaciadora, Vim cree que quiere que haga lo que `viw<space>"<space>Select<space>word` dice. Seleccionar la palabra `<space>.` Obviamente, esto no es lo que queremos.


Si observa detenidamente el efecto de este mapeo, podría notar algo extraño. Tómese unos minutos para tratar de averiguar exactamente qué sucede cuando lo usa y por qué sucede eso.

No se preocupe si no se da cuenta de inmediato, lo hablaremos pronto.

### Ejercicios

Asigne la tecla `-` para "eliminar la línea actual y luego pegarla debajo de la línea en la que esta ahora". Esto le permitirá mover líneas hacia abajo en su archivo con una pulsación de tecla.

Agregue ese comando de mapeo a tu archivo `~/.vimrc` para que pueda usarlo cada vez que inicie Vim.

Averigüe cómo asignar la tecla `_` para mover la línea hacia arriba en lugar de hacia abajo.

Agregue esa asignación a su archivo `~/.vimrc` también.

Intente adivinar cómo puede eliminar un mapeo  y restablecer una tecla a su  modo normal.
