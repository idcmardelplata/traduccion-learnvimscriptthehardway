
# Bucles

¡Es posible que se sorprenda al darse cuenta de que hemos revisado treinta y cinco capítulos de un libro de lenguaje de programación sin siquiera mencionar bucles!
Vimscript ofrece muchas otras opciones para realizar acciones en texto (`normal!`, etc.)
que los bucles no son tan necesarios como en la mayoría de los otros lenguajes.

Aun así, definitivamente los necesitará algún día,
así que ahora echaremos un vistazo a los dos tipos principales de bucles que admite Vim.

## Bucles For

El primer tipo de bucle es el bucle for.
Esto puede parecer extraño si está acostumbrado a Java, C o Javascript, pero resulta bastante elegante.
Ejecute los siguientes comandos:

```vimscript
:let c = 0

:for i in [1, 2, 3, 4]
:  let c += i
:endfor

:echom c
```

Vim muestra `10`, que es el resultado de sumar cada elemento en la lista.
El bucle `for` de Vimscript itera sobre listas o diccionarios (que cubriremos más adelante).

No hay equivalente al estilo C para la forma de bucle `(int i = 0; i <foo; i ++)` en Vimscript.
Esto puede parecer malo al principio, pero en la práctica lo agradeceras.

## Bucles While

Vim también es compatible con el clásico bucle `while`.
Ejecute los siguientes comandos:

```vimscript
:let c = 1
:let total = 0

:while c <= 4
:  let total += c
:  let c += 1
:endwhile

:echom total
```

Una vez más, Vim muestra `10`.
Este ciclo debería ser familiar para casi cualquier persona que haya programado antes, por lo que no pasaremos ningún tiempo en ello.
No lo usarás muy a menudo. Pero manténgalo en el fondo de su mente para las raras ocasiones en que lo necesite.

## Ejercicios

* Lea `:help for`
* Lea `:help while`
