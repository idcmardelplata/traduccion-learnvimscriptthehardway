
# Resaltado de sintaxis avanzado

Hasta ahora hemos definido algunos resaltados simples de sintaxis para archivos de Potion:
palabras clave y funciones.

Si no hizo los ejercicios en el último capítulo, debe regresar y hacerlos. Asumiré que los realizo.

De hecho, debería regresar y hacer cualquier ejercicio que haya omitido.
Incluso si cree que no los necesita, debe hacerlo para que este libro sea efectivo.
Por favor, confíe en mí en esto.

### Resaltando comentarios

Una parte obvia de Potion que debemos destacar son los comentarios.
El problema es que los comentarios de Potion comienzan con `#`, que (casi siempre) no está en `iskeyword`.

Si no sabe lo que significa `isKeyword`, no me ha escuchado.
Regrese y haga los malditos ejercicios.
No solo le estoy tirando trabajo inútil cuando escribo los ejercicios para cada capítulo.
Realmente necesita hacerlos para entender el libro.

Debido a que `#` no es un `keyword` debemos usar una expresión regular para que coincida (y el resto del comentario).
Haremos esto con un `syntax match` en lugar de `syntax keyword`.
Agregue las siguientes líneas a su archivo de sintaxis:

```vimscript
syntax match potionComment "\v#.*$"
highlight link potionComment Comment
```

Ya no voy a decirte dónde colocarlos en el archivo. Es un programador: use su juicio.
Cierre y vuelva a abrir `factorial.pn`.
Agregue un comentario en algún lugar del archivo y verá que está resaltado como un comentario.

La segunda línea es simple: le dice a Vim que resalte cualquier cosa en el grupo de sintaxis `potionComment` como `Comment`.

La primera línea es algo nuevo.
Usamos `syntax match` que le dice a Vim que combine expresiones regulares en lugar de `keywords` literales.

Observe que la expresión regular que estamos usando comienza con `\v` que le dice a Vim que use el modo "very magic".
Vuelva a leer el capítulo sobre expresiones regulares básicas si no está seguro de lo que eso significa.

En este caso particular, el modo "very magic" no es necesario.
Pero en el futuro podríamos cambiar esta expresión regular y preguntarnos por qué no funciona,
por lo que recomendaría usar siempre expresiones regulares "very magic" para mantener la consistencia.

En cuanto a la expresión regular en sí, es bastante simple:
los comentarios comienzan con un hash e incluyen todos los caracteres desde allí hasta el final de la línea.

Si necesita un curso actualizado sobre expresiones regulares, debería echar un vistazo a [Learn Regex the Hard Way ](http://regex.learncodethehardway.org/) por Zed Shaw.

### Resaltando operadores

Otra parte de Potion donde necesitamos expresiones regulares para resaltar son los operadores.
Agregue lo siguiente a su archivo de sintaxis:

```vimscript
syntax match potionOperator "\v\*"
syntax match potionOperator "\v/"
syntax match potionOperator "\v\+"
syntax match potionOperator "\v-"
syntax match potionOperator "\v\?"
syntax match potionOperator "\v\*\="
syntax match potionOperator "\v/\="
syntax match potionOperator "\v\+\="
syntax match potionOperator "\v-\="

highlight link potionOperator Operator
```

Cierre y vuelva a abrir `factorial.pn`. Observe que el `*=` en la función `factorial` ahora está resaltado.

Lo primero que probablemente notó acerca de este trozo de código
es que puse cada expresión regular en su propia línea en lugar de agruparlas como lo hice con las palabras clave.
Esto se debe a que `syntax match` no admite múltiples grupos en una sola línea.


También debe tener en cuenta que usé `\v` al comienzo de cada expresión regular,
incluso cuando no era estrictamente necesario.
Prefiero mantener mi sintaxis de expresiones regulares coherente cuando escribo Vimscript,
incluso si eso significa algunos caracteres adicionales.


Quizás se pregunte por qué no utilicé una expresión regular como `"\v-\=?"` para que coincida con ambos `-` y `-=` en una línea.
Absolutamente puede hacer eso si quiere. Funcionará bien.
Solo tiendo a pensar en `-` y `-=` como operadores separados, así que los pongo en líneas separadas.

Definir esos operadores como coincidencias separadas simplifica las expresiones regulares a costa de cierta verbosidad adicional.
Prefiero hacerlo así, pero puedes sentirse diferente. Use su juicio.

También nunca definí `=` como un operador.
Lo haremos en un segundo, pero quería evitarlo por ahora para poder darle una lección.

¡Porque usé expresiones regulares separadas para `-` y `-=` tuve que definir `-=` después de `-`!

Si lo hiciera en el orden opuesto y usara `-=` en un archivo de Potion, Vim coincidiría `-` (y lo resaltaría, por supuesto) y solo `=` permanecería para la coincidencia. Esto muestra que cuando está creando grupos con `syntax match`, cada grupo "consume" partes del archivo que no se pueden comparar más tarde.

Esto es una simplificación excesiva, pero todavía no quiero atascarme en los detalles.
Por ahora, su regla general debe ser hacer coincidir grupos más grandes después de grupos más pequeños más adelante porque los grupos definidos más adelante tienen prioridad sobre los grupos definidos anteriormente.

Avancemos y agreguemos `=` como operador, ahora que hemos tenido nuestra lección:

```vimscript
syntax match potionOperator "\v\="
```

Tómese un segundo y piense dónde necesita poner esto en el archivo de sintaxis.
Vuelva a leer los últimos párrafos si necesita una pista.

### Ejercicios

* Lea `:help syn-match`.

* Lea `:help syn-priority`.

* No hicimos un operador `:` en nuestro ejemplo.
Lea los documentos de Potion y tome una decisión consciente sobre si hacer un operador `:`.
Si decide hacerlo, agréguelo al archivo de sintaxis.

* Haga lo mismo para `.` y `/`.

* Agregue un syntax group `potionNumber` que resalte los números.
Vincúlelo al grupo de resaltado `Number`.
Recuerde que Potion admite números como `2`, `0xffaf`, `123.23`, `1e-2` y `1.9956e+2`.
Recuerde equilibrar el tiempo que lleva manejar casos límite con la cantidad de tiempo que esos casos límite se utilizarán realmente.
