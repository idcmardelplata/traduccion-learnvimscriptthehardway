
# Argumentos de Funciones

Las funciones de Vimscript pueden, por supuesto, tomar argumentos. Ejecute los siguientes comandos:

```vimscript
:function DisplayName(name)
:  echom "Hello!  My name is:"
:  echom a:name
:endfunction
```

Luego ejecute la función:

```vimscript
:call DisplayName("Your Name")
```

Vim mostrará dos líneas: `Hello! My name is:` y `Your Name`.

Observe `a:` en el nombre de la variable que pasamos al comando `echom`. Esto representa un scope de variable, del que hablamos en un capítulo anterior.

Eliminemos este prefijo de scope y veamos cómo reacciona Vim. Ejecute los siguientes comandos:

```vimscript
:function UnscopedDisplayName(name)
:  echom "Hello!  My name is:"
:  echom name
:endfunction
:call UnscopedDisplayName("Your Name")
```

Esta vez Vim se queja de que no puede encontrar el nombre de la variable.

Cuando escriba una función de Vimscript que toma argumentos, siempre debe prefijar esos argumentos con `a:` para poder utilizarlos, esto sirve para decirle a Vim que están en el scope de argumentos.

### varargs

Las funciones de Vimscript pueden opcionalmente tomar listas de argumentos de longitud variable como Javascript y Python. Ejecute los siguientes comandos:

```vimscript
:function Varg(...)
:  echom a:0
:  echom a:1
:  echo a:000
:endfunction

:call Varg("a", "b")
```

Esta función nos muestra varias cosas, así que veamos cada una individualmente.

El `...` en la definición de función le dice a Vim que esta función puede tomar cualquier número de argumentos. Esto es como un argumento `*args`  en una función de Python.

La primera línea de la función hace un `echom` de `a:0` y muestra `2`. Cuando define una función que toma un número variable de argumentos en Vim, `a:0` se configurará según el número de argumentos adicionales que recibió. En este caso, pasamos dos argumentos a *varg*, por lo que Vim mostró `2`.

La segunda línea hace `echom a:1` que muestra `a`. Puede usar `a:1`, `a:2`, etc. para referirse a cada argumento adicional que recibe su función. Si hubiéramos usado `a:2` Vim habría mostrado `b`.

La tercera línea es un poco más complicada. Cuando una función tiene `varargs`, `a:000` se establecerá como una lista que contenga todos los argumentos adicionales que se pasaron. Todavía no hemos visto listas, así que no se preocupes demasiado por esto. No puede usar `echom` con una lista, por eso usamos `echo` en esa línea.

Puede usar `varargs` junto con argumentos comunes también. Ejecute los siguientes comandos:

```vimscript
:function Varg2(foo, ...)
:  echom a:foo
:  echom a:0
:  echom a:1
:  echo a:000
:endfunction

:call Varg2("a", "b", "c")
```

Podemos ver que Vim pone "a" en el argumento nombrado `a:foo`, y el resto se coloca en la lista de `varargs`.

### Asignaciones

Intente ejecutar los siguientes comandos:

```vimscript
:function Assign(foo)
:  let a:foo = "Nope"
:  echom a:foo
:endfunction

:call Assign("test")
```

Vim emitirá un error, porque no puede reasignar variables de argumento. Ahora ejecute estos comandos:

```vimscript
:function AssignGood(foo)
:  let foo_tmp = a:foo
:  let foo_tmp = "Yep"
:  echom foo_tmp
:endfunction

:call AssignGood("test")
```

Esta vez la función funciona, y Vim muestra `Yep`.

### Ejercicios

Lea los dos primeros párrafos de `:help function-argument`.
Lea `:help local-variables`.

