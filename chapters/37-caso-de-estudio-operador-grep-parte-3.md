
# Caso de estudio: Operador Grep tercera parte.

Nuestro nuevo y brillante "operador grep" está funcionando muy bien,
pero parte de la escritura de Vimscript es ser considerado y facilitar la vida de sus usuarios.
Podemos hacer dos cosas más para que nuestro operador funcione bien en el ecosistema de Vim.

## Guardar registros

Al introducir el texto en el registro sin nombre, destruimos todo lo que estaba allí anteriormente.
Además, al usar una selección visual para copiar el texto en el caso de que nuestro operador se aplique con un movimiento,
también destruimos cualquier registro de la selección visual más reciente.


Esto no es muy agradable para nuestros usuarios,
así que evitemos usar una selección visual en ese caso y también guardemos el contenido del registro sin nombre antes de copiar en todos los casos
para que podamos restaurarlo una vez que hayamos terminado. Cambie el código para que se vea así:

```vimscript
nnoremap <leader>g :set operatorfunc=GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call GrepOperator(visualmode())<cr>

function! GrepOperator(type)
    let saved_unnamed_register = @@

    if a:type ==# 'v'
        normal! `<v`>y
    elseif a:type ==# 'char'
        normal! `[y`]
    else
        return
    endif

    silent execute "grep! -R " . shellescape(@@) . " ."
    copen

    let @@ = saved_unnamed_register
endfunction
```

Hemos agregado dos sentencias `let` al comienzo y al final de la función.
El primero guarda el contenido de `@@` en una variable y el segundo lo restaura.
Además, hemos aplicado la copia con un movimiento en lugar de una selección visual en el caso de que nuestro operador se aplique con un movimiento.

## Namespacing

Nuestro script creó una función llamada `GrepOperator` en el espacio de nombres global.
Esto probablemente no sea gran cosa, pero cuando escribe Vimscript es mucho mejor prevenir que curar.


Podemos evitar contaminar el espacio de nombres global cambiando un par de líneas en nuestro código.
Edite el archivo para que se vea así:

```vimscript
nnoremap <leader>g :set operatorfunc=<SID>GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call <SID>GrepOperator(visualmode())<cr>

function! s:GrepOperator(type)
    let saved_unnamed_register = @@

    if a:type ==# 'v'
        normal! `<v`>y
    elseif a:type ==# 'char'
        normal! `[v`]y
    else
        return
    endif

    silent execute "grep! -R " . shellescape(@@) . " ."
    copen

    let @@ = saved_unnamed_register
endfunction
```

Las primeras tres líneas del script han cambiado.
Primero, modificamos el nombre de la función para comenzar con `s:` que lo coloca en el namespace del script actual.

También modificamos los mapeos y anteponemos el nombre de la función `GrepOperator` con `<SID>` para que puedan encontrar la función.
Si no hubiéramos echo esto, habrían intentado encontrar la función en el namespace global, lo que no hubiera funcionado.

¡Felicitaciones, nuestro script `grep-operator.vim` no solo es extremadamente útil,
sino que también es un ciudadano considerado de Vimscript!


## Ejercicios

Lea `:help <SID>`

Disfrute de un refrigerio. ¡Se lo merece!
