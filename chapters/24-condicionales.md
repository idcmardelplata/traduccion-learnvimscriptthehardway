
# Condicionales

Cada lenguaje de programación tiene una forma de bifurcar, y en Vimscript esa forma es la instrucción `if`. La sentencia `if` es el núcleo de la bifurcación en Vim. No hay una sentencia `unless` como en Ruby, por lo que cualquier decisión que tome en tu código se hará con `if`s.

Antes de hablar sobre la sentencia `if` de Vim, necesitamos tomar un breve desvío para hablar sobre la sintaxis para que todos estemos en la misma página.

### Sentencias de múltiples lineas.

A veces no se puede colocar un fragmento de Vimscript en una sola línea de código. Vimos esto cuando hablamos de grupos de autocomandos. Aquí hay un trozo de código que usamos antes:

```vimscript
:augroup testgroup
:    autocmd BufWrite * :echom "Baz"
:augroup END
```

Puede escribir esto como tres líneas separadas en un archivo Vimscript, lo que es ideal, pero se vuelve tedioso escribir de esta manera cuando se ejecutan comandos manualmente. En su lugar, puede separar cada línea con un carácter de canalización (`|`). Ejecute el siguiente comando:

```vimscript
:echom "foo" | echom "bar"
```

Vim tratará eso como dos comandos separados. Utilice `:messages` para verificar el registro si no vio aparecer ambas líneas.

Para el resto de este libro, si desea ejecutar manualmente un comando pero no quiere molestarse en escribir las líneas nuevas y los dos puntos, no dude en ponerlo todo en una línea separada por canalizaciones.

### If básico

Ahora que lo hemos dejado de lado, ejecute los siguientes comandos:

```vimscript
:if 1
:    echom "ONE"
:endif
```

Vim mostrará `*ONE*`, porque el entero 1 es "verdadero". Ahora pruebe estos comandos:

```vimscript
:if 0
:    echom "ZERO"
:endif
```

Vim no mostrará `*CERO*` porque el entero `0` es "false". Veamos cómo se comportan los strings. Ejecute estos comandos:

```vimscript
:if "something"
:    echom "INDEED"
:endif
```

Los resultados pueden sorprenderle. Vim no necesariamente trata una cadena no vacía como "true", ¡así que no mostrará nada!

Vamos a profundizar un poco más. Ejecute estos comandos:

```vimscript
:if "9024"
:    echom "WHAT?!"
:endif
```

Esta vez Vim muestra el texto! WHAT?!

Para intentar entender en nuestras cabezas lo que sucede, ejecute los siguientes tres comandos:

```vimscript
:echom "hello" + 10
:echom "10hello" + 10
:echom "hello10" + 10
```

¡El primer comando hace que Vim haga `echo` de `10`, el segundo comando hace `echo` de `20` y el tercero hace `echo` de 10 de nuevo!

Después de observar todos estos comandos, podemos formarnos algunas conclusiones sobre Vimscript:

* Vim intentará forzar variables (y literales) cuando sea necesario. Cuando se evalúe 10 + "20foo", Vim hara una coerción "20foo" a un entero (lo que da como resultado 20) y luego lo agregará a 10.
* Los Strings que comienzan con un número seran coaccionados a ese número, de lo contrario, seran coaccionados a 0.
* Vim ejecutará el cuerpo de una instrucción `if` cuando su condición se evalúe como un entero que no sea cero, después de que tenga lugar la coacción.

### Else y ElseIf

Vim, como Python, admite las cláusulas `else` y `else if`. Ejecute los siguientes comandos:

```vimscript
:if 0
:    echom "if"
:elseif "nope!"
:    echom "elseif"
:else
:    echom "finally!"
:endif
```

Vim hace `echo "finnally!"` porque ambas condiciones previas se evalúan a cero, lo cual es `false`.

### Ejercicios

Beba una cerveza para consolarse sobre la coacción de strings de Vim a los enteros.
