
 Learn Vimscript the Hard Way (Spanish Translation)
==================================================

Los programadores dan forma a las ideas en texto.
Ese texto se convierte en números y esos números chocan con otros números y hacen que *las cosas sucedan.*

Como programadores, usamos editores de texto para sacar nuestras ideas de la cabeza y crear los trozos de texto que llamamos "programas". Los programadores de tiempo completo pasarán decenas de miles de horas de sus vidas interactuando con su editor de texto, durante el cual harán muchas cosas:

* Obtener texto en bruto desde sus cerebros hacia sus computadoras.
* Corregir errores en ese texto.
* Reestructurar el texto para formular el problema de una manera diferente.
* Documentar cómo y por qué se hizo algo de una manera particular.
* Comunicarse con otros programadores sobre todas estas cosas.

Vim es increíblemente poderoso out of the box, pero no brilla realmente hasta que se toma un tiempo para personalizarlo para su trabajo, para sus hábitos y para sus dedos en particular. Este libro le presentará Vimscript, el lenguaje de programación principal utilizado para personalizar Vim. Podrá moldear Vim para que sea un editor adecuado para sus necesidades personales de edición y hacer que el resto de su tiempo en Vim sea mucho más eficiente.

En el camino, también mencionaré cosas que no son estrictamente acerca de Vimscript, sino más bien sobre aprender y ser más eficientes en general. Vimscript no le va a ayudar mucho si termina jugando con su editor todo el día en lugar de trabajar, por lo que es importante lograr un equilibrio.

El estilo de este libro es un poco diferente de la mayoría de los otros libros sobre lenguajes de programación. En lugar de simplemente presentarle información sobre cómo funciona Vimscript, lo guiare a través de la introducción de comandos para ver qué hacen.

A veces, el libro lo llevará a callejones sin salida antes de explicar la "manera correcta" de resolver un problema. La mayoría de los otros libros no hacen esto, o solo mencionan los problemas pegajosos después de mostrarle la solución. Sin embargo, no es así como ocurren las cosas en el mundo real. A menudo, escribirá un fragmento rápido de Vimscript y se encontrará con una peculiaridad del lenguaje que deberá resolver. Al pasar por este proceso en el libro, en lugar de pasarlo por alto, espero acostumbrarlo a tratar las peculiaridades de Vimscript para que esté listo cuando encuentre sus propios casos. La práctica hace la perfección.

Cada capítulo del libro se centra en un solo tema. Son cortos pero están llenos de información, así que no los hojee solamente. Si realmente quiere sacar el máximo provecho de este libro, debe escribir todos los comandos. Puede que ya sea un programador experimentado que esté acostumbrado a leer código y entenderlo de inmediato. Si es así: no importa. Aprender Vim y Vimscript es una experiencia diferente de aprender un lenguaje de programación normal.

Necesita escribir todos los comandos.
Necesitas hacer todos los ejercicios.

Hay dos razones por las que esto es tan importante. Primero, Vimscript es viejo y tiene muchos rincones polvorientos y pasillos sinuosos. Una opción de configuración puede cambiar cómo funciona todo el lenguaje. Al escribir cada comando en cada lección y al hacer cada ejercicio, descubrirá problemas con su compilación o con su configuración de Vim en los comandos más simples, donde será más fácil diagnosticarlos y corregirlos.

En segundo lugar, Vimscript es Vim. Para guardar un archivo en Vim, se escribe *:write* (o *:w* para abreviar) y se presiona enter. Para guardar un archivo en Vimscript utilice `write`. Muchos de los comandos de Vimscript que aprenderá también se pueden usar en su edición diaria, pero solo son útiles si están en su memoria muscular, lo que simplemente no ocurre con solo leer.

Espero que encuentre este libro útil. No pretende ser una guía completa de Vimscript. Está pensado para que se sienta lo suficientemente cómodo con el lenguaje como para moldear Vim a su gusto, escribir algunos complementos simples para otros usuarios, leer el código de otras personas (con algunos viajes ocasionales a `:help`) y reconocer algunas de las dificultades más comunes.

¡Buena suerte!
