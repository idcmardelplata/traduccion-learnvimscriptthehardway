
# Grupos de Autocomandos

Hace unos pocos capítulos aprendimos sobre autocomandos. Ejecute el siguiente comando:

```vimscript
:autocmd BufWrite * :echom "Writing buffer!"
```

Ahora guarde el búfer actual con `:write` y ejecute `:messages` para ver el registro de mensajes.
Debería ver el mensaje *Writing Buffer!* en la lista de mensajes.

Ahora vuelva a guardar el búfer actual y ejecute `:messages` para ver el registro de mensajes. Debería ver el mensaje *Writing Buffer!*  en la lista dos veces.

Ahora ejecute el mismo autocomando de nuevo exactamente:

```vimscript
:autocmd BufWrite * :echom "Writing buffer!"
```

Guarde el búfer actual una vez más y ejecute `:messages`.
Verá el mensaje *Writing Buffer!* en la lista cuatro veces. ¿Que pasó?

Cuando crea un comando automático como este, Vim no tiene forma de saber si quiere reemplazar uno existente.
En nuestro caso, Vim creó dos autocomandos separados que hacen lo mismo.

### El problema

Ahora que sabe que es posible crear autocomandos duplicados, puedes estar pensando: "¿Y qué? ¡Simplemente no haga eso!"

El problema es que cuando recarga su archivo `~/.vimrc` se vuelve a leer el archivo completo, ¡incluyendo cualquier comando automático que haya definido! Esto significa que cada vez que usted haga `source ~/.vimrc` duplicará las autocomandos, lo que hará que Vim se ejecute más lento porque ejecuta los mismos comandos una y otra vez.

Para simular esto, intente ejecutar el siguiente comando:

```vimscript
:autocmd BufWrite * :sleep 200m
```

Ahora guarde el búfer actual. Puede o no notar una ligera lentitud en el tiempo de escritura de Vim. Ahora ejecute el comando tres veces más:

```vimscript
:autocmd BufWrite * :sleep 200m
:autocmd BufWrite * :sleep 200m
:autocmd BufWrite * :sleep 200m
```

Guarde el archivo de nuevo. Esta vez la lentitud será más aparente.

Obviamente, no tendrá ningún comando automático que solamente duerma, pero el `~/.vimrc` de un usuario experimentado de Vim puede llegar fácilmente a 1,000 líneas, muchos de los cuales serán comandos automáticos. Combine eso con los autocomandos definidos en cualquier plugin instalado y definitivamente puede afectar el rendimiento.

### Agrupando los autocomandos

Vim tiene una solución al problema. El primer paso es agrupar los autocomandos relacionados en grupos nombrados.

Abra una nueva instancia de Vim para borrar los autocomandos anteriores, y luego ejecute los siguientes comandos:

```vimscript
:augroup testgroup
:    autocmd BufWrite * :echom "Foo"
:    autocmd BufWrite * :echom "Bar"
:augroup END
```

La indentacion en las dos líneas del medio es insignificante. No tiene que escribirlo si no quiere.

Guarde un búfer y compruebe `:mensajes.` Debería ver tanto a `Foo` como a `Bar`. Ahora ejecute los siguientes comandos:

```vimscript
:augroup testgroup
:    autocmd BufWrite * :echom "Baz"
:augroup END
```

Intente adivinar qué sucederá cuando vuelva a guardar el búfer. Una vez que tenga una conjetura en mente, guarde el búfer y ejecute `:mensajes` para ver si estaba en lo correcto.

### Limpiando Grupos

¿Qué pasó cuando escribió el archivo? ¿Es ésto lo que esperaba?

Si pensaba que Vim reemplazaría al grupo, puede ver que adivino mal. No se preocupe, la mayoría de la gente piensa lo mismo al principio (sé que lo hice).

Cuando usa `augroup` varias veces, Vim combinará los grupos cada vez.

Si quiere borrar un grupo puede usar `autocmd!` dentro del grupo. Ejecuta los siguientes comandos:

```vimscript
:augroup testgroup
:    autocmd!
:    autocmd BufWrite * :echom "Cats"
:augroup END
```

Ahora intente guardar su archivo y verifique `:messages`. Esta vez Vim solo hizo echo de `Cats` cuando guardo el archivo.

### Usando autocomandos en su archivo vimrc

Ahora que sabemos cómo agrupar autocomandos y borrar esos grupos, podemos usar esto para agregar autocomandos a `~/.vimrc` que no generen un duplicado cada vez que lo obtenemos.

Agregue lo siguiente a su archivo `~/.vimrc`:

```vimscript
augroup filetype_html
    autocmd!
    autocmd FileType html nnoremap <buffer> <localleader>f Vatzf
augroup END
```

Cuando ingresamos al grupo `filetype_html`, lo eliminamos de inmediato, definimos un `autocommand` y dejamos el grupo. Si recargamos  `~/.vimrc`, el borrado evitará que Vim agregue autocomandos duplicados.

### Ejercicios

Revise su archivo `~/.vimrc` y envuelva cada autocomando que tenga en grupos como este.
Puede poner varios autocomandos en el mismo grupo si tiene sentido para usted.

Trate de averiguar lo que hace el mapeo en el último ejemplo.

Lea `:help autocmd-groups`.
