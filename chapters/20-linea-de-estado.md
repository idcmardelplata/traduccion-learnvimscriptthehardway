
# Status Line

Vim le permite personalizar el texto en la línea de estado en la parte inferior de cada ventana. Esto se hace a través de la opción de `statusline`. Ejecute el siguiente comando:

```vimscript
set statusline=%f
```

Debería ver la ruta del archivo (relativa al directorio actual) en la barra de estado. Ahora ejecute este comando:

```vimscript
:set statusline=%f\ -\ FileType:\ %y
```

Ahora verá algo como `foo.markdown - FileType: [markdown]` en la línea de estado.

Si está familiarizado con el `printf de C` o la interpolación de cadenas de Python, el formato de esta opción puede resultar familiar. Si no, el único truco es que las cosas que comienzan con `%` se expanden a un texto diferente dependiendo de lo que venga después de ellas. En nuestro ejemplo, `%f` se reemplaza con el nombre de archivo y `%y` se reemplaza con el tipo de archivo.

Observe cómo los espacios en la línea de estado deben escaparse con barras invertidas. Esto se debe a que set le permite configurar varias opciones a la vez, como vimos en el segundo capítulo.

Las barras de estado pueden complicarse extremadamente rápido, así que hay una mejor manera de establecerlas que nos permitirá ser más claros. Ejecute los siguientes comandos:

```vimscript
:set statusline=%f         " Path to the file
:set statusline+=\ -\      " Separator
:set statusline+=Formato: " Label
:set statusline+=%y        " Filetype of the file
```

En el primer comando usamos `=` para borrar cualquier valor existente. En el resto usamos `+=` para construir la opción una pieza a la vez. También agregamos comentarios que explican cada pieza para otras personas que lean el código (o nosotros mismos varios meses después).

Ejecute los siguientes comandos:

```vimscript
:set statusline=%l    " Current line
:set statusline+=/    " Separator
:set statusline+=%L   " Total lines
```

Ahora, la barra de estado solo contiene el número de línea actual y el número de líneas en el archivo, y se parece a 12/223.

### Width y Padding

Se pueden usar caracteres adicionales en algunos de los diversos códigos `%` para cambiar la forma en que se muestra la información. Ejecute el siguiente comando:

```vimscript
:set statusline=[%4l]
```

El número de línea en la barra de estado ahora estará precedido por espacios suficientes para que tenga al menos cuatro caracteres de ancho (por ejemplo: [12]). Esto puede ser útil para evitar que el texto en la línea de estado se desplace distraídamente.

Por defecto, los margenes se agregan en el lado izquierdo del valor. Ejecute este comando:

```vimscript
:set statusline=Current:\ %4l\ Total:\ %4L
```

Su barra de estado ahora se verá así:

```vimscript
Current:   12 Total:  223
```

Puede usar `-` para colocar el relleno a la derecha en lugar de a la izquierda. Ejecute este comando:

```vimscript
:set statusline=Current:\ %-4l\ Total:\ %-4L
```

Su barra de estado ahora se verá así:

```vimscript
Current: 12   Total: 223
```

Esto se ve mucho mejor porque los números están al lado de sus etiquetas.

Para los códigos que dan como resultado un número, puede decirle a Vim que rellene con ceros en lugar de espacios. Ejecute el siguiente comando:

```vimscript
:set statusline=%04l
```

Ahora su línea de estado leerá `0012` cuando esté en la línea doce.

Finalmente, también puede establecer el ancho máximo de la salida de un código. Ejecute este comando:

```vimscript
:set statusline=%F
```

`%F` muestra la ruta completa al archivo actual. Ahora ejecute este comando para cambiar el ancho máximo:

```vimscript
:set statusline=%.20F
```

El path se truncará si es necesario, mostrando algo como esto:

```vimscript
<hapters/17.markdown
```

Esto puede ser útil para evitar que las rutas y otros códigos largos ocupen toda la línea.

### Formato General

El formato general de un código en una línea de estado se muestra en `:help statusline`:

```vimscript
%-0{minwid}.{maxwid}{item}
```

Todo excepto el `%` y el elemento es opcional.

### Dividiendo

No vamos a cubrir las barras de estado con demasiados detalles aquí (la propia documentación de Vim sobre ellas es muy extensa si desea obtener más información), pero hay un código más simple que puede ser muy útil de inmediato. Ejecute los siguientes comandos:

```vimscript
:set statusline=%f         " Path to the file
:set statusline+=%=        " Switch to the right side
:set statusline+=%l        " Current line
:set statusline+=/         " Separator
:set statusline+=%L        " Total lines
```

Ahora la barra de estado contendrá la ruta al archivo en el lado izquierdo y las líneas actuales / totales en el lado derecho. El código `%=` le dice a Vim que todo lo que venga después debe estar alineado (en conjunto) a la derecha en lugar de a la izquierda.

### Ejercicios

Hojee la lista de códigos disponibles en `:help statusline`. No se preocupe si aún no entiende algunos de ellos.

Agregue algunas líneas a su archivo `~/.vimrc` para crear una barra de estado personalizada. Asegúrese de usar la forma `+=` de `set` para dividir la definición en varias líneas, y agregue un comentario en cada línea para documentar lo que hace cada pieza.

Intente usar `autocommands` y `setlocal` para definir diferentes líneas de estado para diferentes tipos de archivos. Asegúrese de envolver los `autocommands` en grupos para evitar la duplicación (como siempre).
