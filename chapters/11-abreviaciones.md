
# Abreviaciones

Vim tiene una característica llamada "abreviaciones" que se siente similar a los mapeos pero que están diseñadas para ser utilizadas en los modos de insertar, reemplazar y comando. Son extremadamente flexibles y poderosas, pero aquí vamos a cubrir los usos más comunes.

Solo nos preocuparemos por las abreviaturas de modo inserción en este libro. Ejecuta el siguiente comando:

```vimscript
:iabbrev adn and
```

Ahora ingrese el modo de inserción y escriba:

```vimscript
One adn two.
```

Tan pronto como llegue al espacio después de escribir `adn`, Vim lo reemplazará con `and`.

La corrección de errores tipográficos como este es un gran uso para las abreviaciones.

Ejecute estos comandos:

```vimscript
:iabbrev waht what
:iabbrev tehn then
```

Ahora ingrese nuevamente al modo de inserción y escriba:

```vimscript
Well, I don't know waht we should do tehn.
```

Observe cómo se sustituyeron ambas abreviaciones, aunque no haya escrito un espacio después del segundo.

### Caracteres claves.

Vim sustituirá una abreviación cuando escriba cualquier "*carácter que no sea de palabra clave*" después de una abreviación. "Carácter que no es de palabra clave" significa cualquier carácter que no esté en la opción `iskeyword`.

Ejecute este comando:

```vimscript
:set iskeyword?
```

Debería ver algo como `iskeyword = @, 48-57, _, 192-255`. Este formato es muy complicado, pero en esencia significa que todos los siguientes caracteres se consideran "caracteres de palabras clave":

    . Todos los caracteres alfabéticos ASCII, mayúsculas y minúsculas, y sus versiones acentuadas.
    . Cualquier carácter con un valor ASCII entre 48 y 57 (los dígitos del cero al nueve).
    . El carácter de subrayado (_).
    . Cualquier carácter con un valor ASCII entre 192 y 255 (algunos caracteres especiales ASCII).

Si desea leer la descripción completa del formato de esta opción, puede consultar `:help isfname`, pero le advertiré que es mejor que tenga una cerveza lista para esto.

Para nuestros propósitos, simplemente puede recordar que las abreviaciones se expandirán cuando escriba algo que no sea una letra, un número o un guión bajo.

### Más Abreviaciones

Las abreviaciones son útiles para algo más que para corregir errores tipográficos. Agreguemos algunas más que puedan ayudar en la edición diaria de texto.

Ejecute los siguientes comandos:

```vimscript
:iabbrev @@    steve@stevelosh.com
:iabbrev ccopy Copyright 2013 Steve Losh, all rights reserved.
```

Siéntase libre de reemplazar mi nombre y dirección de correo electrónico con los suyos, luego ingrese al modo  inserción y pruébelos.

Estas abreviaciones toman grandes trozos de texto que suele escribir con frecuencia y los comprime hasta unos pocos caracteres. Con el tiempo, esto puede ahorrarle mucha escritura, así como el desgaste en los dedos.

### ¿Por qué no utilizar mapeos?

Si está pensando que las abreviaciones parecen similares a los mapeos, tiene razón.
Sin embargo, están destinados a ser utilizados para diferentes cosas. Veamos un ejemplo.

Ejecute este comando:

```vimscript
:inoremap ssig -- <cr>Steve Losh<cr>steve@stevelosh.com
```

Esta es una asignación que le permite insertar su firma rápidamente. Pruébelo introduciendo el modo de inserción y escribiendo `ssig`.

Parece funcionar muy bien, pero hay un problema. Intente ingresar al modo de inserción y escribir este texto:

```vimscript
Larry Lessig wrote the book "Remix".
```

¡Notará que Vim ha expandido el nombre! Los mapeos no tienen en cuenta qué caracteres vienen antes o después del mapa, solo observan la secuencia específica que ha sido mapeada.

Elimine el mapeo y sustitúyalo por una abreviación ejecutando los siguientes comandos:

```vimscript
:iunmap ssig
:iabbrev ssig -- <cr>Steve Losh<cr>steve@stevelosh.com
```

Ahora pruebe la abreviación otra vez.

Esta vez, Vim prestará atención a los caracteres antes y después de `ssig` y solo los expandirá cuando lo deseemos.

### Ejercicios

Agregue abreviaciones para algunos errores tipográficos comunes que sabe que hace y agréguelos a su archivo `~/.vimrc.` ¡Asegúrese de utilizar los mapeos que creó en el último capítulo para abrir y obtener el archivo!

Agregue abreviaciones para su propia dirección de correo electrónico, sitio web y firma también.

Piense en algunos fragmentos de texto que escriba muy a menudo y agregue abreviaciones para ellos también.
