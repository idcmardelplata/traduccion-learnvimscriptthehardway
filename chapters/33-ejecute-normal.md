
# execute normal!

Ahora que hemos visto `execute` y `normal!` Podemos hablar del lenguaje Vimscript con más detalle. Ejecute el siguiente comando:


```vimscript
:execute "normal! gg/foo\<cr>dd"
```

Esto se moverá a la parte superior del archivo, buscará la primera aparición de `foo` y eliminará la línea que lo contiene.

Anteriormente tratamos de usar `normal!` con un comando de búsqueda pero no pudo ingresar el `return` necesario para realizar la búsqueda.
Combinando `normal!` con `execute` arreglamos ese problema.

`execute` le permite crear comandos mediante programación, para que pueda usar las secuencias de escape normales de Vim para generar los caracteres no imprimibles.
Pruebe el siguiente comando:

```vimscript
:execute "normal! mqA;\<esc>`q"
```

¿Qué hace esto? Vamos a separarlo:


* `:execute "normal! ..."` ejecuta la secuencia de comandos como si se hubieran ingresado en modo normal,
ignorando todas los mapeos y reemplazando las secuencias de escape del string con sus resultados.
* `mq`: almacena la ubicación actual en la marca `"q"`.
* `A`: va al final de la línea actual e ingresa al modo de inserción después del último carácter.
* `;` ahora estamos en modo de inserción, así que simplemente ponga un punto y coma literal en el archivo.
* `\<esc>`: esta es una secuencia de escape de string que se resuelve al presionar la tecla de escape, lo que nos saca del modo de inserción.
* `\q`: volver a la ubicación exacta de la marca `q"`.


Parece un poco aterrador, pero en realidad es bastante útil: pone un punto y coma al final de la línea actual mientras deja el cursor en su lugar.
Un mapeo para esto podría ser útil si olvida un punto y coma al escribir Javascript, C o cualquier otro lenguaje con punto y coma como terminadores de instrucciones.

## Ejercicios

Lea `:help expr-quote` de nuevo (lo ha visto antes)
para recordarle cómo usar strings de escape para pasar caracteres especiales al modo `normal!` con `execute`.

Deje este libro por un tiempo antes de continuar con el siguiente capítulo.
Consiga un sándwich o una taza de café, alimente a sus mascotas si las tiene y relájese un poco antes de continuar.
