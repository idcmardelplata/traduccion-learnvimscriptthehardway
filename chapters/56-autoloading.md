
# Autoloading

Hemos escrito una buena cantidad de funcionalidad para nuestro plugin de Potion, y eso es todo lo que haremos en este libro.
Antes de terminar, hablaremos sobre algunas formas más importantes de pulirlo y realmente hacer que brille.

Lo primero en la lista es hacer que nuestro plugin sea más eficiente con la carga automática.

### ¿Como funciona autoload?

Actualmente, cuando un usuario carga nuestro plugin (al abrir un archivo de Potion), se carga toda su funcionalidad.
Nuestro plugin todavía es pequeño, por lo que probablemente no sea un gran problema, pero para plugins más grandes, la carga de todo su código puede llevar una cantidad de tiempo notable.

La solución de Vim a esto es algo llamado "autoload". Autoload le permite retrasar la carga del código hasta que sea realmente necesario.
En general, tendrá un ligero impacto en el rendimiento, pero si sus usuarios no siempre usan cada bit de código en su plugin, la carga automática puede ser una gran mejora.

Así es como funciona. Mire el siguiente comando:


```vimscript
:call somefile#Hello()
```

Cuando ejecuta este comando, Vim se comportará un poco diferente a una llamada de función normal.

Si esta función ya se ha cargado, Vim simplemente la llamará normalmente.

De lo contrario, Vim buscará un archivo llamado `autoload/somefile.vim` en su directorio `~/.vim` (y cualquier paquete de `Pathogen`).

Si este archivo existe, Vim cargará y recargara el archivo. Luego intentará llamar a la función normalmente.

Dentro de este archivo, la función debe definirse así:

```vimscript
function somefile#Hello()
    " ...
endfunction
```

Puede usar varios `#` caracteres en el nombre de la función para representar subdirectorios. Por ejemplo:

```vimscript
:call myplugin#somefile#Hello()
```

Esto buscará el archivo cargado automáticamente en `autoload/myplugin/somefile.vim`.
La función dentro de ella debe definirse con la ruta completa de autoload:

```vimscript
function myplugin#somefile#Hello()
    " ...
endfunction
```

### Experimentando

Para tener una idea de cómo funciona esto, vamos a intentarlo.
Cree un archivo `~/.vim/autoload/example.vim` y agregue lo siguiente:

```vimscript
echom "Loading..."

function! example#Hello()
    echom "Hello, world!"
endfunction

echom "Done loading."
```

Guarde el archivo y ejecute `:call example#Hello()`. Vim generará lo siguiente:

```vimscript
Loading...
Done loading.
Hello, world!
```

Esta pequeña demostración demuestra algunas cosas:

1. Vim realmente carga el archivo `example.vim` sobre la marcha. ¡Ni siquiera existía cuando abrimos Vim, por lo que no podría haberse cargado al inicio!
2. Cuando Vim encuentra el archivo que necesita cargar automáticamente, carga todo el archivo antes de llamar a la función.

**Sin cerrar Vim**, cambie la definición de la función para que se vea así:

```vimscript
echom "Loading..."

function! example#Hello()
    echom "Hello AGAIN, world!"
endfunction

echom "Done loading."
```

Guarde el archivo y **sin cerrar Vim** ejecute `:call example#Hello()` Vim simplemente generará:

```vimscript
Hello, world!
```

Vim ya tiene una definición de  `example#Hello`, por lo que no necesita volver a cargar el archivo, lo que significa:

1. El código fuera de la función no se volvió a ejecutar.
2. No recogió los cambios en la función.

Ahora ejecute `:call example#BadFunction()`. Verá los mensajes de carga nuevamente, así como un error sobre una función inexistente.
Pero ahora intente ejecutar `:call example#Hello()` nuevamente. ¡Esta vez verá el mensaje actualizado!


A estas alturas ya debería tener un control bastante claro de lo que sucede cuando Vim encuentra una llamada a una función con un nombre de estilo de autoload:

1. Comprueba si tiene una función con ese nombre definido. Si es así, solo la llama.
2. De lo contrario, busca el archivo apropiado (basado en el nombre) y lo carga.
3. Luego intente llamar a la función. Si funciona, genial. Si falla, simplemente imprime un error.

Si todavía no lo tiene completamente claro, regrese y trabaje en esta demostración nuevamente e intente ver dónde surte efecto cada regla.

### Que cosas cargar automáticamente?

La carga automática no es gratuita.
Hay algunos gastos generales (pequeños) relacionados con la configuración, sin mencionar los nombres de funciones feos que necesita para su código.

Dicho esto, si está creando un plugin que no se usará cada vez que un usuario abra una sesión de Vim, probablemente sea una buena idea mover tanta funcionalidad a los archivos con carga automática como sea posible.
Esto reducirá el impacto que su plugin tiene en los tiempos de inicio de sus usuarios, lo cual es importante ya que las personas instalan más y más plugins de Vim.

Entonces, ¿qué tipo de cosas se pueden cargar automáticamente de forma segura? La respuesta es básicamente cualquier cosa que sus usuarios no llamen directamente.
Los mapeos y los comandos personalizados no pueden cargarse automáticamente (porque no estarían disponibles para que los usuarios los llamen), pero muchas otras cosas pueden estarlo.

Miremos nuestro plugin de Potion y veamos qué podemos cargar automáticamente.

### Agregar autoloading al plugin de Potion

Comenzaremos con la funcionalidad de compilación y ejecución.
Recuerde que nuestro archivo `ftplugin/potion/running.vim` se veía así al final del capítulo anterior:

```vimscript
if !exists("g:potion_command")
    let g:potion_command = "potion"
endif

function! PotionCompileAndRunFile()
    silent !clear
    execute "!" . g:potion_command . " " . bufname("%")
endfunction

function! PotionShowBytecode()
    " Get the bytecode.
    let bytecode = system(g:potion_command . " -c -V " . bufname("%"))

    " Open a new split and set it up.
    vsplit __Potion_Bytecode__
    normal! ggdG
    setlocal filetype=potionbytecode
    setlocal buftype=nofile

    " Insert the bytecode.
    call append(0, split(bytecode, '\v\n'))
endfunction

nnoremap <buffer> <localleader>r :call PotionCompileAndRunFile()<cr>
nnoremap <buffer> <localleader>b :call PotionShowBytecode()<cr>
```

Este archivo solo se llama cuando se carga un archivo de Potion, por lo que no se agrega a la sobrecarga del inicio de Vim en general.
Pero puede haber algunos usuarios que simplemente no necesitan esta funcionalidad, por lo que si podemos cargar automáticamente algunos de ellos, les ahorraremos unos pocos milisegundos cada vez que abran un archivo de Potion.

Sí, en este caso los ahorros no serán enormes.
Pero estoy seguro de que puede imaginar un plugin con muchos miles de líneas de funciones donde el tiempo requerido para cargarlas sería más significativo.

Empecemos. Cree un archivo `autoload/potion/running.vim` en su repositorio del plugin. Luego mueva las dos funciones y ajuste sus nombres, para que se vean así:

```vimscript
echom "Autoloading..."

function! potion#running#PotionCompileAndRunFile()
    silent !clear
    execute "!" . g:potion_command . " " . bufname("%")
endfunction

function! potion#running#PotionShowBytecode()
    " Get the bytecode.
    let bytecode = system(g:potion_command . " -c -V " . bufname("%"))

    " Open a new split and set it up.
    vsplit __Potion_Bytecode__
    normal! ggdG
    setlocal filetype=potionbytecode
    setlocal buftype=nofile

    " Insert the bytecode.
    call append(0, split(bytecode, '\v\n'))
endfunction
```

Observe cómo la porción de  `potion#running` de los nombres de las funciones coincide con el directorio y el nombre del archivo donde estan alojados.
Ahora cambie el archivo `ftplugin/potion/running.vim` para que se vea así:

```vimscript
if !exists("g:potion_command")
    let g:potion_command = "potion"
endif

nnoremap <buffer> <localleader>r
            \ :call potion#running#PotionCompileAndRunFile()<cr>

nnoremap <buffer> <localleader>b
            \ :call potion#running#PotionShowBytecode()<cr>
```

Guarde los archivos, cierre Vim y abra su archivo `factorial.pn`.
Intente usar los mapeos para asegurarse de que todavía funcionan correctamente.

Asegúrese de ver el mensaje `autoloading...` de diagnóstico solo la primera vez que ejecute uno de los mapeos (es posible que necesite usar `:messages` para verlo).
Una vez que confirme que la carga automática funciona correctamente, puede eliminar ese mensaje.

Como puede ver, hemos dejado las llamadas `nnoremap` que mapean las teclas.
¡No podemos cargarlos automáticamente porque el usuario no tendría forma de iniciar la carga automática si lo hiciéramos!

Este es un patrón común que verá en los plugins de Vim:
la mayor parte de su funcionalidad se mantendrá en funciones cargadas automáticamente, con solo `nnoremap` y comandos `command` en los archivos que Vim carga cada vez.
Tengalo en cuenta siempre que escriba un plugin no trivial de Vim.

### Ejercicios

* Lea: `:help autoload`.

* Experimente un poco y descubra cómo se comportan las variables de autoload.

* Suponga que desea forzar programáticamente una recarga de un archivo de autoload que Vim ya ha cargado, sin molestar al usuario.
¿Cómo puede hacer esto? Es posible que desee leer `:help :silent`. Por favor, nunca haga esto en la vida real.
