
# Resaltado de sintaxis básico


Ahora que hemos sacado el boilerplate del camino, es hora de comenzar a escribir un código útil para nuestro plugin de Potion.
Comenzaremos con un simple resaltado de sintaxis.

Cree un archivo en `syntax/potion.vim` en el repositorio de su plugin.
Ponga el siguiente código en el archivo:

```vimscript
if exists("b:current_syntax")
    finish
endif

echom "Our syntax highlighting code will go here."

let b:current_syntax = "potion"
```

Cierre Vim y luego abra su archivo `factorial.pn`.
Puede o no ver el mensaje, dependiendo de si tiene algún otro plugin que ejecute comandos después de que se ejecute este.
Si ejecuta `:messages`, definitivamente verá que el archivo se cargó.

**Nota**:
> Cada vez que le pido que abra el archivo de Potion, quiero que lo haga en una nueva **ventana/instancia** de Vim en lugar de en una **división/pestaña**. Abrir una nueva ventana de Vim hace que Vim recargue todos los archivos del `bundle` para esa ventana, mientras que el uso de una división no lo hace.

Las líneas al principio y al final del archivo son una convención que evita que se cargue si el resaltado de sintaxis ya se ha habilitado antes para este búfer.

### Resaltado de palabras claves

Para el resto de este capítulo, ignoraremos el `if` y el `let` y dejaremos el boilerplate al principio y al final del archivo.
No elimine esas líneas, solo olvídese de ellas.

Reemplace el placeholder `echom` en el archivo con el siguiente código:

```vimscript
syntax keyword potionKeyword to times
highlight link potionKeyword Keyword
```

Cierre el archivo `factorial.pn` y vuelva a abrirlo.
¡Las palabras `to` y `times` se resaltarán como palabras clave en su esquema de colores!

Estas dos líneas muestran la estructura básica del resaltado de sintaxis simple en Vim.
Para resaltar una sintaxis:

* Primero define un "pedazo de sintaxis" utilizando la keyword `syntax` o un comando relacionado (del que hablaremos más adelante).

* Luego vincule "fragmentos" a grupos de resaltado.
  Un grupo de resaltado es algo que se define en un esquema de color, por ejemplo, "los nombres de las funciones deben ser azules".


Esto permite a los autores de plugins definir los "fragmentos" de sintaxis de manera que tengan sentido para ellos,
y luego vincularlos a grupos de resaltado comunes.
También permite a los creadores de esquemas de color definir colores para un conjunto común de construcciones de programación para que no necesiten saber sobre lenguajes individuales.

Potion tiene muchas otras palabras clave que no hemos usado en nuestro programa de juguete,
así que editemos nuestro archivo de sintaxis para resaltarlas también:

```vimscript
syntax keyword potionKeyword loop times to while
syntax keyword potionKeyword if elsif else
syntax keyword potionKeyword class return

highlight link potionKeyword Keyword
```

Primero que nada: la última línea no ha cambiado.
Todavía le estamos diciendo a Vim que cualquier cosa en el grupo de sintaxis `potionKeyword` debería ser resaltado como una `keyword`.

Ahora tenemos tres líneas, cada una de las cuales comienza con `syntax keyword potionKeyword`.
Esto muestra que ejecutar este comando varias veces no restablece el grupo de sintaxis, ¡se agrega! Esto le permite definir grupos poco a poco.

La forma en que define sus grupos depende de usted:

* Puede simplemente tirar todo en una línea y terminar con eso.
* Es posible que prefiera dividir las líneas para que quepan en 80 columnas para que sean más fáciles de leer.
* Podría tener una línea separada para cada elemento en un grupo, para que las diferencias se vean mejor.
* Podría hacer lo que he hecho aquí y agrupar elementos relacionados.

### Resaltando funciones

Otro grupo de resaltado estándar de Vim es `Function`.
Agreguemos algunas de las funciones de Potion incorporadas a nuestro script de resaltado.
Edite las tripas de su archivo de sintaxis para que se vea así:

```vimscript
syntax keyword potionKeyword loop times to while
syntax keyword potionKeyword if elsif else
syntax keyword potionKeyword class return

syntax keyword potionFunction print join string

highlight link potionKeyword Keyword
highlight link potionFunction Function
```

Cierre y vuelva a abrir `factorial.pn` y verá que las funciones built-in de potion ahora están resaltadas.

Esto funciona exactamente de la misma manera que el resaltado de `keyword`.
Hemos definido un nuevo grupo de sintaxis y lo hemos vinculado a un grupo de resaltado diferente.

### Ejercicios

Piense por qué existe el `if` y deje que las líneas al principio y al final del archivo sean útiles.
Si no puede resolverlo, no se preocupe. Yo tuve que preguntarle a Tim Pope para estar seguro.


* Lea `:help syn-keyword` y preste mucha atención a la parte donde hace mención a `isKeyword`

* Lea: `:help isKeyword`.

* Lea `:help grup-name` para tener una idea de algunos grupos de resaltado comunes que los autores de esquemas de color usan con frecuencia.
