
# Folding avanzado

En el último capítulo utilizamos el folding `indent` de Vim para agregar un plegado rápido y sucio a los archivos de Potion.

Abra `factorial.pn` y asegúrese de que todos los pliegues estén cerrados con `zM`. El archivo ahora debería verse así:

```vimscript
factorial = (n):
+--  5 lines: total = 1

10 times (i):
+--  4 lines: i string print
```

Cambie el primer pliegue y se verá así:

```vimscript
factorial = (n):
    total = 1
    n to 1 (i):
+---  2 lines: # Multiply the running total.
    total.

10 times (i):
+--  4 lines: i string print
```

Esto es bastante bueno, pero personalmente prefiero plegar la primera línea de un bloque con su contenido.
En este capítulo escribiremos un código de `folding` personalizado, y cuando terminemos, nuestros pliegues se verán así:

```vimscript
factorial = (n):
    total = 1
+---  3 lines: n to 1 (i):
    total.

+--  5 lines: 10 times (i):
```

Esto es más compacto y (para mí) más fácil de leer.
Si prefiere el método `indent`, está bien, pero haga este capítulo de todos modos solo para tener algo de práctica escribiendo expresiones de `folding` en Vim.

### teoria del Folding

Escribir un código de folding personalizado, ayuda tener una idea de cómo Vim "piensa" en plegar.
Aquí están las reglas en pocas palabras:

* Cada línea de código en un archivo tiene un "nivel de plegado".
  Esto siempre es cero o un entero positivo.

* Las líneas con un pliegue de cero nunca se incluyen en ningún pliegue.

* Las líneas adyacentes con el mismo nivel de plegado se pliegan juntas.

* Si se cierra un pliegue de nivel X,
  las líneas posteriores con un nivel de pliegue mayor o igual a X se pliegan junto con él hasta llegar a una línea con un nivel inferior a X.

Es más fácil tener una idea de esto con un ejemplo. Abra una ventana de Vim y pegue el siguiente texto en ella.

```vimscript
a
    b
    c
        d
        e
    f
g
```


Active el plegado `ìndent` ejecutando el siguiente comando:

```vimscript
:setlocal foldmethod=indent
```

Juegue con los pliegues durante un minuto para ver cómo se comportan.

Ahora ejecute el siguiente comando para ver el nivel de plegado de la línea 1:

```vimscript
:echom foldlevel(1)
```

Vim mostrará `0`. Ahora busquemos el nivel de plegado de la línea 2:

```vimscript
:echom foldlevel(2)
```

Vim mostrará `1`. Probemos la línea 3:

```vimscript
:echom foldlevel(3)
```

Una vez más, Vim muestra `1`.
Esto significa que las líneas 2 y 3 son parte de un pliegue de nivel `1`. Aquí están los niveles de plegado para cada línea:

```vimscript
a           0
    b       1
    c       1
        d   2
        e   2
    f       1
g           0
```

Vuelva a leer las reglas al comienzo de esta sección.
Abra y cierre cada pliegue en este archivo, observe los niveles de pliegue y asegúrese de comprender por qué los pliegues se comportan como lo hacen.

Una vez que esté seguro de comprender cómo funciona el nivel de plegado de cada línea para crear la estructura de folding, pase a la siguiente sección.

### Primero: Planifique

Antes de sumergirnos en escribir código, tratemos de esbozar algunas "reglas" aproximadas para nuestro plegado.

Primero, las líneas indentadas deben plegarse juntas.
También queremos que la línea anterior se pliegue con ellos, para que algo como esto:

```vimscript
hello = (name):
    'Hello, ' print
    name print.
```

Se pliegue asi:

```vimscript
+--  3 lines: hello = (name):
```

Las líneas en blanco deben estar al mismo nivel que las líneas posteriores,
por lo que no se incluirán las líneas en blanco al final de un pliegue.
Esto significa que esto:

```vimscript
hello = (name):
    'Hello, ' print
    name print.

hello('Steve')
```

Se plegara asi:

```vimscript
+--  3 lines: hello = ():

hello('Steve')
```
y no de esta manera:

```vimscript
+--  4 lines: hello = ():
hello('Steve')
```

Estas reglas son una cuestión de preferencia personal, pero por ahora esta es la forma en que implementaremos el plegado.

### Comenzando

Comencemos con nuestro código de plegado personalizado abriendo Vim con dos divisiones.
Uno debe contener nuestro archivo `ftplugin/potion/folding.vim,` y el otro debe contener nuestra función de muestra `factorial.pn`.

En el capítulo anterior, cerramos y volvimos a abrir Vim para que nuestros cambios en `fold.vim` surtan efecto,
pero resulta que hay una manera más fácil de hacerlo.

Recuerde que cualquier archivo dentro de `ftplugin/potion/` se ejecutará siempre que el tipo de archivo de un búfer esté configurado en Potion.
Esto significa que simplemente puede ejecutar `:set ft=potion` en la división que contiene `factorial.pn` y Vim volverá a cargar el código de plegado.

Esto es mucho más rápido que cerrar y volver a abrir el archivo cada vez.
Lo único que debe recordar es que debe guardar `fold.vim` en el disco; de lo contrario, los cambios no guardados no se tendrán en cuenta.

### Folding Expr

Vamos a utilizar el plegado `expr` de Vim para darnos una flexibilidad ilimitada en la forma en que se pliega nuestro código.

Podemos continuar y eliminar el `foldignore` de `fold.vim` porque solo es relevante cuando se usa el plegado de `indent`.
También queremos decirle a Vim que use el plegado `expr`, así que cambie el contenido de `fold.vim` para que se vea así:

```vimscript
setlocal foldmethod=expr
setlocal foldexpr=GetPotionFold(v:lnum)

function! GetPotionFold(lnum)
    return '0'
endfunction
```

La primera línea simplemente le dice a Vim que use el plegado `expr`.

La segunda línea define la expresión que Vim debería usar para obtener el nivel de plegado de una línea.
Cuando Vim ejecuta la expresión, establecerá `v:lnum` en el número de línea de la línea que desea conocer.
Nuestra expresión llamará a una función personalizada con este número como argumento.

Finalmente definimos una función ficticia que simplemente devuelve `'0'` para cada línea.
Tenga en cuenta que está devolviendo un `string` y no un integer. Veremos por qué en breve.

Continúe y recargue el código de folding guardando `fold.vim` y ejecute `:set ft=potion` en `factorial.pn`.
Nuestra función devuelve '0' para cada línea, por lo que Vim no doblará nada en absoluto.

### Lineas en blanco

Primero veamos el caso especial de las líneas en blanco. Modifique la función `GetPotionFold` para que se vea así:

```vimscript
function! GetPotionFold(lnum)
    if getline(a:lnum) =~? '\v^\s*$'
        return '-1'
    endif

    return '0'
endfunction
```

Hemos agregado una declaración `if` para cuidar las líneas en blanco. ¿Como funciona?

Primero usamos `getline(a:lnum)` para obtener el contenido de la línea actual como un `string`.

Comparamos esto con la expresión regular `\v^\s*$`. Recuerde que `\v` activa el modo "very magic" ("el modo sano").
Esta expresión regular coincidirá con "inicio de línea, cualquier número de caracteres de espacio en blanco, final de línea".

La comparación está utilizando el operador de coincidencia entre mayúsculas y minúsculas `=~?`.
Técnicamente no tenemos que preocuparnos por el caso, ya que solo estamos haciendo coincidir los espacios en blanco,
pero prefiero ser más explícito al usar operadores de comparación en Strings. Puede usar `=~` en su lugar si lo prefiere.

Si necesita una refrescada sobre el uso de expresiones regulares en Vim,
debe volver y releer el capítulo "Expresiones regulares básicas" y los capítulos del "Operador Grep".

Si la línea actual tiene algunos caracteres que no son espacios en blanco, no coincidirá y simplemente devolveremos '0' como antes.

Si la línea actual coincide con la expresión regular (es decir, si está vacía o solo espacio en blanco), devolvemos la cadena '-1'.

Anteriormente dije que el nivel de plegado de una línea puede ser cero o un entero positivo, entonces, ¿qué está pasando aquí?

### Foldlevels especiales.

Su expresión de folding personalizada puede devolver un nivel de plegado directamente, o devolver una de las pocas cadenas "especiales" que le indican a Vim cómo plegar la línea sin especificar directamente su nivel.


`'-1'` es una de estas cadenas especiales.
Le dice a Vim que el nivel de esta línea es "indefinido".
Vim interpretará esto como "el nivel de plegado de esta línea es igual al nivel de plegado de la línea encima o debajo de ella, lo que sea menor".

Esto no es exactamente lo que nuestro plan requería, pero veremos que está lo suficientemente cerca y hará lo que queramos.

Vim puede "encadenar" estas líneas indefinidas, por lo que si tiene dos seguidas seguidas de una línea en el nivel `1`, establecerá la última línea indefinida en `1`, luego la penúltima en `1` y luego la primera en `1`.

Al escribir un código de plegado personalizado, a menudo encontrará algunos tipos de línea para los que puede establecer fácilmente un nivel específico.
Luego usará `'-1'` (y algunos otros niveles de plegado especiales que veremos pronto)
para "conectar en cascada" los niveles de plegado adecuados al resto del archivo.

Si vuelve a cargar el código de plegado para `factorial.pn`, Vim todavía no plegara ninguna línea.
Esto se debe a que todas las líneas tienen un nivel de plegado cero o "indefinido".
El nivel 0 "en cascada" a través de las líneas indefinidas y, finalmente, todas las líneas tendrán su nivel de plegado establecido en 0.

### Un ayudante de nivel de indentacion.

Para abordar las líneas que no están en blanco, necesitaremos conocer su nivel de indentacion,
así que creemos una pequeña función auxiliar para calcularla por nosotros. Agregue la siguiente función sobre `GetPotionFold`:

```vimscript
function! IndentLevel(lnum)
    return indent(a:lnum) / &shiftwidth
endfunction
```

Recargue el código de folding.
Pruebe su función ejecutando el siguiente comando en el búfer `factorial.pn`:

```vimscript
:echom IndentLevel(1)
```

Vim muestra 0 porque la línea 1 no está indentada. Ahora inténtalo en la línea 2:

```vimscript
:echom IndentLevel(2)
```

Esta vez, Vim muestra 1.
La línea dos tiene 4 espacios al principio, y `shiftwidth` se establece en 4, por lo que 4 dividido entre 4 es 1.

IndentLevel es bastante sencillo.
La indentacion `(a:lnum)` devuelve el número de espacios al comienzo del número de línea dado.
Dividimos eso por el `shiftwidth` del búfer para obtener el nivel de indentacion.

¿Por qué usamos `&shiftwidth` en lugar de simplemente dividir por 4?
Si alguien prefiere una sangría de dos espacios en sus archivos de Potion, dividir por 4 produciría un resultado incorrecto.
Usamos la configuración de `shiftwidth` para permitir cualquier cantidad de espacios por nivel.

### Un ayudante mas.

Puede que no sea obvio a dónde ir desde aquí.
Detengámonos y pensemos qué tipo de información necesitamos tener para descubrir cómo plegar una línea no en blanco.

Necesitamos saber el nivel de indentacion de la línea misma.
Tenemos eso cubierto con la función `IndentLevel`, así que ya estamos listos.

También necesitaremos conocer el nivel de indentacion de la siguiente línea que no esté en blanco,
porque queremos plegar las líneas de "encabezado" con sus cuerpos indentados.

Escribamos una función auxiliar para obtener el número de la siguiente línea no en blanco después de una línea dada.
Agregue la siguiente función sobre `IndentLevel`:

```vimscript
function! NextNonBlankLine(lnum)
    let numlines = line('$')
    let current = a:lnum + 1

    while current <= numlines
        if getline(current) =~? '\v\S'
            return current
        endif

        let current += 1
    endwhile

    return -2
endfunction
```

Esta función es un poco más larga, pero es bastante simple. Tomémoslo pieza por pieza.

Primero almacenamos el número total de líneas en el archivo con `line('$')`.
Consulte la documentación de `line()` para ver cómo funciona.

A continuación, establecemos la variable `current` en el número de la siguiente línea.

Luego comenzamos un bucle que recorrerá cada línea del archivo.

Si la línea coincide con la expresión regular `\v\S`, que significa "coincidir con un carácter que no sea un espacio en blanco",
entonces no debe estar en blanco, por lo que debemos devolver su número de línea.

Si la línea no coincide, damos la vuelta al siguiente.

Si el bucle llega hasta el final del archivo sin volver nunca, entonces no hay líneas que no estén en blanco después de la línea actual.
Devolvemos -2 si eso sucede. -2 no es un número de línea válido, por lo que es una manera fácil de decir "lo siento, no hay un resultado válido".

Podríamos haber devuelto -1, porque tampoco es un número de línea válido.
¡Incluso podría haber elegido 0, ya que los números de línea en Vim comienzan en 1!
Entonces, ¿por qué elegí -2, que parece una elección extraña?

Elegí -2 porque estamos trabajando con código plegable, y '-1' (y '0') es una cadena de nivel de plegado Vim especial.

Cuando mis ojos leen este archivo y veo un -1, mi cerebro inmediatamente piensa en "nivel de plegado indefinido".
Lo mismo es cierto con 0.
Escogí -2 aquí simplemente para hacer obvio que no es un nivel de plegado, sino que es un "error".

Si esto le parece extraño, puede cambiar con seguridad el -2 a un -1 o un 0. Es solo una preferencia de estilo de programacion.

### Finalizando nuestra funcion de folding.

Esto está resultando ser un capítulo bastante largo, así que terminemos la función de folding.
Cambie `GetPotionFold` para que se vea así:

```vimscript
function! GetPotionFold(lnum)
    if getline(a:lnum) =~? '\v^\s*$'
        return '-1'
    endif

    let this_indent = IndentLevel(a:lnum)
    let next_indent = IndentLevel(NextNonBlankLine(a:lnum))

    if next_indent == this_indent
        return this_indent
    elseif next_indent < this_indent
        return this_indent
    elseif next_indent > this_indent
        return '>' . next_indent
    endif
endfunction
```

¡Eso es mucho código nuevo! Veamos cómo funciona todo.

### Blancos


Primero tenemos nuestro checkeo para líneas en blanco.
Nada ha cambiado allí. Si superamos esa verificación, sabemos que estamos viendo una línea que no está en blanco.

### Buscando niveles de indentacion

A continuación, usamos nuestras dos funciones auxiliares para obtener el nivel de indentacion de la línea actual y el nivel de indentacion de la siguiente línea no en blanco.

Quizás se pregunte qué sucede si `NextNonBlankLine` devuelve nuestra condición de error de -2.
Si eso sucede, se ejecutará `indent(-2)`.
Ejecutar `indent()` en un número de línea inexistente solo devolverá -1.
Siga adelante y pruébelo usted mismo con `:echom indent(-2)`.

-1 dividido por cualquier `shiftwidth` mayor que 1 devolverá 0.
Esto puede parecer un problema, pero resulta que no lo será. Por ahora, no se preocupe por eso.

### Indentaciones iguales

Ahora que tenemos los niveles de indentacion de la línea actual y la siguiente línea no en blanco, podemos compararlos y decidir cómo plegar la línea actual.

Aquí está la declaración `if` nuevamente:

```vimscript
if next_indent == this_indent
    return this_indent
elseif next_indent < this_indent
    return this_indent
elseif next_indent > this_indent
    return '>' . next_indent
endif
```

Primero verificamos si las dos líneas tienen el mismo nivel de indentacion.
Si es asi, simplemente devolvemos ese nivel de indent como el nivel de folding.

Un ejemplo de esto sería:

```vimscript
a
b
    c
    d
e
```

Si estamos mirando la línea que contiene "c", tiene un nivel de sangría de 1.
Esto es lo mismo que el nivel de la siguiente línea no en blanco ("d"), por lo que devolvemos 1 como el nivel de foldlevel.

Este caso completa dos niveles de plegado en este simple ejemplo:

```vimscript
a       0
b       ?
    c   1
    d   ?
e       ?
```

¡Por pura suerte, esto también maneja el caso especial de "error" de la última línea también!
Recuerde que dijimos que `next_indent` será 0 si nuestra función auxiliar devuelve -2.

En este ejemplo, la línea "e" tiene un nivel de indentacion de 0, y `next_indent` también se establecerá en 0, por lo que este caso coincide y devuelve 0.
Los niveles de folding ahora se ven así:

```vimscript
a       0
b       ?
    c   1
    d   ?
e       0
```

### Niveles de indentacion menores

Una vez más, aquí está la declaración `if`:

```vimscript
if next_indent == this_indent
    return this_indent
elseif next_indent < this_indent
    return this_indent
elseif next_indent > this_indent
    return '>' . next_indent
endif
```

La segunda parte del `if` verifica si el nivel de indentacion de la siguiente línea es menor que la línea actual.
Esto sería como la línea "d" en nuestro ejemplo.


Si ese es el caso, una vez más devolvemos el nivel de indentacion de la línea actual.

Ahora nuestro ejemplo se ve así:

```vimscript
a       0
b       ?
    c   1
    d   1
e       0
```

Podría, por supuesto, combinar estos dos casos con `||`, pero prefiero mantenerlos separados para hacerlo más explícito.
Puede sentirse diferente. Es un problema de estilo.

Nuevamente, por pura suerte, este caso maneja el otro posible caso de "error" de nuestra función auxiliar.
Imagine que tenemos un archivo como este:

```vimscript
a
    b
    c
```

El primer caso se ocupa de la línea "b":

```vimscript
a       ?
    b   1
    c   ?
```

La línea "c" es la última línea y tiene un nivel de indentacion de 1.
El `next_indent` se establecerá en 0 gracias a nuestras funciones auxiliares.
La segunda parte del `if` coincide y establece el nivel de folding en el nivel de indentacion actual, o en 1:

```vimscript
a       ?
    b   1
    c   1
```

Esto funciona muy bien, porque "b" y "c" se doblarán juntas.

### Niveles de indentacion mayores

Aquí está esa declaración `if` complicada por última vez:

```vimscript
if next_indent == this_indent
    return this_indent
elseif next_indent < this_indent
    return this_indent
elseif next_indent > this_indent
    return '>' . next_indent
endif
```

Y nuestro archivo de ejemplo:

```vimscript
a       0
b       ?
    c   1
    d   1
e       0
```

La única línea que no hemos descubierto es "b", porque:

* "b" tiene un nivel de indentacion de 0.

* "c" tiene un nivel de indentacion de 1.

* 1 no es igual a 0, ni 1 es menor que 0.

El último caso verifica si la siguiente línea tiene un nivel de indentacion mayor que el actual.


Este es el caso de que el plegado de indentacion de Vim se equivoque, ¡y es la razón por la que estamos escribiendo este plegado personalizado en primer lugar!

El caso final dice que cuando la siguiente línea está indentada más que la actual,
debería devolver un string de un carácter `>` y el nivel de indentacion de la siguiente línea. ¿Qué diablos es eso?

Devolver un `string` como >1 desde la expresión de folding es otro de los niveles de plegado "especiales" de Vim.
Le dice a Vim que la línea actual debería abrir un pliegue del nivel dado.

En este sencillo ejemplo, podríamos haber devuelto el número, pero veremos por qué esto es importante en breve.

En este caso, la línea "b" abrirá un pliegue en el nivel 1, lo que hace que nuestro ejemplo se vea así:

```vimscript
a       0
b       >1
    c   1
    d   1
e       0
```

Eso es exactamente lo que queremos! ¡Hurra!

### Repasando

Si ha llegado hasta aquí, debería sentirse orgulloso de usted mismo.
Incluso un simple código plegable como este puede ser complicado y alucinante.

Antes de terminar, repasemos nuestro código original `factorial.pn` y veamos cómo nuestra expresión de folding llena los niveles de plegado de sus líneas.

Aquí `factorial.pn` para referencia:

```vimscript
factorial = (n):
    total = 1
    n to 1 (i):
        # Multiply the running total.
        total *= i.
    total.

10 times (i):
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.
```

Primero, los niveles de folding de cualquier línea en blanco se establecerán como indefinidos:

```vimscript
factorial = (n):
    total = 1
    n to 1 (i):
        # Multiply the running total.
        total *= i.
    total.
                                         undefined
10 times (i):
    i string print
    '! is: ' print
    factorial (i) string print
    "\n" print.
```

Cualquier línea donde la indentacion de la siguiente línea sea igual a la suya se establece en su propio nivel:

```vimscript
factorial = (n):
    total = 1                            1
    n to 1 (i):
        # Multiply the running total.    2
        total *= i.
    total.
                                         undefined
10 times (i):
    i string print                       1
    '! is: ' print                       1
    factorial (i) string print           1
    "\n" print.
```

Lo mismo sucede cuando la indentacion de la siguiente línea es menor que la línea actual:

```vimscript
factorial = (n):
    total = 1                            1
    n to 1 (i):
        # Multiply the running total.    2
        total *= i.                      2
    total.                               1
                                         undefined
10 times (i):
    i string print                       1
    '! is: ' print                       1
    factorial (i) string print           1
    "\n" print.
```

El último caso es cuando la indentacion de la línea siguiente es mayor que la línea actual.
Cuando eso sucede, el nivel de plegado de la línea se configura para abrir un pliegue del nivel de plegado de la siguiente línea:

```vimscript
factorial = (n):                         >1
    total = 1                            1
    n to 1 (i):                          >2
        # Multiply the running total.    2
        total *= i.                      2
    total.                               1
                                         undefined
10 times (i):                            >1
    i string print                       1
    '! is: ' print                       1
    factorial (i) string print           1
    "\n" print.
```

Ahora tenemos un nivel de indentacion para cada línea del archivo.
Todo lo que queda es que Vim resuelva las líneas indefinidas.

Anteriormente dije que las líneas indefinidas tomarán el nivel de pliegue más pequeño de cualquiera de sus vecinos.

Así lo describe el manual de Vim, pero no es del todo exacto.
Si ese fuera el caso, la línea en blanco en nuestro archivo tomaría el nivel de plegado 1, porque sus dos vecinos tienen un nivel de plegado de 1.

¡En realidad, la línea en blanco tendrá un nivel de plegado de 0!

La razón de esto es que no solo configuramos la línea `10 times (i):` para plegar el nivel 1 directamente.
Le dijimos a Vim que la línea abre un pliegue de nivel 1.
Vim es lo suficientemente inteligente como para saber que esto significa que la línea indefinida debe establecerse en 0 en lugar de 1.

La lógica exacta de esto probablemente esté enterrada en el código fuente de Vim.
En general, Vim se comporta de manera bastante inteligente al resolver líneas indefinidas contra niveles de plegado "especiales", por lo que generalmente hará lo que usted quiera.


Una vez que Vim ha resuelto la línea indefinida, tiene una descripción completa de cómo doblar cada línea en el archivo, que se ve así:

```vimscript
factorial = (n):                         1
    total = 1                            1
    n to 1 (i):                          2
        # Multiply the running total.    2
        total *= i.                      2
    total.                               1
                                         0
10 times (i):                            1
    i string print                       1
    '! is: ' print                       1
    factorial (i) string print           1
    "\n" print.                          1
```


Eso es todo, hemos terminado! Recargue el código de folding y juegue con el nuevo y elegante plegado en `factorial.pn`.

### Ejercicios

* Lea `:help foldexpr`.

* Leer `:help fold-expr`. Preste especial atención a todas las cadenas "especiales" que su expresión puede devolver.

* Lea `:help getline`.

* Lea `:help indent()`.

* Lea `:help line()`.

* Descubra por qué es importante que usemos `.` para combinar el carácter `>` con el número en nuestra función de folding.
¿Qué pasaría si usara `+` en su lugar? ¿Por qué?

* Definimos nuestras funciones auxiliares como funciones globales, pero esa no es una buena idea.
Cámbielos para que sean funciones locales de script.

* Deje este libro y salga un rato para que su cerebro se recupere de este capítulo.
