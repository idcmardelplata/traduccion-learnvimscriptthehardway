
# Diccionarios

El último tipo de variable de Vimscript del que hablaremos es el **diccionario**.
Los diccionarios en Vimscript son similares a los diccionarios de Python, los hashes de Ruby y los objetos de Javascript.

Los diccionarios se crean usando llaves `{}`.
Los valores son heterogéneos, pero las claves siempre se convierten en strings.
No pensó que las cosas iban a estar completamente sanas, ¿verdad?

Ejecute este comando:

```vimscript
:echo {'a': 1, 100: 'foo'}
```

Vim muestra `{'a': 1, '100': 'foo'}`, que muestra que Vimscript realmente obliga a que las claves sean strings mientras deja intactos solo los valores.

Vimscript evita la estupidez del estándar Javascript y le permite usar una coma después del último elemento en un diccionario.
Ejecute el siguiente comando:

```vimscript
:echo {'a': 1, 100: 'foo',}
```

Una vez más, Vim muestra `{'a': 1, '100': 'foo'}`.
Siempre debe usar la coma final en sus diccionarios,
especialmente cuando están definidos en varias líneas, porque hará que agregar nuevas entradas sea menos propenso a errores.

## Indexación

Para buscar una clave en un diccionario, se usa la misma sintaxis que la mayoría de los lenguajes.
Ejecute este comando:

```vimscript
:echo {'a': 1, 100: 'foo',}['a']
```

Vim muestra 1. Pruébelo con un índice sin strings:

```vimscript
:echo {'a': 1, 100: 'foo',}[100]
```

Vim coerciona el índice a un string antes de realizar la búsqueda,
lo que tiene sentido ya que las claves solo pueden ser strings.

Vimscript también admite la búsqueda de "puntos" al estilo Javascript cuando la clave es un string que consta solo de letras, dígitos y/o guiones bajos.
Pruebe los siguientes comandos:

```vimscript
:echo {'a': 1, 100: 'foo',}.a
:echo {'a': 1, 100: 'foo',}.100
```

Vim muestra el elemento correcto en ambos casos.
La forma en que elige indexar sus diccionarios es una cuestión de gusto y estilo.


## Asignando y agregando

La adición de entradas a los diccionarios se realiza simplemente asignándolas como variables.
Ejecute este comando:

```vimscript
:let foo = {'a': 1}
:let foo.a = 100
:let foo.b = 200
:echo foo
```

Vim muestra `{'a': 100, 'b': 200}`, que demuestra que asignar y agregar entradas funcionan de la misma manera.

## Removiendo entradas

Hay dos formas de eliminar entradas de un diccionario.
Ejecute los siguientes comandos:

```vimscript
:let test = remove(foo, 'a')
:unlet foo.b
:echo foo
:echo test
```

Vim muestra `{}` y `100`. La función `remove` eliminará la entrada con la clave dada del diccionario dado y devolverá el valor eliminado.
El comando `unlet` también elimina las entradas del diccionario, pero no permite obtener el valor para usarlo luego.


No puede eliminar entradas inexistentes de un diccionario.
Intente ejecutar este comando:

```vimscript
:unlet foo["asdf"]
```

Vim lanza un error.

La elección de `remove` o `unlet` es principalmente una cuestión de gusto personal.
Si me presiona, recomendaría usar `remove` en todas partes porque es más flexible que `unlet`.
`remove` puede hacer cualquier cosa que `unlet` pueda hacer, pero lo contrario no es cierto, por lo que siempre puede ser coherente si usa `remove`.

## Funciones de diccionario

Al igual que las listas, Vim tiene una serie de funciones integradas para trabajar con diccionarios.
Ejecute el siguiente comando:

```vimscript
:echom get({'a': 100}, 'a', 'default')
:echom get({'a': 100}, 'b', 'default')
```

Vim muestra `100` y `default`, al igual que la función `get` para listas.

También puede verificar si una clave dada está presente en un diccionario.

Ejecute estos comandos:

```vimscript
:echom has_key({'a': 100}, 'a')
:echom has_key({'a': 100}, 'b')
```

Vim muestra `1` y `0`. Recuerde que Vimscript trata `0` como falso y cualquier otro número como verdadero.

Puede extraer los pares clave-valor de un diccionario con `items`.
Ejecute este comando:

```vimscript
:echo items({'a': 100, 'b': 200})
```

Vim mostrará una lista anidada que se parece a `[['a', 100], ['b', 200]]`.
Por lo que puedo decir que no se garantiza que los diccionarios en Vimscript estén ordenados,
¡así que no espere que los `items` que obtiene de una llamada a `items` estén en un orden específico!

Puede obtener solo las claves o solo los valores con las funciones `keys` y `values`.
Funcionan como se esperaba, búsquelos.

## Ejercicios

* Lea `:help Dictionary` por completo (note la D capital)
* Lea `:help get()`
* Lea `:help has_key()`
* Lea `:help items()`
* Lea `:help keys()`
* Lea `:help values()`
