
# Normal

Hasta ahora hemos cubierto algunos de los comandos Vimscript más útiles, pero ¿qué hay de todas las cosas que hace a diario en el modo normal? ¿Podemos usar de alguna manera todo el conocimiento que tenemos de la edición de texto en nuestros scripts?

La respuesta es: "por supuesto". Hemos visto el comando `normal` antes, y ahora es el momento de revisarlo con un poco más de detalle. Ejecute el siguiente comando:


```vimscript
:normal G
```

Vim moverá el cursor a la última línea del archivo actual, al igual que presionar G en el modo normal. Ahora ejecute el siguiente comando:

```vimscript
:normal ggdd
```

Vim se moverá a la primera línea en el archivo (`gg`) y luego lo eliminará (`dd`).

El comando `normal` simplemente toma una secuencia de teclas y pretende que fueron escritas en modo normal. Parece bastante simple.

## Evitar mapeos.

Ejecute el siguiente comando para asignar la tecla `G` a otra cosa:


```vimscript
:nnoremap G dd
```

Ahora presionando `G` en modo normal borrará una línea. Pruebe este comando:


```vimscript
:normal G
```

Vim borrará la línea actual. El comando `normal` tendrá en cuenta los mapeos que existan.

Esto significa que necesitamos algo como la versión `nnoremap` de `nmap` para `normal`, de lo contrario nunca podremos usarlo ya que no podemos saber qué teclas han mapeado nuestros usuarios.

Por suerte Vim tiene un comando `normal!` que hace exactamente esto. Ejecute este comando:

```vimscript
:normal! G
```

Esta vez, Vim se mueve a la parte inferior del archivo a pesar de que `G` esta mapeado.

Al escribir scripts Vim, siempre debe usar `normal!`, y nunca usar `normal`. No puede confiar en las teclas que sus usuarios habrán mapeado en sus archivos `~/.vimrc`.

## Caracteres especiales

Si juega con `normal!` el tiempo suficiente, probablemente notará un problema. Pruebe el siguiente comando:


```vimscript
:normal! /foo<cr>
```

A primera vista puede parecer que esto debería realizar una búsqueda de foo, pero verá que no funciona. El problema es que `normal!` no analiza secuencias de caracteres especiales como `<cr>`.

En este caso, Vim cree que quería buscar la secuencia de caracteres "f, o, o, corchete '<', c, r, corchete '>'", y no se da cuenta de que incluso presionó return para realizar la búsqueda. Hablaremos sobre cómo solucionar esto en el siguiente capítulo.

## Ejercicios

Lea `:help normal`. El final de eso hara alusión al tema del próximo capítulo.

## Credito Extra

Si no se siente con ganas de un desafío, salte esta sección. Si es así, buena suerte!

Recordar lo qué `:help normal` dijo hacerca de `undo`. Intente hacer un mapeo que elimine dos líneas, pero le permita deshacer cada eliminación por separado. `nnoremap <leader> d dddd` es un buen lugar para comenzar.

¡No necesitará realmente `normal!` para esto (basta con `nnoremap`), pero ilustra un buen punto:
a veces, leer acerca de un comando Vim puede despertar un interés en algo no relacionado.

Si nunca ha usado el comando `helpgrep`, probablemente lo necesite ahora.
Lea `:help helpgrep`. Preste atención a las partes sobre cómo navegar entre los aciertos.

No se preocupe por los `petterns` todavía, los cubriremos pronto.
Por ahora es suficiente saber que puede usar algo como foo.*Bar para encontrar líneas que contengan esa expresión regular en la documentación.

Desafortunadamente, `helpgrep` puede ser frustrante a veces porque necesita saber qué palabras buscar antes de poder encontrarlas.
Le dejaré un poco y te diré que, en este caso, está buscando una manera de romper la secuencia de `undo` de Vim manualmente,
para que las dos eliminaciones en su mapeo se puedan deshacer por separado.

En el futuro, ser pragmático. A veces, Google es más rápido y fácil cuando no sabe exactamente lo que está buscando.
