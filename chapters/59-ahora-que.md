
# Ahora que?

Si ha leído hasta este punto y ha completado todos los ejemplos y ejercicios, ahora tiene una comprensión bastante sólida de los conceptos básicos de Vimscript. Sin embargo, no se preocupe, ¡aún queda mucho por aprender!

Aquí hay algunas ideas de temas a considerar si tiene hambre de más.

### Esquemas de color

En este libro agregamos resaltado de sintaxis para archivos de Potion. El otro lado de la moneda es la creación de esquemas de colores personalizados que definen qué color mostrar cada elemento de sintaxis.

Los esquemas de color en Vim son bastante sencillos, aunque un poco repetitivos de hacer.
Lea `:help highlight` para aprender lo básico.
Es posible que desee echar un vistazo a algunos de los esquemas de color integrados para ver cómo estructuran sus archivos.

Si está preparado para un desafío, eche un vistazo a la fuente de mi propio esquema de color Bad Wolf para ver cómo he usado Vimscript para hacer la definición y el mantenimiento mucho más fáciles para mí. Preste atención al diccionario de "paleta" y a la función `HL` que construye dinámicamente los comandos de `highlight`.

### El comando command

Muchos plugins permiten al usuario interactuar con ellos a través de mapeos de teclas y llamadas a funciones, pero algunos prefieren crear comandos Ex.
Por ejemplo, el plugin Fugitive crea comandos como `:Gbrowse` y `:Gdiff` y deja que el usuario decida cómo llamarlos.

Los comandos como este se crean con el comando `:command`. `Lea :help user-commands` para aprender a hacer los suyos.
Debería saber suficiente Vimscript ahora que leer la documentación de Vim es suficiente para aprender sobre nuevos comandos.

### runtimepath

En este libro he pasado por alto cómo Vim decide qué archivos cargar al decir "solo use Pathogen".
Ahora que conoce una cantidad decente de Vimscript, puede leer `:help runtimepath` y consultar el código fuente de Pathogen para descubrir qué está sucediendo realmente bajo el capó.

### Omnicomplete

Vim ofrece varias formas diferentes de completar el texto (lea `:help ins-completion` para tener una idea general).
La mayoría son bastante simples, pero la más poderosa de ellas es `"omnicomplete"`, que le permite llamar a una función de Vimscript personalizada para determinar los completados de cualquier forma que pueda imaginar.

Cuando esté listo para sumergirse en la madriguera del conejo de omnicompletion, puede comenzar con `:help omnifunc` y `:help compl-omni` y siga el rastro desde allí.

### Soporte de compiladores

En nuestro plugin de Potion creamos algunos mapeos para compilar y ejecutar nuestros archivos de Potion.
Vim ofrece un soporte mucho más profundo para interactuar con los compiladores, incluido el análisis de errores de compilación y proporciona una buena lista que le permite saltar a la línea de cada error.

Si está interesado en esto, puede sumergirse leyendo `:help quickfix.txt` en su totalidad.
Sin embargo, le advertiré ahora que el formato de error no es para los débiles de corazón.

### Otros lenguajes

Este libro se ha centrado en Vimscript, pero Vim también ofrece interfaces en varios otros lenguajes, como Python, Ruby y Lua.
Esto significa que puede escribir Vim en un idioma diferente si no le gusta Vimscript.

Todavía es bueno conocer Vimscript para editar su `~/.vimrc`, y para comprender la API que Vim presenta en cada lenguaje.
Pero usar un lenguaje alternativo puede ser una excelente manera de escapar de la rudeza de Vimscript, especialmente para plugins grandes.

Si desea obtener más información sobre la creación de scripts de Vim en un lenguaje en particular, consulte los documentos de ayuda:

* `:help Python`
* `:help Ruby`
* `:help Lua`
* `:help perl-using`
* `:help MzScheme`

### Documentación de Vim

Como nota de despedida final, aquí hay una lista de temas de ayuda de Vim que son especialmente útiles, informativos, interesantes o simplemente divertidos (sin ningún orden en particular):

* `:help various-motions`
* `:help sign-support`
* `:help virtualedit`
* `:help map-alt-keys`
* `:help error-messages`
* `:help development`
* `:help tips`
* `:help 24.8`
* `:help 24.9`
* `:help usr_12.txt`
* `:help usr_26.txt`
* `:help usr_32.txt`
* `:help usr_42.txt`

### Ejercicios

¡Vaya a escribir un plugin de Vim para algo que siempre ha querido y compártelo con el mundo!
