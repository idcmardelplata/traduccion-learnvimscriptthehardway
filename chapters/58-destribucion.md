
# Distribución

Por ahora tiene las habilidades de Vimscript para hacer plugins de Vim que muchas otras personas pueden encontrar útiles.
Este capítulo explicará cómo obtener sus plugins online y hacerlos fácilmente disponibles, así de cómo hacer correr la voz sobre ellos a los usuarios potenciales.

### Hosting

Lo primero que deberá hacer es publicar su plugin en línea para que otras personas puedan descargarlo. El lugar canónico para que los plugins de Vim vivan es la [ sección de scripts del sitio web de Vim ](http://www.vim.org/scripts/).

Necesitará una cuenta gratuita en el sitio web. Una vez que tenga una, puede hacer clic en el link "Add Script" y completar el formulario.
Debería ser bastante autoexplicativo.

Una tendencia reciente en los últimos años ha sido distribuir plugins alojando sus repositorios en un lugar público como Bitbucket o GitHub u Gitlab.
El aumento de la popularidad de este método probablemente se puede atribuir a dos factores.
Primero, Pathogen ha hecho trivial mantener el código de cada plugin instalado en su propia ubicación separada.
El aumento de los sistemas de control de versiones distribuidas como Mercurial y Git y los sitios de alojamiento público como Bitbucket y GitHub también probablemente han tenido un impacto.

Proporcionar repositorios es útil para las personas que mantienen sus `dotfiles` en un repositorio controlado por un sistema de versiones propio.
Los usuarios de Mercurial pueden usar los "subrepositorios" de Mercurial para realizar un seguimiento de las versiones de plugins, y los usuarios de Git pueden usar submódulos (aunque solo para otros repositorios de Git, a diferencia de los subrepos de Mercurial).

Tener un repositorio completo para cada uno de sus plugins instalados también facilita la depuración cuando algo sale mal con ellos.
Puede usar `blame`,  `bisect` y cualquiera de las otras herramientas que su VCS proporciona para descubrir qué está sucediendo.
También es más fácil contribuir con las correcciones si ya tiene el repositorio en su máquina.

Espero haberle convencido de que también debe hacer que el repositorio de su plugin esté disponible públicamente.
Realmente no importa qué servicio use, siempre que el repositorio esté disponible en algún lugar.

### Documentación

Ya ha documentado su plugin a fondo en el formato de ayuda interna de Vim, pero su trabajo aún no ha terminado.
Todavía querrá proponer una descripción rápida que resuma algunas cosas:

* ¿De qué se trata su plugin?
* ¿Por qué el usuario querría usarlo?
* ¿Por qué es mejor que los plugins de la competencia (si los hay)?
* ¿Cuál es la licencia?
* Un enlace a una bonita versión de la documentación completa, presentada por el sitio web vim-doc.

Esto debería ir en su archivo `README` (que se mostrará en la página de inicio de los repositorios de Bitbucket y GitHub u Gitlab), y puede usarlo como la descripción para la entrada del plugin en Vim.org.

Incluir algunas capturas de pantalla es casi siempre una gran idea.
Ser un editor de solo texto no significa que Vim no tenga una interfaz de usuario.

### Publicidad

Una vez que haya instalado su plugin en todos sus hogares en la web, ¡cuéntele al mundo! Puede compartirlo con sus seguidores en Twitter, publicarlo en la sección /r/vim de Reddit, escribir una entrada de blog al respecto en su propio sitio web personal y anunciarlo en la lista de correo de Vim para empezar.


Cada vez que libere una creación suya, recibirá algunos elogios y algunas críticas.
No deje que las palabras negativas le afecten demasiado.

Escuche lo que dicen, pero mantenga una piel gruesa y no se emocione demasiado cuando alguien señale fallas (válidas o no) en su trabajo.
Nadie es perfecto, y esto es Internet, por lo que deberá poder tomar un poco de calor y encogerse de hombros si quiere mantenerte feliz y motivado.

### Ejercicios

Cree una cuenta en Vim.org si aún no tiene una.

Mire los archivos `README` de algunos de sus plugins favoritos para ver cómo están estructurados y qué tipo de información incluyen.
