

# Alternancia

En uno de los primeros capítulos, hablamos sobre cómo establecer opciones en Vim.
¡Para las opciones booleanas podemos usar `set someoption!` para "alternar" la opción.
Esto es especialmente bueno cuando creamos un mapeo para ese comando.


Ejecute el siguiente comando:

```vimscript
:nnoremap <leader>N :setlocal number!<cr>
```

Pruébelo presionando `<leader>N` en modo normal.
Vim activará y desactivará los números de línea de la ventana actual.
Crear un mapeo de "alternar" como esta es realmente útil, porque no necesitamos tener dos teclas separadas para apagar y encender algo.

Lamentablemente, esto solo funciona para las opciones booleanas.
Si queremos alternar una opción no booleana, necesitaremos hacer un poco más de trabajo.

## Opciones de alternancia

Comencemos creando una función que alternará una opción para nosotros y un mapeo que la llamará.
Ponga lo siguiente en su archivo `~/.vimrc` (o un archivo separado en `~/.vim/plugin/` si lo prefiere):

```vimscript
nnoremap <leader>f :call FoldColumnToggle()<cr>

function! FoldColumnToggle()
    echom &foldcolumn
endfunction
```

Guarde y recargue el archivo, luego pruébelo presionando `<leader>f` Vim mostrará el valor actual de la opción de `foldcolumn`.
Continúe y lea: `:help foldcolumn` si no esta familiarizado con esta opcion.

Agreguemos la funcionalidad de alternancia real.
Edite el código para que se vea así:

```vimscript
nnoremap <leader>f :call FoldColumnToggle()<cr>

function! FoldColumnToggle()
    if &foldcolumn
        setlocal foldcolumn=0
    else
        setlocal foldcolumn=4
    endif
endfunction
```

Guarde y recargue el archivo y pruébelo.
Cada vez que presione `<leader>f`, Vim mostrará u ocultará la columna de plegado.

La instrucción `if` simplemente comprueba si `&foldcolumn` es verdadero (recuerde que Vim trata el entero `0` como falso y cualquier otro número como verdadero).
Si es así, lo establece en cero (lo que lo oculta). De lo contrario, lo establece en cuatro. Bastante simple.

Puede usar una función simple como esta para alternar cualquier opción donde `0` significa "apagado" y cualquier otro número está "encendido".

## Alternar otras cosas

Las opciones no son lo único que podríamos querer alternar.
Una cosa particularmente agradable para tener en un mapeo es la ventana de quickfix.
Comencemos con el mismo esqueleto que antes. Agregue el siguiente código a su archivo:

```vimscript
nnoremap <leader>q :call QuickfixToggle()<cr>

function! QuickfixToggle()
    return
endfunction
```
Este mapeo aún no hace nada.
Transformémoslo en algo un poco más útil (pero que aún no está completamente terminado).
Cambie el código para que se vea así:

```vimscript
nnoremap <leader>q :call QuickfixToggle()<cr>

function! QuickfixToggle()
    copen
endfunction
```

Guarde y recargue el archivo.
Si prueba el mapeo ahora, verá que simplemente abre la ventana de `quickfix`.

Para obtener el comportamiento de "alternancia" que estamos buscando,
usaremos una solución rápida y sucia: una variable global.
Cambie el código para que se vea así:

```vimscript
nnoremap <leader>q :call QuickfixToggle()<cr>

function! QuickfixToggle()
    if g:quickfix_is_open
        cclose
        let g:quickfix_is_open = 0
    else
        copen
        let g:quickfix_is_open = 1
    endif
endfunction
```

Lo que hemos hecho es bastante simple:
simplemente estamos almacenando en una variable global el estado abierto/cerrado de la ventana de `quickfix` cada vez que llamamos a la función.

Guarde y recargue el archivo, e intente ejecutar el mapeo.
¡Vim se quejará de que la variable aún no está definida!
Arreglemos eso inicializándolo de una vez:

```vimscript
nnoremap <leader>q :call QuickfixToggle()<cr>

let g:quickfix_is_open = 0

function! QuickfixToggle()
    if g:quickfix_is_open
        cclose
        let g:quickfix_is_open = 0
    else
        copen
        let g:quickfix_is_open = 1
    endif
endfunction
```

Guarde y recargue el archivo, e intente nuevamente el mapeo. Esta vez funciona!.

## Mejoras


Nuestra función de alternar funciona, pero tiene algunos problemas.

La primera es que si el usuario abre o cierra la ventana manualmente con `:copen` o `:cclose`, nuestra variable global no se actualiza.
Esto no es realmente un gran problema en la práctica porque la mayoría de las veces el usuario probablemente abrirá la ventana con el mapeo,
y si no, siempre puede presionarlo nuevamente.

Esto ilustra un punto importante sobre la escritura del código Vimscript:
si intenta manejar cada caso limite, se atascará en él y nunca realizará ningún trabajo.

Obtener algo que funciona la mayor parte del tiempo (y no explota cuando no funciona) y volver a la codificacion suele ser mejor que pasar horas para que sea 100% perfecto.
La excepción es cuando está escribiendo un plugin que espera que muchas personas usen.
En ese caso, es mejor dedicarle el tiempo y hacerlo a prueba de balas para mantener a sus usuarios contentos y reducir los informes de errores.

## Restaurar las ventanas y los Buffers

El otro problema con nuestra función es que si el usuario ejecuta el mapeo cuando ya está en la ventana de quickfix,
Vim lo cierra y los pone en la última división en lugar de enviarlos de regreso a donde estaban.
Esto es molesto si solo desea verificar la ventana de corrección rápida y volver a trabajar.

Para resolver esto, presentaremos un modismo que resulta muy útil al escribir complementos de Vim.
Edite su código para que se vea así:

```vimscript
nnoremap <leader>q :call QuickfixToggle()<cr>

let g:quickfix_is_open = 0

function! QuickfixToggle()
    if g:quickfix_is_open
        cclose
        let g:quickfix_is_open = 0
        execute g:quickfix_return_to_window . "wincmd w"
    else
        let g:quickfix_return_to_window = winnr()
        copen
        let g:quickfix_is_open = 1
    endif
endfunction
```
Hemos agregado dos nuevas líneas en este mapeo.
Uno de ellas (en la cláusula else) que establece otra variable global que guarda el número de ventana actual antes de ejecutar `:copen`.

La segunda línea (en la cláusula `if`) ejecuta `wincmd w` con ese número antepuesto como un conteo, lo que le dice a Vim que vaya a esa ventana.


Una vez más, nuestra solución no es a prueba de balas, porque el usuario puede abrir o cerrar una nueva división entre ejecuciones del mapeo.
Aun así, maneja la mayoría de los casos, por lo que es lo suficientemente bueno por ahora.

Esta estrategia de salvar manualmente el estado global estaría mal vista en la mayoría de los programas serios,
pero para las pequeñas funciones de Vimscript es una forma rápida y sucia de hacer que algo funcione y siga adelante con su vida.

## Ejercicios

* Lea `:help foldcolumn`
* Lea `:help winnr()`
* Lea `:help ctrl-w_w`
* Lea `:help wincmd`

Cree espacios de nombres para las funciones agregando `s:` y `<SID>` cuando sea necesario.
