FROM debian:latest

LABEL maintainer="Martin Hernan Algañaraz <idcmardelplata@gmail.com>"

WORKDIR /build

COPY . .

ADD  https://github.com/jgm/pandoc/releases/download/2.9.1.1/pandoc-2.9.1.1-1-amd64.deb /build
RUN dpkg -i pandoc-2.9.1.1-1-amd64.deb


RUN apt-get update -y && apt-get install make texlive-fonts-recommended texlive-xetex ttf-dejavu lmodern -y --no-install-recommends
RUN apt-get clean && rm -rf pandoc-2.9.1.1-1-amd64.deb

ENTRYPOINT /bin/bash
