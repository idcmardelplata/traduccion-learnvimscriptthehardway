![cover Learn Vimscript In The Hard Way](./images/cover.png)

Learn VimScript in The hard Way (Traducción al castellano)
==========================================================


Este libro es la traducción al español del libro **Learn VimScript in The hard Way** de [ Steve Losh ](https://stevelosh.com/),
donde el autor te llevara de la mano paso a paso a trabes de un viaje por VimScript dándote las herramientas necesarias para aprender
a crear tus propios plugins para Vim como asi también aprender a modificar el editor a tu gusto. Asique... Disfrútalo

He realizado la traducción al castellano porque considero que este es uno de los pocos materiales de estudios suficientemente completos
que existen en la actualidad para aprender VimScript en profundidad (de echo es el unico libro sobre e tema que conozco).

He intentado ser lo mas fiel posible con el escrito original, salvo algunas partes que he sacrificado en honor la legibilidad y para hacer mas comprensibles los conceptos que explica el libro

Puedes descargar el libro en diferentes formatos de forma individual:

* [ docx ](./formatos/docx/AprendeVimScriptDeLaManeraDificil.docx)
* [ epub ](./formatos/epub/AprendeVimScriptDeLaManeraDificil.epub)
* [ html ](./formatos/html/AprendeVimScriptDeLaManeraDificil.html)
* [ pdf ](./formatos/pdf/AprendeVimScriptDeLaManeraDificil.pdf)

O tambien en su conjunto haciendo click [ aqui ](./formatos/learnvimscriptinthehardway.tgz)

Si has encontrado algún error o piensas que puedes modificar algo agradecería muchísimo tu colaboración.

Puedes clonar este repositorio y enviarme tus correcciones o modificaciones a trabes de un pull request.

Si te ayudo el leer este libro en castellano y quieres regalarme una cerveza te lo agradeceré.

Puedes hacerlo en estas direcciónes:

Bitcoin: `1VkBDa4GqFZJpuobKrkyr6g7tgJDw1b6s`

Ethereum: `0xcA3CFbB168f4DE8eC87034d707F2e30C789d9413`

Litecoin: `LdJQGCa8KbmrCZbVf2HY1PsBT3r3vMcq7r`

y como tambien tomo cafe podes [![regalarme un café](https://cdn.cafecito.app/imgs/buttons/button_1.svg)](https://cafecito.app/idcmardelplata)

¡Happy Coding With Vim! ;)
